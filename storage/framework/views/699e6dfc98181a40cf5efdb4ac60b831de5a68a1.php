<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="breadcrumb">
    <div class="container">
        <ol class="breadcrumb-list">
            <li>
                <a href="/">
                    <span>Home</span>
                </a>
            </li>
            <li>
                <strong> Return And Exchange</strong>
            </li>
        </ol>
    </div>
</div>
<section>
    <div class="catalog-view">
        <div class="container">
            <div class="catalog-view-row">
            </div>
            <!-- Product Details Start -->
            <div class="product-info">
                <ul class="product-tablist row m-0">
                    <li class="col p-0">
                        <a href="#product-details" class="scrollLink active">
                            <h2>Welcome to eMetro Customer Service</h2>
                        </a>
                    </li>
                </ul>
                <div class="product-i-tabs row m-0">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h5>What is eMetro Return Policy?</h5>
                                <h3>Risk-Free 100% Satisfaction Guarantee</h3>
                                <br>
                                <p><strong>On Membership:</strong>We will cancel and refund your membership fee at any time if you are dissatisfied.</p>
                                <br>
                                <p><strong>On Merchandise:</strong>We guarantee your satisfaction on every product we sell, and will refund your purchase price*,
                                    with the following exceptions:</p><br>

                                <p> •	FMCG Products: eMetro will accept returns within 5 days (from the date the member purchased from eMetro) for items such as Packed Foods,
                                        Grains, Pulses, By-Products. Our eMetro Stock Points Services representatives are available to assist with technical support and warranty
                                         information for many of the products above.
                                </p><br>
                                <p>    •	Products such as Fruits, Vegetables and Fresh Produce items like Green Leafy Vegetables with a limited useful life expectancy,
                                            may be sold with a product-specific limited warranty.
                                </p><br>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">How to Return an Item</h3>
                                <br>
                                <div class="product-info-description product-i-content">

                                    <p>Products purchased at any eMetro location need to be returned at the returns counter at any eMetro warehouse.</p><br>
                                    <p>Items ordered online can be returned at any eMetro Stock Point -or- you can initiate a return through eMetro.in </p><br>
                                    <p> Accepted payment methods differ online and in the warehouse.</p><br>
                                    <p>*Terms and Conditions apply. See the membership counter or eMetro.com for details.</p><br>
                                    <p>Reprint your ReturnLabel</p><br>
                                </div>

                                <div class="product-info-description product-i-content">

                                    <p>If you have already requested a return label, but haven't received it yet, then follow these steps:</p><br>
                                    <p>1.	Select My Account in the upper right-hand corner of the homepage. You can log into your account through Sign in / Register.</p><br>
                                    <p>2.	Select Order Details under My Orders.</p><br>
                                    <p>3.	Choose the View Return / Refund Status hyperlink within the appropriate order, and follow the prompts to complete your reprint or re-email label request.</p><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Return or Replace your eMetro.in Online Order</h3>
                                <br>
                                <div class="product-info-description product-i-content">

                                    <p>We are committed to providing quality and value on the products we sell with a risk-free 100% satisfaction guarantee on your membership. Limited exceptions apply.</p><br>
                                    <p>You can return or replace your purchase through www.eMetro.com for the following reasons:</p><br>

                                    <p>   •	You’re dissatisfied with the item</p><br>
                                    <p>   •	The item received is incorrect</p><br>
                                    <p>   •	You received duplicate items</p><br>
                                    <p>   •	You refused upon delivery due to damage to the item or box</p><br>
                                    <p>   •	An item is missing (replacement only)</p><br>
                                    <p>   •	The shipment is lost</p><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">We offer two convenient options to request a return:</h3>
                                <br>
                                <strong>Return to eMetro Stock Point :</strong>
                                <div class="product-info-description product-i-content">
                                    <p>For an immediate eMetro refund to your virtual Wallet, including shipping and handling fees,
                                        simply return your purchase to any eMetro Stock Point location.
                                    </p><br>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Information on Warehouse Returns </h3>
                                <br>
                                <div class="product-info-description product-i-content">
                                    <p> *Terms and Conditions apply. See the membership counter or eMetro.com for details.</p>
                                    <br>
                                  <strong>Return or Replacement on eMetro.in:</strong>
                                    <p>To request a return or replacement on www.eMetro.in:</p>

                                    <p>    1.	Log into your account through www.eMetro.in</p><br>

                                    <p>    2.	Select Orders & Returns.</p><br>
                                    <p>    3.	Choose the Return Items button next to the appropriate order, and follow the prompts to complete your return or replacement request.</p>
                                    <br>
                                    <P>    4.	Receive your return label immediately or schedule a pickup (where eligible). For large items, you'll be sent an email with additional
                                                instructions.
                                    </p><br>
                                    <p>Items returned through www.eMetro.in will be refunded* - including shipping and handling fees -
                                        and will be returned in the Payment mode used to place your order. You won’t be charged for the return shipping.</p>
                                    <br>
                                    <p>Select items require additional processing time before a refund is issued. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">What’s the status of my Return or Replacement?</h3>
                                <br>
                                <div class="product-info-description product-i-content">
                                    <p>You can find the status of your return or replacement order by following the below steps:</p>
                                    <br>
                                    <p>1.	Select Orders & Returns in the upper right.</p><br>
                                    <p>2.	Follow the prompt to log into your account.</p><br>
                                    <p>3.	Choose the Order Details button next to the appropriate order.</p><br>
                                    <p>4.	Select the View Return / Refund Status link.</p><br>
                                    <p>5.	The next page will give you the status of your refund or replacement along with the date the status was updated.</p><br>
                                    <p>6.	You may also reprint your return shipping label.</p><br>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/footer/return-exchange.blade.php ENDPATH**/ ?>