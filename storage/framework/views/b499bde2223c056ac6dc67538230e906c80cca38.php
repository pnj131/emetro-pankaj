<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="breadcrumb">
    <div class="container">
        <ol class="breadcrumb-list">
            <li>
                <a href="/">
                    <span>Home</span>
                </a>
            </li>
            <li>
                <strong> Join Now </strong>
            </li>
        </ol>
    </div>
</div>
<section>
    <div class="catalog-view">
        <div class="container">
            <div class="catalog-view-row">
            </div>
            <!-- Product Details Start -->
            <div class="product-info">
                <ul class="product-tablist row m-0">
                    <li class="col p-0">
                        <a href="#product-details" class="scrollLink active">
                            <h2>Join Now</h2>
                        </a>
                    </li>
                </ul>
                <div id="search-results" class="col-xs-12 ">
                    <!-- BEGIN LayoutBuilderComposeOnly.jspf -->
                    <div class="search-results-tile  ">

                        <div id="error-message" class="hide"></div>
                        <div class="c_642605">



                            <div data-bi-layout="fp-join-costco_Slot 2" class="espot">
                                <!-- BEGIN RWD/LayoutBuilder_eSpot.jsp --><!-- This comment is necessary to avoid a caching bug --><!-- BEGIN ContentAreaRwdESpot.jsp fp-join-costco_Slot 2 --><!-- Debugging Purpose ... USE_SEARCH_BASED_CATENTRY_ESPOTS Flag Value :  1 --><!-- DEBUG SearchProvider:  Order of Catentry IDs from CMC: [] --><!-- END ContentAreaRwdESpot.jsp fp-join-costco_Slot 2 --><!-- END LayoutBuilder_eSpot.jsp -->
                            </div>

                        </div>
                        <script type="text/javascript">

                            function showOverflowLink() {
                                window.COSTCO.Search.showOverflowLink();
                            }

                            // display 'show more' link in category description espot if needed
                            if ($('.ellipsis-overflow').length > 0) {

                                $( document ).ready(function() {
                                    if(costcoloadstate.submodules){
                                        showOverflowLink();
                                    }else{
                                        document.addEventListener("CostcoSubModulesLoaded", showOverflowLink, false);
                                    }


                                });
                                $(window).resize(function() {
                                    if(costcoloadstate.submodules){
                                        showOverflowLink();
                                    }
                                });
                            }
                        </script>
                    </div>

                    <div class="search-results-tile  ">

                        <div id="error-message" class="hide"></div>
                        <div class="c_10007">



                            <div data-bi-layout="fp-join-costco_Slot 1" class="espot">
                                <!-- BEGIN RWD/LayoutBuilder_eSpot.jsp --><!-- This comment is necessary to avoid a caching bug --><!-- BEGIN ContentAreaRwdESpot.jsp fp-join-costco_Slot 1 --><!-- Debugging Purpose ... USE_SEARCH_BASED_CATENTRY_ESPOTS Flag Value :  1 --><!-- Debugging Purpose ... Call to RenderRwdESpotContent.jspf from ContentAreaRwdESpot.jsp --><!-- BEGIN - CostcoGLOBALSAS\Snippets\Marketing\ESpot\RenderRwdESpotContent.jspf --><!-- Display the actual text STARTS here -->                <!-- Special page styles here -->
                                <style type="text/css">

                                    /**************************
                                     * Costco new CSS overrides ------------------------------------------------------------------------
                                     **************************/

                                    /***********
                                     * Font body -----------------------------------------------
                                     ***********/

                                    div[class*="row no-gutter rs-costco-membership-"],
                                    div[class*="row rs-costco-membership-"]
                                    {
                                        font-family: "Helvetica Neue", Helvetica, Arial, Roboto, sans-serif;
                                    }

                                    /**************
                                     * H2 overrides --------------------------------------------
                                     **************/

                                    /* H2 overrides (THE SELECTOR MUST NOT HAVE THE "DIV") */
                                    [class*="row no-gutter rs-costco-membership-"] h2,
                                    [class*="row rs-costco-membership-"] h2
                                    {
                                        margin-top:         20px;
                                        margin-bottom:      10px;

                                        margin-block-start: 20px;
                                        margin-block-end:   10px;

                                        font-size:          20px;
                                    }

                                    @media (min-width: 668px)
                                    {
                                        [class*="row no-gutter rs-costco-membership-"] h2,
                                        [class*="row rs-costco-membership-"] h2
                                        {
                                            font-size: 22px;
                                        }
                                    }

                                    @media (min-width: 769px)
                                    {
                                        [class*="row no-gutter rs-costco-membership-"] h2,
                                        [class*="row rs-costco-membership-"] h2
                                        {
                                            font-size: 24px;
                                        }
                                    }

                                    @media (min-width: 1025px)
                                    {
                                        [class*="row no-gutter rs-costco-membership-"] h2,
                                        [class*="row rs-costco-membership-"] h2
                                        {
                                            font-size: 30px;
                                        }
                                    }

                                    /**************
                                     * H3 overrides --------------------------------------------
                                     **************/

                                    @media (min-width: 1025px)
                                    {
                                        [class*="row no-gutter rs-costco-membership-"] h3,
                                        [class*="row rs-costco-membership-"] h3
                                        {
                                            font-size: 24px;
                                        }
                                    }

                                    /* ---------------------------------------------------------------------------------------------- */

                                    /*********
                                     * GENERAL -----------------------------------------------------------------------------
                                     *********/

                                    #rs-costco-membership-wrapper sup
                                    {
                                        top:            0;
                                        font-size:      50%;
                                        vertical-align: super;
                                    }

                                    /* No gutter for columns */
                                    #rs-costco-membership-wrapper .row.no-gutter > [class*="col-"]
                                    {
                                        padding-left:  0;
                                        padding-right: 0;
                                    }

                                    /*****************
                                     * HERO BANNER ROW ---------------------------------------------------------------------
                                     *****************/

                                    /* General ---------------------------------- */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner
                                    {
                                        background-image: linear-gradient(0deg, #005daa 18.85%, #003b6b 89.9%);
                                        position:         relative; /* For image */
                                        height:           218px;
                                        overflow:         hidden;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner *
                                    {
                                        color: white !important;
                                    }

                                    /* Renew ------------------------------------ */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-renew
                                    {
                                        background-color: #00294c;
                                        display:          none !important; /* Replaced by Renew v2 below tabs */
                                        align-items:      center;
                                        height:           43px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-renew > div
                                    {
                                        display:     flex;
                                        align-items: center;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-renew a
                                    {
                                        position:    relative;
                                        display:     flex;
                                        align-items: center;
                                        margin:      0;
                                        font-size:   18px;
                                        line-height: 24px;
                                        z-index:     1;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-renew img
                                    {
                                        margin-left: 20px;
                                    }

                                    /* Inner ------------------------------------ */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-inner
                                    {
                                        position: relative;
                                        z-index:  1;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-inner h1
                                    {
                                        margin-top: 60px;
                                    }

                                    /* Image ------------------------------------ */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner > img
                                    {
                                        position:  absolute;
                                        left:      calc(50% + 43px);
                                        bottom:    0;
                                        transform: translateX(-50%);
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner > img:last-child
                                    {
                                        display: none;
                                    }

                                    /*********************
                                     * MEMBERSHIP TABS ROW -----------------------------------------------------------------
                                     *********************/

                                    /* Parent tabs ------------------------------ */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-parent > div
                                    {
                                        display:         flex;
                                        flex-direction:  column;
                                        align-items:     center;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-parent > div > div:first-child
                                    {
                                        background-color: #eee;
                                        display:          flex;
                                        justify-content:  center;
                                        margin-bottom:    5px;
                                        border-radius:    32px;
                                        padding:          9px;
                                        max-width:        100%;
                                        width:            408px;
                                        height:           60px;
                                        transform:        translateY(-23px);
                                        box-shadow:       0 3px 4px 0 rgba(74, 74, 74, 0.25);
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-parent button
                                    {
                                        flex-grow:        1;
                                        background-color: transparent;
                                        display:          flex;
                                        align-items:      center;
                                        justify-content:  center;
                                        border:           0;
                                        border-radius:    32px;
                                        line-height:      1.2;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-parent button.rs-active
                                    {
                                        background-color: #337ab7;
                                        color:            white;
                                    }

                                    /* Renew v2 --------------------------------- */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-parent > div > div:last-child
                                    {
                                        display:        flex;
                                        flex-direction: column;
                                        align-items:    center;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-parent > div > div:last-child > span
                                    {
                                        margin:      0;
                                        font-size:   20px;
                                        line-height: 24px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-parent > div > div:last-child > a
                                    {
                                        display:         flex;
                                        align-items:     center;
                                        justify-content: center;
                                        margin-top:      7px;
                                        margin-bottom:   30px;
                                        border:          1px solid #005daa;
                                        border-radius:   5px;
                                        width:           280px;
                                        height:          48px;
                                        font-size:       18px;
                                        line-height:     20px;
                                    }

                                    /* Child tabs ------------------------------- */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child
                                    {
                                        display: none;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child.rs-active
                                    {
                                        display: block;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child > div
                                    {
                                        display:     flex;
                                        align-items: flex-end;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child button
                                    {
                                        flex-grow:        1;
                                        background-color: #eee;
                                        display:          flex;
                                        flex-direction:   column;
                                        align-items:      center;
                                        justify-content:  center;
                                        border:           0;
                                        border-top:       8px solid #dbdbdb;
                                        padding:          4px 5px 10px;
                                        width:            50%;
                                        height:           106px;
                                        box-shadow:       inset 1px -1px 3px -2px rgba(0, 0, 0, 0.5);
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child button > span:nth-child(1)
                                    {
                                        font-size:   15px;
                                        line-height: 21px;
                                        color:       black;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child button > span:nth-child(2)
                                    {
                                        font-size:   24px;
                                        font-weight: bold;
                                        line-height: 21px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child button.rs-active
                                    {
                                        background-color: white;
                                        border-top-color: #005daa;
                                        height:           110px;
                                        box-shadow:       none;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child button.rs-active:first-child
                                    {
                                        border-left: 1px solid #eee;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child button.rs-active:last-child
                                    {
                                        border-right: 1px solid #eee;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child button.rs-active > span:nth-child(2)
                                    {
                                        color: #005daa;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child button:focus
                                    {
                                        position: relative;
                                        z-index:  1;
                                    }

                                    /* Child tab panes -------------------------- */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane
                                    {
                                        display:    none;
                                        border:     1px solid #eee;
                                        border-top: 0;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane.rs-active
                                    {
                                        display: block;
                                    }

                                    /* Button row */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(1)
                                    {
                                        display: flex;
                                        align-items: center;
                                        justify-content: center;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(1) > a,
                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(2) > a:last-child
                                    {
                                        background-color: #337ab7;
                                        margin-top:       32px;
                                        border-radius:    5px;
                                        padding:          15px 52px;
                                        width:            320px;
                                        font-size:        18px;
                                        color:            white !important;
                                        text-align:       center;
                                        line-height:      20px;
                                    }

                                    /* Left column */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(2)
                                    {
                                        display:        flex;
                                        flex-direction: column;
                                        padding:        32px 15px 0;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(2) > div
                                    {
                                        background-color: #e31837;
                                        margin:           0 auto 6px;
                                        padding:          6px;
                                        max-width:        277px;
                                        width:            100%;
                                        font-weight:      bold;
                                        font-size:        18px;
                                        color:            white;
                                        line-height:      1;
                                        text-align:       center;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(2) > a:nth-last-child(2)
                                    {
                                        display:      inline-block;
                                        margin-left:  auto;
                                        margin-right: auto;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane:last-child > div:nth-child(2) > div
                                    {
                                        display: none;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(2) > a:last-child
                                    {
                                        display: none;
                                    }

                                    /* Right column */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3)
                                    {
                                        padding: 32px 15px 0 0;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) ul
                                    {
                                        margin: 0;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) li
                                    {
                                        position:      relative;
                                        margin-bottom: 10px;
                                        padding-left:  40px;
                                        font-size:     18px;
                                        color:         black;
                                        line-height:   1.4;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) li:last-child
                                    {
                                        margin: 0;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) li:before
                                    {
                                        content:          "";
                                        position:         absolute;
                                        background-image: url("https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-tabs-checkmark.png");
                                        top:              5px;
                                        left:             0;
                                        width:            16px;
                                        height:           12px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) li > div
                                    {
                                        display:     none;
                                        max-width:   256px;
                                        font-size:   14px;
                                        line-height: 21px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) li > div > *
                                    {
                                        display: block;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) li > div > a
                                    {
                                        text-decoration: underline !important;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) > button
                                    {
                                        background-color: transparent;
                                        display:          flex;
                                        align-items:      center;
                                        margin-top:       20px;
                                        margin-left:      19%;
                                        border:           0;
                                        padding:          0;
                                        font-size:        18px;
                                        color:            #337ab7;
                                        line-height:      1.2;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) > button > img
                                    {
                                        transition: transform 400ms;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) > button.rs-active > img
                                    {
                                        transform: rotate(-180deg);
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) > button > span
                                    {
                                        margin-right: 9px;
                                    }

                                    /* Bottom row */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(4)
                                    {
                                        display:        flex;
                                        flex-direction: column;
                                        align-items:    center;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(4) > strong
                                    {
                                        margin-top:    24px;
                                        margin-bottom: 4px;
                                        line-height:   1;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(4) > small
                                    {
                                        margin-bottom: 24px;
                                        font-size:     10px;
                                        line-height:   1;
                                    }

                                    @media (max-width: 768px)
                                    {
                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-parent > div > div
                                        {
                                            margin-bottom: 0;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-parent button
                                        {
                                            font-size: 13px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child button.rs-active
                                        {
                                            height: 114px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane:last-child > div:nth-child(2) > div
                                        {
                                            background-color: #bebebe;
                                            display:          block;
                                        }
                                    }

                                    /*********
                                     * FAQ ROW -----------------------------------------------------------------------------
                                     *********/

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq h2
                                    {
                                        margin:     82px 0 20px;
                                        font-size:  24px;
                                        color:      #005daa;
                                        text-align: center;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row
                                    {
                                        border-top: 1px solid #eee;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row:last-child
                                    {
                                        border-bottom: 1px solid #eee;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > button
                                    {
                                        background-color: transparent;
                                        display:          flex;
                                        align-items:      center;
                                        justify-content:  space-between;
                                        border:           0;
                                        padding:          16px 0;
                                        width:            100%;
                                        text-align:       left;
                                        cursor:           pointer;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > button > span
                                    {
                                        margin-right: 15px;
                                        font-size:    18px;
                                        line-height:  24px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > button > img:last-of-type
                                    {
                                        display: none;
                                    }

                                    /*  Active state  --------------------------- */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > button[aria-expanded="true"] > span
                                    {
                                        color: #005daa;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > button[aria-expanded="true"] > img:first-of-type
                                    {
                                        display: none;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > button[aria-expanded="true"] > img:last-of-type
                                    {
                                        display: block;
                                    }

                                    /* ------------------------------------------ */

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > div
                                    {
                                        display:       none;
                                        margin-top:    11px;
                                        margin-right:  32px;
                                        margin-bottom: 16px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > div > a
                                    {
                                        display:         block;
                                        margin-bottom:   14px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > div a
                                    {
                                        text-decoration: underline !important;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > div > p
                                    {
                                        margin:      0;
                                        line-height: 24px;
                                    }

                                    /***********************
                                     * MORE WAYS TO SAVE ROW ---------------------------------------------------------------
                                     ***********************/

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways
                                    {
                                        background-color: #005daa;
                                        margin-top:       24px;
                                        padding:          50px 0;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways h2,
                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways p
                                    {
                                        color: white;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways h2
                                    {
                                        margin: 0 0 4px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways p
                                    {
                                        margin-bottom: 20px;
                                        font-size:     20px;
                                        line-height:   1;
                                    }

                                    /* jCarousel -------------------------------- */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .rs-costco-membership-jcarousel-wrapper
                                    {
                                        display: none;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .rs-costco-membership-jcarousel-wrapper.rs-active
                                    {
                                        display: block;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .jcarousel
                                    {
                                        position:      relative;
                                        margin-bottom: 15px;
                                        overflow:      hidden;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .jcarousel ul
                                    {
                                        position:   relative;
                                        margin:     0;
                                        padding:    0;
                                        width:      10000em;
                                        list-style: none;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .jcarousel li
                                    {
                                        float: left;
                                    }

                                    /* Slides ----------------------------------- */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .rs-costco-membership-slide
                                    {
                                        background-color: white;
                                        display:          flex;
                                        flex-direction:   column;
                                        margin-right:     10px;
                                        border-radius:    6px;
                                        width:            280px;
                                        overflow:         hidden;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .rs-costco-membership-slide > span
                                    {
                                        display:        flex;
                                        flex-direction: column;
                                        padding:        8px 16px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .rs-costco-membership-slide > span > span:first-child
                                    {
                                        font-weight: bold;
                                        color:       #005daa;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .rs-costco-membership-slide > span > span:last-child
                                    {
                                        display:         flex;
                                        align-items:     flex-start;
                                        justify-content: space-between;
                                        font-size:       13px;
                                        color:           #333;
                                        line-height:     1.2;
                                    }

                                    /* ------------------------------------------ */

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .rs-costco-membership-jcarousel-wrapper small
                                    {
                                        display:     block;
                                        font-size:   10px;
                                        color:       white;
                                        line-height: 1;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .rs-costco-membership-jcarousel-wrapper small > sup
                                    {
                                        top:       1px;
                                        font-size: 67%;
                                    }

                                    /* Carousel controls ------------------------ */
                                    #rs-costco-membership-wrapper .rs-costco-membership-jcarousel-wrapper
                                    {
                                        position: relative;
                                    }

                                    #rs-costco-membership-wrapper .jcarousel-prev,
                                    #rs-costco-membership-wrapper .jcarousel-next
                                    {
                                        position:         absolute;
                                        background-color: white;
                                        display:          flex;
                                        align-items:      center;
                                        justify-content:  center;
                                        top:              50%;
                                        width:            48px;
                                        height:           96px;
                                        transform:        translateY(-50%);
                                        opacity:          0.8;
                                        z-index:          10;
                                    }

                                    #rs-costco-membership-wrapper .jcarousel-prev:hover,
                                    #rs-costco-membership-wrapper .jcarousel-next:hover
                                    {
                                        opacity: 1;
                                    }

                                    #rs-costco-membership-wrapper .jcarousel-prev
                                    {
                                        left: 0;
                                    }

                                    #rs-costco-membership-wrapper .jcarousel-prev > img
                                    {
                                        transform: rotate(90deg);
                                    }

                                    #rs-costco-membership-wrapper .jcarousel-next
                                    {
                                        right: 0;
                                    }

                                    #rs-costco-membership-wrapper .jcarousel-next > img
                                    {
                                        transform: rotate(-90deg);
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .jcarousel-prev,
                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .jcarousel-next
                                    {
                                        top: 125px;
                                    }

                                    /* Carousel pagination ---------------------- */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .jcarousel-pagination
                                    {
                                        text-align: center;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .jcarousel-pagination > button
                                    {
                                        background-color: #337ab7;
                                        display:          inline-block;
                                        margin:           0 2px;
                                        border:           0;
                                        border-radius:    50%;
                                        padding:          0;
                                        width:            11px;
                                        height:           11px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways .jcarousel-pagination > button.rs-active
                                    {
                                        background-color: white;
                                    }

                                    /* Mobile */
                                    @media (max-width: 768px)
                                    {
                                        #rs-costco-membership-wrapper > .rs-costco-membership-more-ways
                                        {
                                            margin-top:  39px;
                                            padding-top: 35px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-more-ways h2
                                        {
                                            font-size:   24px;
                                            font-weight: bold;
                                            line-height: 21px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-more-ways p
                                        {
                                            font-size:   15px;
                                            line-height: 21px;
                                        }
                                    }

                                    /*****************
                                     * CITI COSTCO ROW ---------------------------------------------------------------------
                                     *****************/

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco
                                    {
                                        background-color: #eff5fc;
                                        padding:          56px 0 64px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(1) > img
                                    {
                                        margin-bottom: 52px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(2)
                                    {
                                        padding-right: 50px !important;
                                        text-align:    right;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(2) > div
                                    {
                                        text-align: center;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco a
                                    {
                                        background-color: #337ab7;
                                        display:          inline-flex;
                                        align-items:      center;
                                        justify-content:  center;
                                        margin-top:       38px;
                                        border-radius:    5px;
                                        max-width:        274px;
                                        width:            100%;
                                        height:           48px;
                                        font-size:        15px;
                                        color:            white;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco h2
                                    {
                                        margin-top:    0;
                                        margin-bottom: 7px;
                                        color:         #005daa;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(3) > p
                                    {
                                        margin-bottom: 30px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(3) > a
                                    {
                                        display: none;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(3) > div:first-of-type
                                    {
                                        margin-bottom: 30px;
                                    }

                                    /* Quad card -------------------------------- */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco .rs-costco-membership-quad-card
                                    {
                                        position:         relative;
                                        background-color: white;
                                        display:          inline-flex;
                                        flex-direction:   column;
                                        align-items:      center;
                                        border:           1px solid #cee5ff;
                                        border-radius:    6px;
                                        padding:          30px 26px 10px;
                                        width:            191px;
                                        height:           119px;
                                    }

                                    @media (min-width: 769px) and (max-width: 1024px)
                                    {
                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco .rs-costco-membership-quad-card
                                        {
                                            padding: 30px 10px 10px;
                                            width:   147px;
                                        }
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco .rs-costco-membership-quad-card:first-child
                                    {
                                        margin-right: 11px;
                                    }

                                    @media (min-width: 769px) and (max-width: 1024px)
                                    {
                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco .rs-costco-membership-quad-card:first-child
                                        {
                                            margin-right: 5px;
                                        }
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco .rs-costco-membership-quad-card > div
                                    {
                                        position:         absolute;
                                        background-color: #005daa;
                                        display:          flex;
                                        align-items:      center;
                                        justify-content:  center;
                                        top:              0;
                                        border-radius:    50%;
                                        width:            39px;
                                        height:           39px;
                                        transform:        translateY(-50%);
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco .rs-costco-membership-quad-card > span
                                    {
                                        font-size:   35px;
                                        font-weight: bold;
                                        color:       #005daa;
                                        line-height: 1;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco .rs-costco-membership-quad-card > p
                                    {
                                        max-width:   100%; /* IE 11 flexbugs */
                                        font-size:   12px;
                                        color:       black;
                                        line-height: 1.2;
                                        text-align:  center;
                                    }

                                    /*****************************
                                     * CITI COSTCO ROW (version 2) ---------------------------------------------------------
                                     *****************************/

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco-version2
                                    {
                                        margin-top: 68px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco-version2 img
                                    {
                                        width: 100%;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco-version2 img:last-child
                                    {
                                        display: none;
                                    }

                                    /* Mobile */
                                    @media (max-width: 768px)
                                    {
                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco-version2
                                        {
                                            margin-top: 49px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco-version2 img:first-child
                                        {
                                            display: none;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco-version2 img:last-child
                                        {
                                            display: block;
                                        }
                                    }

                                    /****************
                                     * PROMOTIONS ROW ----------------------------------------------------------------------
                                     ****************/

                                    #rs-costco-membership-wrapper > .rs-costco-membership-promotions
                                    {
                                        margin-top: 20px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-promotions h2
                                    {
                                        margin:      33px 0 5px;
                                        font-size:   36px;
                                        color:       #005daa;
                                        line-height: 39px;
                                        text-align:  center;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-promotions h2 + p
                                    {
                                        display:       block;
                                        margin-bottom: 34px;
                                        color:         black;
                                        font-size:     20px;
                                        font-weight:   bold;
                                        line-height:   1;
                                        text-align:    center;
                                    }

                                    /* Card ------------------------------------- */
                                    /* Row and column equal height */
                                    #rs-costco-membership-wrapper > .rs-costco-membership-promotions h2 + p + div,
                                    #rs-costco-membership-wrapper > .rs-costco-membership-promotions h2 + p + div > div,
                                    #rs-costco-membership-wrapper > .rs-costco-membership-promotions h2 + p + div > div > a
                                    {
                                        display:   flex;
                                        flex-wrap: wrap;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-promotions .row a
                                    {
                                        display:       block;
                                        margin-bottom: 16px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-promotions .rs-costco-membership-promotions-card > div
                                    {
                                        display:         flex;
                                        align-items:     flex-start;
                                        justify-content: space-between;
                                        padding-top:     13px;
                                        padding-left:    2px;
                                        padding-right:   10px;
                                        line-height:     1;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-promotions .rs-costco-membership-promotions-card > div > span
                                    {
                                        display:       block;
                                        margin-bottom: 5px;
                                        margin-right:  10px;
                                        font-size:     18px;
                                        font-weight:   bold;
                                        color:         #005daa;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner a,
                                    #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) > a,
                                    #rs-costco-membership-wrapper > .rs-costco-membership-more-ways a,
                                    #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco a,
                                    #rs-costco-membership-wrapper > .rs-costco-membership-promotions a
                                    {
                                        text-decoration: none !important;
                                    }

                                    /*****************
                                     * DISCLAIMERS ROW ---------------------------------------------------------------------
                                     *****************/

                                    #rs-costco-membership-wrapper > .rs-costco-membership-disclaimers *:not(a)
                                    {
                                        color: black;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-disclaimers p
                                    {
                                        margin-top: 7px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-disclaimers small
                                    {
                                        display:       block;
                                        margin-bottom: 15px;
                                        font-size:     10px;
                                        line-height:   14px;
                                    }

                                    #rs-costco-membership-wrapper > .rs-costco-membership-disclaimers small a
                                    {
                                        text-decoration: underline;
                                    }

                                    /********
                                     * MOBILE ------------------------------------------------------------------------------
                                     ********/

                                    /* Medium and below */
                                    @media (max-width: 768px)
                                    {
                                        /* Hero Banner row ------------------------------------------ */
                                        #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner
                                        {
                                            height: auto;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner > img
                                        {
                                            position: static;
                                            display:  none;
                                            width:    100%;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner > img:last-child
                                        {
                                            display:   block;
                                            transform: none;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-renew > div
                                        {
                                            justify-content: center;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-renew a
                                        {
                                            font-size: 15px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-renew img
                                        {
                                            margin-left: 10px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-inner
                                        {
                                            text-align: center;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-inner h1
                                        {
                                            font-weight: bold;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-inner p
                                        {
                                            margin-left:  auto;
                                            margin-right: auto;
                                        }

                                        /* Membership Tabs row -------------------------------------- */
                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child > div:first-child button > span:nth-child(2) > span
                                        {
                                            display: none;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(1)
                                        {
                                            display: none;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(2)
                                        {
                                            display: block;
                                            padding: 0;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(2) > div
                                        {
                                            margin:    0;
                                            max-width: none;
                                            width:     50%;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(2) > a:nth-last-child(2)
                                        {
                                            display:      block;
                                            margin-top:   20px;
                                            margin-left:  auto;
                                            margin-right: auto;
                                            width:        33%;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(2) > a:last-child
                                        {
                                            display:       block;
                                            margin-left:   auto;
                                            margin-right:  auto;
                                            padding-left:  0;
                                            padding-right: 0;
                                            max-width:     calc(100% - 30px); /* 15px gutter */
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3)
                                        {
                                            padding: 32px 15px 0;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) li > div
                                        {
                                            max-width: none;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) > button
                                        {
                                            margin-left:  auto;
                                            margin-right: auto;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(3) > a
                                        {
                                            padding:    15px;
                                            text-align: center;
                                        }

                                        /* FAQ row -------------------------------------------------- */
                                        #rs-costco-membership-wrapper > .rs-costco-membership-faq h2
                                        {
                                            margin-top:  36px;
                                            font-size:   24px;
                                            font-weight: bold;
                                            line-height: 21px;
                                            text-align:  left;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > div:first-child > span
                                        {
                                            font-size:   16px;
                                            line-height: 18px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-faq .rs-costco-membership-faq-row > div:first-child > img
                                        {
                                            width: 16px;
                                        }

                                        /* More Ways to Save row ------------------------------------ */
                                        #rs-costco-membership-wrapper > .rs-costco-membership-more-ways > div:first-child
                                        {
                                            padding-left: 15px !important;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-more-ways > div:first-child > *:not(.jcarousel)
                                        {
                                            padding-right: 15px;
                                        }

                                        /* Citi Costco row ------------------------------------------ */
                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco
                                        {
                                            padding-top:    24px;
                                            padding-bottom: 0;
                                            box-shadow:     0 4px 13px rgba(123, 123, 123, 0.25);
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div,
                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(2) /* Override more previous specific */
                                        {
                                            padding-left:  15px !important;
                                            padding-right: 15px !important;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(1) > img
                                        {
                                            margin-bottom: 13px;
                                            width:         146px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(2) img
                                        {
                                            width: 248px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(2) a
                                        {
                                            display: none;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(3)
                                        {
                                            text-align: center;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(3) > h2
                                        {
                                            margin-top:    15px;
                                            margin-bottom: 12px;
                                            font-size:     24px;
                                            font-weight:   bold;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(3) > p
                                        {
                                            margin-left:  auto;
                                            margin-right: auto;
                                            max-width:    300px;
                                            width:        100%;
                                            font-size:    13px;
                                            line-height:  1.4;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(3) > div
                                        {
                                            background-color: white;
                                            margin:           0 !important;
                                            padding-left:     15px;
                                            padding-right:    15px;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco .rs-costco-membership-quad-card
                                        {
                                            display:        flex;
                                            flex-direction: row;
                                            margin:         0 !important;
                                            border:         0;
                                            border-bottom:  1px solid #eee;
                                            border-radius:  0;
                                            padding:        5px;
                                            width:          auto;
                                            height:         auto;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco .rs-costco-membership-quad-card > div
                                        {
                                            flex-shrink:  0;
                                            position:     static;
                                            margin-right: 10px;
                                            transform:    none;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco .rs-costco-membership-quad-card > p
                                        {
                                            margin-left:   15px;
                                            margin-bottom: 0;
                                            text-align:    left;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-citi-costco > div:nth-child(3) > a
                                        {
                                            display:       inline-flex;
                                            margin-top:    27px;
                                            margin-bottom: 38px;
                                            width:         180px;
                                        }

                                        /* Promotions row ------------------------------------------- */
                                        #rs-costco-membership-wrapper > .rs-costco-membership-promotions h2
                                        {
                                            font-size:   24px;
                                            font-weight: bold;
                                            line-height: 1;
                                            text-align:  left;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-promotions h2 + p
                                        {
                                            margin-bottom: 15px;
                                            font-size:     16px;
                                            line-height:   21px;
                                            text-align:    left;
                                        }

                                        /* Row and column equal height */
                                        #rs-costco-membership-wrapper > .rs-costco-membership-promotions h2 + p + div,
                                        #rs-costco-membership-wrapper > .rs-costco-membership-promotions h2 + p + div > div,
                                        #rs-costco-membership-wrapper > .rs-costco-membership-promotions h2 + p + div > div > a
                                        {
                                            display: block;
                                        }

                                        #rs-costco-membership-wrapper > .rs-costco-membership-promotions .row a
                                        {
                                            margin-bottom: 25px;
                                        }
                                    }

                                    /* Extra small only */
                                    @media (max-width: 375px)
                                    {
                                        /* Hero Banner row ------------------------------------------ */
                                        #rs-costco-membership-wrapper > .rs-costco-membership-hero-banner .rs-costco-membership-hero-banner-renew a
                                        {
                                            font-size: 14px;
                                        }

                                        /* Membership Tabs row -------------------------------------- */
                                        #rs-costco-membership-wrapper > .rs-costco-membership-membership-tabs .rs-costco-membership-membership-tabs-child-tabpane > div:nth-child(2) ul
                                        {
                                            width: 100%;
                                        }
                                        .rs-costco-membership-membership-tabs-parent{
                                            margin-left:400px;
                                        }
                                    }

                                </style>
                                <div class="container">
                                <div data-bi-placement="fp-join-costco_Slot 1" class="container-fluid sust-costco fixed-container page" style="margin-left:auto; margin-right:auto; padding:0px !important;">

                                    <!-- Main RS wrapper -->
                                    <div id="rs-costco-membership-wrapper">

                                        <!-- Hero Banner row ----------------------------------------------------------- -->
                                        <div class="row no-gutter rs-costco-membership-hero-banner">
                                            <div class="col-xs-12">

                                                <!-- Renew -->
                                                <div class="row no-gutter rs-costco-membership-hero-banner-renew">
                                                    <div class="col-xs-offset-1 col-xs-10">
                                                        <a href="/sign-up" data-bi-title="renew your existing membership">
                                                            Renew Your Existing Membership
                                                            <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-arrow-right.png" alt="">
                                                        </a>
                                                    </div>
                                                </div>

                                                <!-- Inner -->
                                                <div class="row rs-costco-membership-hero-banner-inner">
                                                    <div class="col-xs-10 col-xs-offset-1">
                                                        <h1 class="sr-only">Join eMetro</h1>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Image -->
                                            <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-hero-banner1.png" alt="Costco storefront">
                                            <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-hero-banner1-mobile.png" alt="Costco storefront">
                                        </div>

                                        <!-- Membership Tabs row ------------------------------------------------------- -->
                                        <div class="row rs-costco-membership-membership-tabs" style="margin-left: 600px">
                                            <div class="col-lg-offset-2 col-lg-8 col-xl-offset-3 col-xl-6">
                                                <!-- Parent tabs -->
                                                <div class="row rs-costco-membership-membership-tabs-parent">
                                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                                        <div>
                                                            <button aria-pressed="true" class="rs-active" data-bi-title="personal membership">Personal Membership</button>
                                                            <button aria-pressed="false" data-bi-title="business membership">Business Membership</button>
                                                        </div>

                                                        <!-- Renew v2 -->
                                                        <div>
                                                            <span>Already a Costco member?</span>
                                                            <a href="/sign-up" data-bi-title="renew membership">Renew Membership</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Child tab (Personal Membership) -->
                                                <div class="rs-costco-membership-membership-tabs-child rs-active">
                                                    <!-- Tabs -->
                                                    <div class="row no-gutter">
                                                        <div class="col-xs-12">
                                                            <button aria-pressed="true" class="rs-active" data-bi-title="2% annual reward executive">
                                                                <span>2% Annual Reward</span>
                                                                <span>Executive</span>
                                                            </button>
                                                            <button aria-pressed="false" data-bi-title="everyday value gold star">
                                                                <span>Everyday Value</span>
                                                                <span>Gold Star</span>
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <!-- Child tab pane (Executive Gold Star) -->
                                                    <div class="row no-gutter rs-costco-membership-membership-tabs-child-tabpane rs-active">
                                                        <div class="col-xs-12">
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35671&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership" data-bi-title="select executive">Select Executive</a>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div>Most Rewards</div>
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35671&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership">
                                                                <img class="img-responsive" src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-tabs-personal-executive-card.png" alt="Costco Wholesale Executive Member card">
                                                            </a>
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35671&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership" data-bi-title="select executive-1">Select Executive</a>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <ul>
                                                                <!-- Annual reward -->
                                                                <li tabindex="-1">
                                                                    <span>2% Annual Reward</span>
                                                                    <div>
                                                        <span>
                                                            Up to $1,000 on Eligible Costco and Costco
                                                            Travel purchases
                                                        </span>
                                                                        <a href="/executive-rewards.html" data-bi-title="terms and conditions apply">Terms and Conditions Apply</a>
                                                                    </div>
                                                                </li>

                                                                <!-- Services discounts -->
                                                                <li>
                                                                    <span>Costco Services Discounts</span>
                                                                    <div>
                                                        <span>
                                                            Additional benefits and discounts on many Costco
                                                            services
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Connection magazine -->
                                                                <li>
                                                                    <span><em>Costco Connection</em> Magazine</span>
                                                                    <div>
                                                        <span>
                                                            Your monthly connection to all things Costco
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Shop online -->
                                                                <li>
                                                                    <span>Shop Online and in Warehouses</span>
                                                                    <div>
                                                        <span>
                                                            Shop all Costco warehouses and online at
                                                            Costco.com
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Membership cards -->
                                                                <li>
                                                                    <span>Includes 2 Membership Cards</span>
                                                                    <div>
                                                        <span>
                                                            For you and for someone in your household
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Satisfaction -->
                                                                <li>
                                                                    <span>100% Satisfaction Guarantee</span>
                                                                    <div>
                                                        <span>
                                                            We will cancel and refund your membership at any
                                                            time if you are dissatisfied
                                                        </span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <button aria-pressed="false" data-bi-title="see benefit details">
                                                                <span>See Benefit Details</span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-tabs-benefit-caret.png" alt="">
                                                            </button>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <strong>$120/Year</strong>
                                                            <small>Plus applicable sales tax</small>
                                                        </div>
                                                    </div>

                                                    <!-- Child tab pane (Gold Star) -->
                                                    <div class="row no-gutter rs-costco-membership-membership-tabs-child-tabpane">
                                                        <div class="col-xs-12">
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35669&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership" data-bi-title="select gold star">Select Gold Star</a>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div>Most Rewards</div>
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35669&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership">
                                                                <img class="img-responsive" src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-tabs-personal-goldstar-card.png" alt="Costco Wholesale Gold Star Member card">
                                                            </a>
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35669&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership" data-bi-title="select gold star-1">Select Gold Star</a>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <ul>
                                                                <!-- Shop online -->
                                                                <li tabindex="-1">
                                                                    <span>Shop Online and in Warehouses</span>
                                                                    <div>
                                                        <span>
                                                            Shop all Costco warehouses and online at
                                                            Costco.com
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Membership cards -->
                                                                <li>
                                                                    <span>Includes 2 Membership Cards</span>
                                                                    <div>
                                                        <span>
                                                            For you and for someone in your household
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Satisfaction -->
                                                                <li>
                                                                    <span>100% Satisfaction Guarantee</span>
                                                                    <div>
                                                        <span>
                                                            We will cancel and refund your membership at any
                                                            time if you are dissatisfied
                                                        </span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <button aria-pressed="false" data-bi-title="see benefit details-1">
                                                                <span>See Benefit Details</span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-tabs-benefit-caret.png" alt="">
                                                            </button>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <strong>$60/Year</strong>
                                                            <small>Plus applicable sales tax</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Child tab (Business Membership) -->
                                                <div class="rs-costco-membership-membership-tabs-child">
                                                    <!-- Tabs -->
                                                    <div class="row no-gutter">
                                                        <div class="col-xs-12">
                                                            <button aria-pressed="false" data-bi-title="2% annual reward business executive">
                                                                <span>2% Annual Reward</span>
                                                                <span>Business Executive</span>
                                                            </button>
                                                            <button aria-pressed="false" data-bi-title="everyday value business">
                                                                <span>Everyday Value</span>
                                                                <span>Business</span>
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <!-- Child tab pane (Business Executive) -->
                                                    <div class="row no-gutter rs-costco-membership-membership-tabs-child-tabpane">
                                                        <div class="col-xs-12">
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35673&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership" data-bi-title="select business executive">Select Business Executive</a>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div>Most Rewards</div>
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35673&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership">
                                                                <img class="img-responsive" src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-tabs-personal-executive-card.png" alt="Costco Wholesale Executive Member card">
                                                            </a>
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35673&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership" data-bi-title="select business executive-1">Select Business Executive</a>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <ul>
                                                                <!-- Annual reward -->
                                                                <li tabindex="-1">
                                                                    <span>2% Annual Reward</span>
                                                                    <div>
                                                        <span>
                                                            Up to $1,000 on Eligible Costco and Costco
                                                            Travel purchases
                                                        </span>
                                                                        <a href="/executive-rewards.html" data-bi-title="terms and conditions apply-1">Terms and Conditions Apply</a>
                                                                    </div>
                                                                </li>

                                                                <!-- Services discounts -->
                                                                <li>
                                                                    <span>Costco Services Discounts</span>
                                                                    <div>
                                                        <span>
                                                            Additional benefits and discounts on many Costco
                                                            services
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Magazine -->
                                                                <li>
                                                                    <span><em>Costco Connection</em> Magazine</span>
                                                                    <div>
                                                        <span>
                                                            Your monthly connection to all things Costco
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Shop online -->
                                                                <li>
                                                                    <span>Shop Online and in Warehouses</span>
                                                                    <div>
                                                        <span>
                                                            Shop all Costco warehouses and online at
                                                            Costco.com
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Purchase for resale -->
                                                                <li>
                                                                    <span>Purchase for Resale</span>
                                                                    <div>
                                                        <span>
                                                            Resale of liquor is prohibited except where
                                                            allowed by state law
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Membership cards -->
                                                                <li>
                                                                    <span>Includes 2 Membership Cards</span>
                                                                    <div>
                                                        <span>
                                                            For you and for someone in your household
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Additional people -->
                                                                <li>
                                                                    <span>Add Additional People ($60 each)</span>
                                                                    <div>
                                                        <span>
                                                            To add Affiliates to your Business Membership,
                                                            visit the membership counter at any warehouse
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Satisfaction -->
                                                                <li>
                                                                    <span>100% Satisfaction Guarantee</span>
                                                                    <div>
                                                        <span>
                                                            We will cancel and refund your membership at any
                                                            time if you are dissatisfied
                                                        </span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <button aria-pressed="false" data-bi-title="see benefit details-2">
                                                                <span>See Benefit Details</span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-tabs-benefit-caret.png" alt="">
                                                            </button>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <strong>$120/Year</strong>
                                                            <small>Plus applicable sales tax</small>
                                                        </div>
                                                    </div>

                                                    <!-- Child tab pane (Business) -->
                                                    <div class="row no-gutter rs-costco-membership-membership-tabs-child-tabpane">
                                                        <div class="col-xs-12">
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35670&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership" data-bi-title="select business">Select Business</a>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div>Most Rewards</div>
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35670&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership">
                                                                <img class="img-responsive" src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-tabs-business-business-card.png" alt="Costco Wholesale Business Member card">
                                                            </a>
                                                            <a href="/ManageShoppingCartCmd?actionType=add&amp;quantity=1&amp;itemNumber=35670&amp;quickPurchase=true&amp;targetedCartBehaviour=Membership" data-bi-title="select business-1">Select Business</a>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <ul>
                                                                <!-- Shop online -->
                                                                <li tabindex="-1">
                                                                    <span>Shop Online and in Warehouses</span>
                                                                    <div>
                                                        <span>
                                                            Shop all Costco warehouses and online at
                                                            Costco.com
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Purchase for resale -->
                                                                <li>
                                                                    <span>Purchase for Resale</span>
                                                                    <div>
                                                        <span>
                                                            Resale of liquor is prohibited except where
                                                            allowed by state law
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Membership cards -->
                                                                <li>
                                                                    <span>Includes 2 Membership Cards</span>
                                                                    <div>
                                                        <span>
                                                            For you and for someone in your household
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Additional people -->
                                                                <li>
                                                                    <span>Add Additional People ($60 each)</span>
                                                                    <div>
                                                        <span>
                                                            To add Affiliates to your Business Membership,
                                                            visit the membership counter at any warehouse
                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <!-- Satisfaction -->
                                                                <li>
                                                                    <span>100% Satisfaction Guarantee</span>
                                                                    <div>
                                                        <span>
                                                            We will cancel and refund your membership at any
                                                            time if you are dissatisfied
                                                        </span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <button aria-pressed="false" data-bi-title="see benefit details-3">
                                                                <span>See Benefit Details</span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-tabs-benefit-caret.png" alt="">
                                                            </button>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <strong>$60/Year</strong>
                                                            <small>Plus applicable sales tax</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- More Ways to Save row ----------------------------------------------------- -->
                                        <div class="row no-gutter rs-costco-membership-more-ways">
                                            <div class="col-lg-offset-1 col-lg-12">
                                                <h2>More Ways to Save</h2>
                                                <p>
                                                    Get the most out of your membership
                                                </p>

                                                <!-- Carousel (Personal) -->
                                                <div class="rs-costco-membership-jcarousel-wrapper rs-active">
                                                    <!-- Carousel controls (previous) -->
                                                    <a class="jcarousel-prev" href="#" aria-label="Previous Featured Services" role="button">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-control.png" alt="">
                                                    </a>

                                                    <!-- Actual carousel -->
                                                    <div class="jcarousel">
                                                        <ul style="left: 0px;">
                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="/costco-grocery-faq.html" data-bi-title="home delivery household supplies and essentials delivered right to your door.1" tabindex="0" aria-hidden="false">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-personal-home-delivery.png" alt="Tablet showing grocery items">
                                                        <span>
                                                            <span>Home Delivery</span>
                                                            <span>
                                                                <span>
                                                                    Household supplies and essentials delivered
                                                                    right to your door.<sup>1</sup>
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="/gasoline.html" data-bi-title="gas fill up your tank with high quality and low prices" tabindex="0" aria-hidden="false">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-personal-gas.png" alt="Costco brand fuel pump">
                                                        <span>
                                                            <span>Gas</span>
                                                            <span>
                                                                <span>
                                                                    Fill up your tank with high quality and
                                                                    low prices
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="/pharmacy/warehouse-pickup" data-bi-title="pharmacy fill new prescriptions or get a refill - at a warehouse or delivered to you." tabindex="0" aria-hidden="false">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-personal-pharmacy.png" alt="Costco letterhead with prescription pills">
                                                        <span>
                                                            <span>Pharmacy</span>
                                                            <span>
                                                                <span>
                                                                    Fill new prescriptions or get a refill -
                                                                    at a Warehouse or delivered to you.
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="/grocery-household.html" data-bi-title="grocery make a list and get fresh groceries with same-day delivery by instacart.1" tabindex="0" aria-hidden="false">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-personal-grocery.png" alt="Tablet with grocery items next to fresh vegetables">
                                                        <span>
                                                            <span>Grocery</span>
                                                            <span>
                                                                <span>
                                                                    Make a list and get fresh groceries with
                                                                    same-day delivery by
                                                                    Instacart.<sup>1</sup>
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="/auto-program-services.html" data-bi-title="costco auto program experience exceptional savings on new and select pre-owned vehicles.1" tabindex="-1" aria-hidden="true">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-personal-cap.png" alt="Audi SUV on mountain dirt road">
                                                        <span>
                                                            <span>Costco Auto Program</span>
                                                            <span>
                                                                <span>
                                                                    Experience exceptional savings on new
                                                                    and select pre-owned
                                                                    vehicles.<sup>1</sup>
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="https://www.costcotravel.com/Rental-Cars" data-bi-title="rental cars find and save on rental cars with our low price finder." tabindex="-1" aria-hidden="true">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-personal-rental-cars.png" alt="Rental cars lined up">
                                                        <span>
                                                            <span>Rental Cars</span>
                                                            <span>
                                                                <span>
                                                                    Find and save on rental cars with our Low Price
                                                                    Finder.
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="/pet-supplies.html" data-bi-title="pets everything your pet needs - food, meds, and toys." tabindex="-1" aria-hidden="true">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-personal-pets1.jpg" alt="Dog laying on the grass with Costco food court swag">
                                                        <span>
                                                            <span>Pets</span>
                                                            <span>
                                                                <span>
                                                                    Everything your pet needs - food, meds,
                                                                    and toys.
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="https://costcophotocenter.com/" data-bi-title="photo center relive your favorite moments with prints, photo books and more." tabindex="-1" aria-hidden="true">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-personal-photo-center1.png" alt="Printed photos hanging on wall">
                                                        <span>
                                                            <span>Photo Center</span>
                                                            <span>
                                                                <span>
                                                                    Relive your favorite moments with
                                                                    prints, photo books and more.
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <!-- Carousel controls (next) -->
                                                    <a class="jcarousel-next" href="#" aria-label="Next Featured Services" role="button">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-control.png" alt="">
                                                    </a>

                                                    <!-- Disclaimer -->
                                                    <small>
                                                        <sup>1</sup> Services provided by third party
                                                    </small>

                                                    <!-- Carousel pagination -->
                                                    <div class="jcarousel-pagination"><button aria-pressed="true" aria-label="Featured Services 1 of 2" class="rs-active"></button><button aria-pressed="false" aria-label="Featured Services 2 of 2"></button></div>
                                                </div>

                                                <!-- Carousel (Business) -->
                                                <div class="rs-costco-membership-jcarousel-wrapper">
                                                    <!-- Carousel controls (previous) -->
                                                    <a class="jcarousel-prev" href="#" aria-label="Previous Featured Services" role="button">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-control.png" alt="">
                                                    </a>

                                                    <!-- Actual carousel -->
                                                    <div class="jcarousel">
                                                        <ul>
                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="/merchant-account.html" data-bi-title="payment processing payment processing options tailored to your industry.1">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-business-payment-processing.png" alt="Customer playing via mobile device with payment terminal">
                                                        <span>
                                                            <span>Payment Processing</span>
                                                            <span>
                                                                <span>
                                                                    Payment processing options tailored to your
                                                                    industry.<sup>1</sup>
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="https://www.costcobusinessdelivery.com/" data-bi-title="business delivery get essentials delivered to your business.">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-business-business-delivery.png" alt="Man signing for delivery">
                                                        <span>
                                                            <span>Business Delivery</span>
                                                            <span>
                                                                <span>
                                                                    Get essentials delivered to your
                                                                    business.
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="/health-insurance-marketplace.html" data-bi-title="business health insurance businesses of all sizes can find the plan that fits their needs.1">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-business-health-insurance.png" alt="Stethoscope, writing pad, and laptop">
                                                        <span>
                                                            <span>Business Health Insurance</span>
                                                            <span>
                                                                <span>
                                                                    Businesses of all sizes can find the plan that
                                                                    fits their needs.<sup>1</sup>
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="/water-delivery-services.html" data-bi-title="bottled water delivery stock up for your home or office with exclusive pricing for members.1">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-business-bottled-water-delivery.png" alt="Water delivery man with water jug">
                                                        <span>
                                                            <span>Bottled Water Delivery</span>
                                                            <span>
                                                                <span>
                                                                    Stock up for your home or office with exclusive
                                                                    pricing for members.<sup>1</sup>
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="https://www.costcobusinessprinting.com/" data-bi-title="business printing find affordable, high-quality marketing materials for your business.">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-business-business-printing.png" alt="Stack of business cards">
                                                        <span>
                                                            <span>Business Printing</span>
                                                            <span>
                                                                <span>
                                                                    Find affordable, high-quality marketing
                                                                    materials for your business.
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="/check-services.html" data-bi-title="business &amp; personal checks order and reorder checks with security features and member-only savings.1">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-business-business-checks.png" alt="Business check">
                                                        <span>
                                                            <span>Business &amp; Personal Checks</span>
                                                            <span>
                                                                <span>
                                                                    Order and reorder checks with security
                                                                    features and member-only savings.<sup>1</sup>
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>

                                                            <!-- Slide -->
                                                            <li>
                                                                <a href="https://www.costcotravel.com/Rental-Cars" data-bi-title="rental cars quickly find and save on rental cars with our low price finder.">
                                                    <span class="rs-costco-membership-slide">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-business-rental-cars.png" alt="Man with luggage standing by a car">
                                                        <span>
                                                            <span>Rental Cars</span>
                                                            <span>
                                                                <span>
                                                                    Quickly find and save on rental cars with our
                                                                    Low Price Finder.
                                                                </span>
                                                                <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-caret-right.png" alt="">
                                                            </span>
                                                        </span>
                                                    </span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <!-- Carousel controls (next) -->
                                                    <a class="jcarousel-next" href="#" aria-label="Next Featured Services" role="button">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-carousel-control.png" alt="">
                                                    </a>

                                                    <!-- Disclaimer -->
                                                    <small>
                                                        <sup>1</sup> Services provided by third party
                                                    </small>

                                                    <!-- Carousel pagination -->
                                                    <div class="jcarousel-pagination"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- FAQ row ------------------------------------------------------------------- -->
                                        <div class="row rs-costco-membership-faq" style="margin-left:600px">
                                            <div class="col-lg-offset-2 col-lg-8 col-xl-offset-3 col-xl-6">
                                                <h2>Frequently Asked Questions</h2>

                                                <!-- FAQ row -->
                                                <div class="rs-costco-membership-faq-row">
                                                    <button aria-expanded="false" aria-controls="rs-costco-membership-faq-row1" data-bi-title="how do i qualify for the annual 2% reward and other executive membership privileges?">
                                                        <span>How do I qualify for the annual 2% Reward and other Executive Membership Privileges?</span>
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-plus.png" alt="">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-minus.png" alt="">
                                                    </button>
                                                    <div id="rs-costco-membership-faq-row1">
                                                        <p>
                                                            Executive Members receive an annual 2% Reward (up to $1,000) on
                                                            qualified Costco purchases. <a href="/executive-rewards.html" data-bi-title="terms, conditions, and exclusions apply.">Terms, conditions, and exclusions apply.</a>
                                                            The Executive Membership upgrade fee is an additional $60
                                                            a year for Business or Gold Star Members (plus sales tax where
                                                            applicable). We will prorate the upgrade amount based on the
                                                            months remaining in your current membership. Purchases made
                                                            prior to upgrading are not eligible for the 2% Reward. At your
                                                            next renewal, you will be billed a total of $120 for your
                                                            Executive Membership. To upgrade, visit the membership counter
                                                            at any Costco warehouse.
                                                        </p>
                                                    </div>
                                                </div>

                                                <!-- FAQ row -->
                                                <div class="rs-costco-membership-faq-row">
                                                    <button aria-expanded="false" aria-controls="rs-costco-membership-faq-row2" data-bi-title="how is my 2% reward calculated?">
                                                        <span>How is my 2% Reward calculated?</span>
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-plus.png" alt="">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-minus.png" alt="">
                                                    </button>
                                                    <div id="rs-costco-membership-faq-row2">
                                                        <p>
                                                            Costco Executive Members receive a 2% Reward on qualified
                                                            purchases (<a href="/executive-rewards.html" data-bi-title="see calculation of 2% reward">see calculation of 2% Reward</a>). Reward is capped
                                                            at and will not exceed $1,000 for any 12-month period. Only
                                                            purchases made by the Primary and active Primary Household
                                                            Cardholder on the account will apply toward the Reward. The
                                                            Reward is not guaranteed to be equal to or greater than the
                                                            Executive upgrade fee paid. Limit one Executive Membership per
                                                            household or business. Purchases made prior to upgrade do not
                                                            qualify for the 2% Reward. If, at any time, the Primary
                                                            Household Cardholder is removed from the account, their
                                                            purchases will not apply toward the Reward.
                                                        </p>
                                                    </div>
                                                </div>

                                                <!-- FAQ row -->
                                                <div class="rs-costco-membership-faq-row">
                                                    <button aria-expanded="false" aria-controls="rs-costco-membership-faq-row3" data-bi-title="what is the 100% satisfaction guarantee?">
                                                        <span>What is the 100% Satisfaction Guarantee?</span>
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-plus.png" alt="">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-minus.png" alt="">
                                                    </button>
                                                    <div id="rs-costco-membership-faq-row3">
                                                        <p>
                                                            We are committed to providing quality and value on the products
                                                            we sell with a risk-free 100% satisfaction guarantee on both
                                                            your membership and merchandise. <a href="/member-privileges-conditions.html" data-bi-title="limited exceptions apply.">Limited exceptions apply.</a>
                                                            See return policy for more information.
                                                        </p>
                                                    </div>
                                                </div>

                                                <!-- FAQ row -->
                                                <div class="rs-costco-membership-faq-row">
                                                    <button aria-expanded="false" aria-controls="rs-costco-membership-faq-row4" data-bi-title="what is costco travel?">
                                                        <span>What is Costco Travel?</span>
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-plus.png" alt="">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-minus.png" alt="">
                                                    </button>
                                                    <div id="rs-costco-membership-faq-row4">
                                                        <p>
                                                            Costco Travel offers a selection of the world's top
                                                            destinations, hotels, cruises and other travel products which
                                                            are sold exclusively to Costco Members who reside in the United
                                                            States. They use their buying authority to negotiate the best
                                                            value in the marketplace and pass the savings to Costco members.
                                                            Plus, when Executive Members purchase directly from Costco
                                                            Travel, the purchase is included in the Executive Membership
                                                            annual 2% Reward calculation. <a href="https://www.costcotravel.com" data-bi-title="visit costco travel for additional details/exclusions.">Visit Costco Travel for additional details/exclusions.</a>
                                                        </p>

                                                    </div>
                                                </div>

                                                <!-- FAQ row -->
                                                <div class="rs-costco-membership-faq-row">
                                                    <button aria-expanded="false" aria-controls="rs-costco-membership-faq-row5" data-bi-title="what are costco services?">
                                                        <span>What are Costco Services?</span>
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-plus.png" alt="">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-minus.png" alt="">
                                                    </button>
                                                    <div id="rs-costco-membership-faq-row5">
                                                        <p>
                                                            <a href="/services.html" data-bi-title="costco services">Costco Services</a> works with select providers to offer member-only values for
                                                            your home or business. Home Services include Auto &amp; Home Insurance, Costco
                                                            Auto Program, Bottled Water Delivery, Pet Insurance, Identity Protection,
                                                            Mortgage Purchase &amp; Refinancing and more. Business Services include Payment
                                                            Processing, Health Insurance Marketplace, Business Checks, Forms and
                                                            Accessories, Business Phones and more.
                                                        </p>
                                                    </div>
                                                </div>

                                                <!-- FAQ row -->
                                                <div class="rs-costco-membership-faq-row">
                                                    <button aria-expanded="false" aria-controls="rs-costco-membership-faq-row6" data-bi-title="what is the costco connection magazine?">
                                                        <span>What is the <em>Costco Connection</em> magazine?</span>
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-plus.png" alt="">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-minus.png" alt="">
                                                    </button>
                                                    <div id="rs-costco-membership-faq-row6">
                                                        <p>
                                                            The <a href="/costco-connection-online-edition.html" data-bi-title="costco connection magazine"><em>Costco Connection</em> magazine</a> is your connection to all things
                                                            Costco, including stories and more to help you get the most of
                                                            your Costco membership. All Executive Members receive the
                                                            magazine as a benefit of their membership.
                                                        </p>
                                                    </div>
                                                </div>

                                                <!-- FAQ row -->
                                                <div class="rs-costco-membership-faq-row">
                                                    <button aria-expanded="false" aria-controls="rs-costco-membership-faq-row7" data-bi-title="can i shop at all costco warehouses with my membership?">
                                                        <span>Can I shop at all Costco warehouses with my membership?</span>
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-plus.png" alt="">
                                                        <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-faq-minus.png" alt="">
                                                    </button>
                                                    <div id="rs-costco-membership-faq-row7">
                                                        <p>
                                                            Yes. You can use your membership card at any Costco warehouse
                                                            worldwide and online at Costco.com
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Citi Costco row (version 2) ----------------------------------------------- -->
                                        <div class="row rs-costco-membership-citi-costco-version2">
                                            <div class="col-xs-12">
                                                <a href="https://citicards.citi.com/usc/LPACA/Costco/cards/Dual/PartnerPS/index.html?promocode=Membership58K&amp;BT_TX=1&amp;ProspectID=08AB01C936924320BC3532190E0A5C82" class="external">
                                                    <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/citi-desktop.png" alt="Maximize your membership. Get all the Costco benefits you already love, plus earn cash back rewards everywhere you shop with the Costco Anywhere Visa Card by Citi.">
                                                    <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/citi-mobile.png" alt="Maximize your membership. Get all the Costco benefits you already love, plus earn cash back rewards everywhere you shop with the Costco Anywhere Visa Card by Citi.">
                                                </a>
                                            </div>
                                        </div>

                                        <!-- Promotions row ------------------------------------------------------------ -->
                                        <div class="row no-gutter rs-costco-membership-promotions">
                                            <div class="col-lg-offset-1 col-lg-12">
                                                <!-- Title -->
                                                <h2>New Member Promotions</h2>
                                                <p>
                                                    <strong>
                                                        Join Costco as a new member, enroll in auto renewal and receive a Costco Shop
                                                        Card
                                                    </strong>
                                                </p>

                                                <!-- Card row -->
                                                <div class="row">

                                                    <!-- Card -->
                                                    <div class="col-lg-4">
                                                        <a href="https://hosted-pages.id.me/offers/costco" class="external" data-bi-title="medical professionals">
                                                            <div class="rs-costco-membership-promotions-card">
                                                                <img class="img-responsive" src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-medical3.jpg" alt="">
                                                                <div>
                                                                    <span>Medical Professionals</span>
                                                                    <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-caret-right2.png" alt="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>

                                                    <!-- Card -->
                                                    <div class="col-lg-4">
                                                        <a href="https://hosted-pages.id.me/costco-military-offer" class="external" data-bi-title="military">
                                                            <div class="rs-costco-membership-promotions-card">
                                                                <img class="img-responsive" src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-military3.jpg" alt="">
                                                                <div>
                                                                    <span>Military</span>
                                                                    <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-caret-right2.png" alt="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>

                                                    <!-- Card -->
                                                    <div class="col-lg-4">
                                                        <a href="https://www.myunidays.com/US/en-US/partners/costco/micro/online" class="external" data-bi-title="college students">
                                                            <div class="rs-costco-membership-promotions-card">
                                                                <img class="img-responsive" src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-students4.jpg" alt="">
                                                                <div>
                                                                    <span>College Students</span>
                                                                    <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-caret-right2.png" alt="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>

                                                    <!-- Card -->
                                                    <div class="col-lg-4">
                                                        <a href="https://hosted-pages.id.me/costco-doctcom-first-responder-promotion" class="external" data-bi-title="first responders">
                                                            <div class="rs-costco-membership-promotions-card">
                                                                <img class="img-responsive" src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-first-responders1.jpg" alt="">
                                                                <div>
                                                                    <span>First Responders</span>
                                                                    <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-caret-right2.png" alt="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>

                                                    <!-- Card -->
                                                    <div class="col-lg-4">
                                                        <a href="https://hosted-pages.id.me/costco-government" class="external" data-bi-title="government employees">
                                                            <div class="rs-costco-membership-promotions-card">
                                                                <img class="img-responsive" src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-government.jpg" alt="">
                                                                <div>
                                                                    <span>Government Employees</span>
                                                                    <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-caret-right2.png" alt="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>

                                                    <!-- Card -->
                                                    <div class="col-lg-4">
                                                        <a href="https://hosted-pages.id.me/costco-teachers-offer" class="external" data-bi-title="teachers">
                                                            <div class="rs-costco-membership-promotions-card">
                                                                <img class="img-responsive" src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-teachers2.jpg" alt="">
                                                                <div>
                                                                    <span>Teachers</span>
                                                                    <img src="https://mobilecontent.costco.com/live/resource/img/static-us-landing-pages/rs-membership-promotions-caret-right2.png" alt="">
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Disclaimers row -->
                                        <div class="row rs-costco-membership-disclaimers">
                                            <div class="col-lg-offset-1 col-lg-12">
                                                <p>
                                                    eMetro New Member Promotion Terms and Conditions
                                                </p>
                                                <small>
                                                    <strong>To receive the maximum savings, you must join and enroll in auto renewal of your eMetro membership card at the time of sign-up.
                                                        Promotion is valid for new memberships in the first year of membership when enrolled in auto renewal at time of sign-up.
                                                        Not valid for renewal or upgrade of an existing membership. Previous eMetro members may qualify as a new member only if they
                                                        have not paid membership fees for the past 30 months.
                                                        eMetro Shop Card is not redeemable for cash, except as required by law.
                                                    <br>
                                                    <br>


                                                    <br>
                                                    <a href="/privacy-policy.html" data-bi-title="costco wholesale corporation your privacy rights">Costco Wholesale Corporation Your Privacy Rights</a>
                                                </small>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- End RS wrapper ---------------------------------------------------------------- -->

                                    <!-- External scripts -->
                                    <!-- -->

                                    <!-- Page body scripts -->
                                    <script>

                                        /*********
                                         * GLOBALS -------------------------------------------------------------------------
                                         *********/

                                        var RS_ACTIVE_CLASS = 'rs-active';
                                        var RS_TABS_PARENT_SELECTOR = '.rs-costco-membership-membership-tabs-parent';
                                        var RS_TABS_CHILD_SELECTOR = '.rs-costco-membership-membership-tabs-child';
                                        var RS_TABS_CHILD_TABPANE_SELECTOR = '.rs-costco-membership-membership-tabs-child-tabpane';
                                        var RS_TABS_CHILD_TABPANE_LIST_BUTTON_SELECTOR = RS_TABS_CHILD_TABPANE_SELECTOR + ' > div:nth-child(3) > button';
                                        var RS_FAQ_ROW_SELECTOR = '.rs-costco-membership-faq-row > button';
                                        var RS_PB_CAROUSEL_SELECTOR = '.rs-costco-membership-more-ways .rs-costco-membership-jcarousel-wrapper';
                                        var RS_CAROUSEL_INDEX = 0;
                                        var RS_TOUCHSTART_X = 0;
                                        var RS_TOUCHEND_X = 0;

                                        // Query parameter passthrough
                                        var RS_A_SELECTOR =                 '#rs-costco-membership-wrapper a:not([role="button"])';
                                        var RS_PASSTHROUGH_ALLOWED_PREFIX = ['utm'];

                                        // Anchors
                                        var RS_HASH_PROMOTIONS = '#Promotions';

                                        /***********
                                         * FUNCTIONS -----------------------------------------------------------------------
                                         ***********/

                                        /**
                                         * Click handler for parent "Membership" tabs.
                                         */
                                        function rsMembershipTabsParentOnClick() {
                                            var parentTabsButtons = $(RS_TABS_PARENT_SELECTOR).find('button');
                                            parentTabsButtons.on('click', function() {
                                                var currentParentTab = $(this);
                                                var currentParentTabIndex = currentParentTab.index();

                                                // Parent tab active class
                                                currentParentTab.siblings().removeClass(RS_ACTIVE_CLASS);
                                                currentParentTab.siblings().attr('aria-pressed', 'false');
                                                currentParentTab.addClass(RS_ACTIVE_CLASS);
                                                currentParentTab.attr('aria-pressed', 'true');

                                                // Child tab active class
                                                var childTabs = $(RS_TABS_CHILD_SELECTOR);
                                                childTabs.removeClass(RS_ACTIVE_CLASS);
                                                var currentChildTab = childTabs.eq(currentParentTabIndex);
                                                currentChildTab.addClass(RS_ACTIVE_CLASS);

                                                // Child tab buttons
                                                var currentChildTabButtons = currentChildTab.children('div:first-child').find('button');
                                                currentChildTabButtons.removeClass(RS_ACTIVE_CLASS);
                                                currentChildTabButtons.attr('aria-pressed', 'false');
                                                currentChildTabButtons.eq(0).addClass(RS_ACTIVE_CLASS);
                                                currentChildTabButtons.eq(0).attr('aria-pressed', 'true');

                                                // Child tab panes
                                                var currentChildTabPanes = currentChildTab.find(RS_TABS_CHILD_TABPANE_SELECTOR);
                                                currentChildTabPanes.removeClass(RS_ACTIVE_CLASS);
                                                currentChildTabPanes.eq(0).addClass(RS_ACTIVE_CLASS);

                                                // Child tab pane list benefits reset
                                                var listBenefits = currentChildTabPanes.find('li > div');
                                                listBenefits.css('display', 'none');

                                                // Tab pane list benefit button reset
                                                var listBenefitsButtons = currentChildTabPanes.find('ul + button');
                                                listBenefitsButtons.removeClass(RS_ACTIVE_CLASS);
                                                listBenefitsButtons.attr('aria-pressed', 'false');

                                                // Show Personal/Business carousels
                                                var pbCarousels = $(RS_PB_CAROUSEL_SELECTOR);
                                                pbCarousels.removeClass(RS_ACTIVE_CLASS);
                                                var currentCarousel = pbCarousels.eq(currentParentTabIndex);
                                                currentCarousel.addClass(RS_ACTIVE_CLASS);

                                                // Reset carousel
                                                rsResetCarousel();
                                            });
                                        }

                                        /**
                                         * Click handler for child "Membership" tabs.
                                         */
                                        function rsMembershipTabsChildOnClick() {
                                            var childTabButtons = $(RS_TABS_CHILD_SELECTOR).children('div:first-child').find('button');
                                            childTabButtons.on('click', function() {
                                                var currentButton = $(this);
                                                var currentButtonIndex = currentButton.index();

                                                // Tab button active class
                                                currentButton.siblings().removeClass(RS_ACTIVE_CLASS);
                                                currentButton.siblings().attr('aria-pressed', 'false');
                                                currentButton.addClass(RS_ACTIVE_CLASS);
                                                currentButton.attr('aria-pressed', 'true');

                                                // Tab pane active class
                                                var tabPanes = currentButton.closest(RS_TABS_CHILD_SELECTOR).find(RS_TABS_CHILD_TABPANE_SELECTOR);
                                                tabPanes.removeClass(RS_ACTIVE_CLASS);
                                                tabPanes.eq(currentButtonIndex).addClass(RS_ACTIVE_CLASS);

                                                // Tab pane list benefits reset
                                                var listBenefits = tabPanes.find('li > div');
                                                listBenefits.css('display', 'none');

                                                // Tab pane list benefit button reset
                                                var listBenefitsButtons = tabPanes.find('ul + button');
                                                listBenefitsButtons.removeClass(RS_ACTIVE_CLASS);
                                                listBenefitsButtons.attr('aria-pressed', 'false');
                                            });
                                        }

                                        /**
                                         * Click handler for expanding benefits list
                                         */
                                        function rsMembershipTabsSeeBenefitsOnClick() {
                                            var listButtons = $(RS_TABS_CHILD_TABPANE_LIST_BUTTON_SELECTOR);
                                            listButtons.on('click', function() {
                                                var currentButton = $(this);

                                                // Button active class
                                                currentButton.toggleClass(RS_ACTIVE_CLASS);
                                                currentButton.attr('aria-pressed', 'true');

                                                // Show/hide list details
                                                var ul = currentButton.siblings('ul');
                                                var divs = ul.find('li > div');
                                                divs.slideToggle();

                                                // Focus first <li> on open (this is paired with tabindex="-1")
                                                if (currentButton.hasClass(RS_ACTIVE_CLASS)) {
                                                    var firstLi = currentButton.siblings('ul').children().first();
                                                    firstLi.focus();
                                                }
                                            });
                                        }

                                        /**
                                         * Helper function for slide toggling an FAQ row
                                         * @param  element
                                         */
                                        function rsFAQOpenRow(element) {
                                            var currentButton = $(element);

                                            // ARIA expanded
                                            var expanded = currentButton.attr('aria-expanded');
                                            expanded = (expanded === 'true') ? 'false' : 'true';
                                            currentButton.attr('aria-expanded', expanded);

                                            // jQuery slide content
                                            currentButton.siblings('div').slideToggle();
                                        }

                                        /**
                                         * Click handler for FAQ rows (THIS ALSO HANDLES SPACEBAR!!!)
                                         */
                                        function rsFAQOnClick() {
                                            var faqButtons = $(RS_FAQ_ROW_SELECTOR);
                                            faqButtons.on('click', function(e) {
                                                e.preventDefault();
                                                rsFAQOpenRow(this);
                                            });
                                        }

                                        /**
                                         * Keypress handler for FAQ rows
                                         */
                                        function rsFAQOnKeyPress() {
                                            var faqButtons = $(RS_FAQ_ROW_SELECTOR);
                                            faqButtons.on('keypress', function(e) {
                                                if (e.which === 13) {
                                                    e.preventDefault();
                                                    rsFAQOpenRow(this);
                                                }
                                            });
                                        }

                                        /**
                                         * Animate the carousel one slide. IMPORTANT: this assumes equal width slides!!!
                                         *
                                         * @param  $carousel jQuery carousel element
                                         * @param  moveIndex Desired index to move to
                                         */
                                        function rsMoveCarousel($carousel, moveIndex) {
                                            // Variables
                                            var carouselUL = $carousel.children('ul').first();
                                            var carouselULOffset = parseInt(carouselUL.css('left'), 10);
                                            var slides = carouselUL.children('li');
                                            var slideWidth = slides.first().width();
                                            var slideCount = slides.length;

                                            // Last valid slide index
                                            var carouselWidth = $carousel.width();
                                            var pageSize = Math.floor(carouselWidth / slideWidth);
                                            var subPageSize = false; // This is all range check if slides are bigger than viewport
                                            if (pageSize < 1) {
                                                pageSize = 1;
                                                subPageSize = true;
                                            }
                                            var lastValidIndex = slideCount - pageSize;

                                            // Clamp to valid index
                                            if (moveIndex < 0) {
                                                moveIndex = 0;
                                            } else if (moveIndex > lastValidIndex) {
                                                moveIndex = lastValidIndex;
                                            }

                                            // Update slide index
                                            RS_CAROUSEL_INDEX = moveIndex;

                                            // Calculate active page
                                            var activePageIndex = Math.floor(RS_CAROUSEL_INDEX / pageSize);
                                            var isLastPage = (RS_CAROUSEL_INDEX === lastValidIndex) ? true : false;

                                            // Update active page
                                            var paginationItems = $carousel.siblings('.jcarousel-pagination').children('button');
                                            paginationItems.each(function(index, element) {
                                                var $element = $(element);

                                                var activeCurrent = (index === activePageIndex && !isLastPage);
                                                var activeLast = (index === (paginationItems.length - 1) && isLastPage);

                                                if (activeCurrent || activeLast) {
                                                    $element.addClass(RS_ACTIVE_CLASS);
                                                    $element.attr('aria-pressed', 'true');
                                                } else {
                                                    $element.removeClass(RS_ACTIVE_CLASS);
                                                    $element.attr('aria-pressed', 'false');
                                                }
                                            });

                                            // Calculate animation offset
                                            var offset = RS_CAROUSEL_INDEX * slideWidth * -1;

                                            // Clamp to last slide
                                            if (RS_CAROUSEL_INDEX === lastValidIndex && !subPageSize) {
                                                var firstSlide = carouselUL.children('li').first();
                                                var firstSlideLeft = firstSlide.offset().left;
                                                var lastSlide = carouselUL.children('li').last();
                                                var lastSlideRight = lastSlide.offset().left + slideWidth;
                                                offset = -(lastSlideRight - firstSlideLeft) + $carousel.width();
                                            }

                                            // Actual animation
                                            carouselUL.animate({
                                                'left': offset + "px"
                                            });

                                            // Recalculate pagination (for the sake of dynamic "tabindex")
                                            rsGeneratePagination();
                                        }

                                        /**
                                         * Recreate pagination dots anew.
                                         */
                                        function rsGeneratePagination() {
                                            var activeCarousel = $(RS_PB_CAROUSEL_SELECTOR + '.' + RS_ACTIVE_CLASS).children('.jcarousel').first();
                                            var carouselUL = activeCarousel.children('ul').first();
                                            var carouselSlides = carouselUL.children('li');
                                            var slideWidth = carouselSlides.first().width();
                                            var slideCount = carouselSlides.length;

                                            // Get index of last slide visible
                                            var carouselWidth = activeCarousel.width();
                                            var fullSlidesShown = Math.floor(carouselWidth / slideWidth);
                                            var pageCount = Math.ceil(slideCount / fullSlidesShown);

                                            // Range checks
                                            if (fullSlidesShown < 1) {
                                                fullSlidesShown = 1;
                                            }
                                            if (pageCount > slideCount) {
                                                pageCount = slideCount
                                            }

                                            // Reset pagination
                                            var pagination = activeCarousel.siblings('.jcarousel-pagination').first();
                                            pagination.empty();

                                            // Active page
                                            var activePageIndex = Math.floor(RS_CAROUSEL_INDEX / fullSlidesShown);

                                            // Create new pagination items
                                            for (var i = 0; i < pageCount; i++) {
                                                var paginationButton = $(document.createElement('button'));
                                                paginationButton.attr('aria-pressed', 'false');
                                                paginationButton.attr('aria-label', 'Featured Services ' + (i+1) + ' of ' + pageCount);

                                                // Click handler
                                                paginationButton.on('click', function(e) {
                                                    e.preventDefault();
                                                    var buttonIndex = $(this).index();
                                                    var moveIndex = fullSlidesShown * buttonIndex;
                                                    rsMoveCarousel(activeCarousel, moveIndex);

                                                    // Manual focus
                                                    var activeCarousel2 = $(RS_PB_CAROUSEL_SELECTOR + '.' + RS_ACTIVE_CLASS).children('.jcarousel').first();
                                                    var firstVisibleSlide = activeCarousel2.find('a[tabindex="0"]').first();
                                                    firstVisibleSlide.focus();
                                                });

                                                // Set active page
                                                if (i === activePageIndex) {
                                                    paginationButton.addClass(RS_ACTIVE_CLASS);
                                                    paginationButton.attr('aria-pressed', 'true');
                                                }

                                                // Add to DOM
                                                paginationButton.appendTo(pagination);
                                            }

                                            // Dynamic "tabindex" for visible slides only
                                            var carouselSlidesLinks = carouselSlides.children('a');
                                            carouselSlidesLinks.attr('tabindex', '-1');
                                            carouselSlidesLinks.attr('aria-hidden', 'true'); // Ensure screen readers can't see
                                            carouselSlidesLinks.each(function(index, element) {
                                                if (index >= RS_CAROUSEL_INDEX && index < (RS_CAROUSEL_INDEX + fullSlidesShown)) {
                                                    $(element).attr('tabindex', '0');
                                                    $(element).attr('aria-hidden', 'false');
                                                }
                                            });
                                        }

                                        /**
                                         * Reset the currently active carousel back to initial state
                                         */
                                        function rsResetCarousel() {
                                            // Variables
                                            var activeCarousel = $(RS_PB_CAROUSEL_SELECTOR + '.' + RS_ACTIVE_CLASS).children('.jcarousel').first();
                                            var carouselUL = activeCarousel.children('ul').first();

                                            // Reset animation offset
                                            carouselUL.css('left', '0px');

                                            // Reset index
                                            RS_CAROUSEL_INDEX = 0;

                                            // Reset pagination
                                            rsGeneratePagination();
                                        }

                                        /**
                                         * Init jCarousel
                                         */
                                        function rsInitCarouselControls() {
                                            // Previous button
                                            $(RS_PB_CAROUSEL_SELECTOR).find('.jcarousel-prev').on('click', function(e) {
                                                e.preventDefault();
                                                var control = $(this);
                                                var carousel = control.siblings('.jcarousel').first();
                                                rsMoveCarousel(carousel, RS_CAROUSEL_INDEX - 1);

                                                // Manual focus
                                                var activeCarousel2 = $(RS_PB_CAROUSEL_SELECTOR + '.' + RS_ACTIVE_CLASS).children('.jcarousel').first();
                                                var firstVisibleSlide = activeCarousel2.find('a[tabindex="0"]').first();
                                                firstVisibleSlide.focus();
                                            });

                                            // Next Button
                                            $(RS_PB_CAROUSEL_SELECTOR).find('.jcarousel-next').on('click', function(e) {
                                                e.preventDefault();
                                                var control = $(this);
                                                var carousel = control.siblings('.jcarousel').first();
                                                rsMoveCarousel(carousel, RS_CAROUSEL_INDEX + 1);

                                                // Manual focus
                                                var activeCarousel2 = $(RS_PB_CAROUSEL_SELECTOR + '.' + RS_ACTIVE_CLASS).children('.jcarousel').first();
                                                var lastVisibleSlide = activeCarousel2.find('a[tabindex="0"]').last();
                                                lastVisibleSlide.focus();
                                            });
                                        }

                                        /**
                                         * Super simple swipe handler to move carousel
                                         */
                                        function rsInitCarouselSwipe() {
                                            // Swipe on <li> to avoid conflicts with controls
                                            var carouselLI = $(RS_PB_CAROUSEL_SELECTOR).find('.jcarousel li');

                                            // Touchstart --------------------------------------
                                            carouselLI.on('touchstart', function(e) {
                                                // Store initial coordinate
                                                RS_TOUCHSTART_X = e.originalEvent.changedTouches[0].screenX;
                                            });

                                            // Touchend (move carousel) ------------------------
                                            carouselLI.on('touchend', function(e) {
                                                // Store final coordinate
                                                RS_TOUCHEND_X = e.originalEvent.changedTouches[0].screenX;

                                                // Calculate distance to avoid click conflict
                                                var dist = Math.abs(RS_TOUCHEND_X - RS_TOUCHSTART_X);

                                                // Have to swipe at least 30px (arbitrary)
                                                if (dist > 30) {
                                                    // Swipe left (next slide)
                                                    var moveIndex = null;
                                                    if (RS_TOUCHEND_X < RS_TOUCHSTART_X) {
                                                        moveIndex = RS_CAROUSEL_INDEX + 1;
                                                    }

                                                    // Swipe left (prevous slide)
                                                    else if (RS_TOUCHEND_X > RS_TOUCHSTART_X) {
                                                        moveIndex = RS_CAROUSEL_INDEX - 1;
                                                    }

                                                    // Move only if in mobile
                                                    var windowWidth = $(window).width();
                                                    if (windowWidth <= 768 && moveIndex !== null) {
                                                        var activeCarousel = $(RS_PB_CAROUSEL_SELECTOR + '.' + RS_ACTIVE_CLASS).children('.jcarousel').first();
                                                        rsMoveCarousel(activeCarousel, moveIndex);
                                                    }
                                                }
                                            });
                                        }

                                        // Query parameter passthrough -------------------------
                                        /**
                                         * Convert a URL parameter string into a key/value object.  NOTE this function
                                         * expects a leading "?".
                                         *
                                         * @param  {string} queryString
                                         * @param  {boolean} isPageUrl
                                         * @return  Object containing query parameter key/value pairs
                                         */
                                        function rsExtractQueryParams(queryString, isPageUrl) {
                                            var result = {};

                                            // Values exist
                                            if (queryString !== "") {
                                                result = queryString.substring(1); // Remove leading "?"
                                                result = '{"' + result.replace(/&/g, '","').replace(/=/g, '":"') + '"}';

                                                // Handle syntax error so page continues rendering
                                                try {
                                                    result = JSON.parse(result, function (key, value) {return key === "" ? value : decodeURIComponent(value)});
                                                }

                                                    // Reset result to empty
                                                catch (error) {
                                                    // If page URL is malformed, pretend there is no query string
                                                    if (isPageUrl) {
                                                        result = {};
                                                        console.error('PAGE URL QUERY STRING PARSE EXCEPTION!');
                                                    }

                                                    // This means that a CTA URL is malformed, but needs passthrough logic still
                                                    else {
                                                        throw error; // Rethrow
                                                    }
                                                }
                                            }

                                            return result;
                                        }

                                        /**
                                         * Filter the given query parameters aginast the approved list of query parameters.
                                         *
                                         * NOTE: matching is done where an allowed query parameter MUST be the start of a
                                         * page query parameter.  Matching is done on an case-insenstive basis.
                                         */
                                        function rsFilterPageUrlParams(pageParams) {
                                            var result = {};

                                            // Loop page params
                                            var pageParamKeys = Object.keys(pageParams);
                                            for (var i = 0; i < pageParamKeys.length; i++) {
                                                var currentParamKey = pageParamKeys[i];

                                                // Loop allowed prefixes
                                                for (var j = 0; j < RS_PASSTHROUGH_ALLOWED_PREFIX.length; j++) {
                                                    var currentAllowedPrefix = RS_PASSTHROUGH_ALLOWED_PREFIX[j];

                                                    // Convert both to lowercase (case-insensitive matching)
                                                    var currentParamKeyLowerCase = currentParamKey.toLowerCase();
                                                    var currentAllowedPrefixLowerCase = RS_PASSTHROUGH_ALLOWED_PREFIX[j].toLowerCase();

                                                    // Page param starts with prefix
                                                    if (currentParamKeyLowerCase.indexOf(currentAllowedPrefixLowerCase) === 0) {
                                                        result[currentParamKey] = pageParams[currentParamKey];

                                                        // Next page param
                                                        break;
                                                    }
                                                }
                                            }

                                            return result;
                                        }

                                        /**
                                         * Build new query string.
                                         *
                                         * NOTE: The returned string will always have a leading question mark.
                                         */
                                        function rsBuildHrefQuery(params) {
                                            var result = '?';

                                            // Loop params
                                            var paramKeys = Object.keys(params);
                                            for (var i = 0; i < paramKeys.length; i++) {
                                                var key = paramKeys[i];
                                                var value = encodeURIComponent(params[key]);

                                                // Append pair
                                                result += key;
                                                result += '=';
                                                result += value;

                                                // Next pair
                                                if (i < (paramKeys.length - 1)) {
                                                    result += '&';
                                                }
                                            }

                                            return result;
                                        }

                                        /**
                                         * Takes the given allowed page URL query parameters and does the following:
                                         *  1) If the query parameter exists in an <a> "href", then override the value
                                         *  2) If the query parameter doesn't exist in an <a> "href", then append it.
                                         *
                                         * Currently allowed page URL query parameters (controlled by RS global variable):
                                         *  1) utm
                                         *  2) extid
                                         *
                                         * NOTES:
                                         *  1) Allowed parameters above must pass "starts with".
                                         *  2) Matching is case-insensitive.
                                         */
                                        function rsQueryParameterPassthrough() {
                                            // Get page URL query parameters
                                            var pageUrlParams = rsExtractQueryParams(location.search, true);

                                            // Page parameters present
                                            if (Object.keys(pageUrlParams).length > 0) {
                                                // Keep only allowed params
                                                pageUrlParams = rsFilterPageUrlParams(pageUrlParams);

                                                // Target RS <a> elements
                                                $(RS_A_SELECTOR).each(function() {
                                                    var currentLink = $(this);
                                                    var currentLinkHref = currentLink.attr('href');
                                                    var currentLinkParams = currentLinkHref;

                                                    // Extract current link params -----------------------------------------
                                                    var currentLinkParamsIndex = currentLinkParams.indexOf('?');
                                                    if (currentLinkParamsIndex === -1) {
                                                        currentLinkParams = {};
                                                    } else {
                                                        currentLinkParams = currentLinkParams.substring(currentLinkParamsIndex);
                                                        currentLinkParams = rsExtractQueryParams(currentLinkParams, false);
                                                    }

                                                    // Variables
                                                    var newParams = {};
                                                    var pageParamKeys = Object.keys(pageUrlParams);
                                                    var currentLinkParamKeys = Object.keys(currentLinkParams);

                                                    // Special override array
                                                    var pageParamKeysUsed = [];
                                                    for (var i = 0; i < pageParamKeys.length; i++) {
                                                        pageParamKeysUsed.push(false);
                                                    }

                                                    // Loop link params (override) -----------------------------------------
                                                    for (var i = 0; i < currentLinkParamKeys.length; i++) {
                                                        var currentLinkParamKey = currentLinkParamKeys[i];
                                                        var currentLinkParamValue = currentLinkParams[currentLinkParamKey];

                                                        // Override link param with applicable page param
                                                        for (var j = 0; j < pageParamKeys.length; j++) {
                                                            var currentPageParamKey = pageParamKeys[j];
                                                            var currentPageParamValue = pageUrlParams[currentPageParamKey];

                                                            // Convert keys to lowercase (case-insensitive matching)
                                                            var linkLowerCase = currentLinkParamKey.toLowerCase();
                                                            var pageLowerCase = currentPageParamKey.toLowerCase();

                                                            // Match
                                                            if (linkLowerCase === pageLowerCase) {
                                                                // Link key with page value
                                                                currentLinkParamValue = currentPageParamValue;

                                                                // Mark page param used
                                                                pageParamKeysUsed[j] = true;
                                                            }
                                                        }

                                                        // Add new link key
                                                        newParams[currentLinkParamKey] = currentLinkParamValue;
                                                    }

                                                    // Append any unused page params (append) ------------------------------
                                                    for (var i = 0; i < pageParamKeys.length; i++) {
                                                        var pageParamKey = pageParamKeys[i];
                                                        var pageParamValue = pageUrlParams[pageParamKey];

                                                        // Not used
                                                        var isUsed = pageParamKeysUsed[i];
                                                        if (!isUsed) {
                                                            newParams[pageParamKey] = pageParamValue;
                                                        }
                                                    }

                                                    // Assemble new query string -------------------------------------------
                                                    var newHrefQuery = rsBuildHrefQuery(newParams);

                                                    // Get href domain
                                                    var newHref = (currentLinkParamsIndex === -1) ? currentLinkHref : currentLinkHref.substring(0, currentLinkParamsIndex);

                                                    // Append new query string
                                                    newHref += newHrefQuery;

                                                    // Set new href
                                                    currentLink.attr('href', newHref);
                                                });
                                            }
                                        }

                                        /**
                                         * Jump to Promotions
                                         */
                                        function rsHandleHash() {
                                            if (window.location.hash === RS_HASH_PROMOTIONS) {
                                                // Top offset
                                                var selector = '.rs-costco-membership-promotions';
                                                var top = $(selector).offset().top;

                                                // Fast animate there
                                                $('html, body').animate(
                                                    {
                                                        "scrollTop": top
                                                    },
                                                    1 // Instantly, but not zero
                                                );
                                            }
                                        }

                                        /**********************
                                         * JQUERY PAGE HANDLERS ------------------------------------------------------------
                                         **********************/

                                        // DOM ready
                                        $(document).ready(function() {
                                            rsMembershipTabsParentOnClick();
                                            rsMembershipTabsChildOnClick();
                                            rsMembershipTabsSeeBenefitsOnClick();
                                            rsFAQOnClick();
                                            rsFAQOnKeyPress();
                                            rsInitCarouselControls();
                                            rsInitCarouselSwipe();

                                            // Query parameter passthrough
                                            rsQueryParameterPassthrough();
                                        });

                                        // Window load
                                        $(window).on('load', function() {
                                            rsResetCarousel();
                                            setTimeout(rsHandleHash, 500); // Safari workaround for window.load garbageware
                                        });

                                        // Window resize
                                        $(window).resize(function() {
                                            rsResetCarousel();
                                        });
                                    </script>
                                </div><!-- Display the actual text ENDS here --><!-- End - CostcoGLOBALSAS\Snippets\Marketing\ESpot\RenderRwdESpotContent.jspf --><!-- DEBUG SearchProvider: lucidworks Order of Catentry IDs from CMC: [] --><!-- END ContentAreaRwdESpot.jsp fp-join-costco_Slot 1 --><!-- END LayoutBuilder_eSpot.jsp -->
                            </div>

                        </div>
                        <script type="text/javascript">

                            function showOverflowLink() {
                                window.COSTCO.Search.showOverflowLink();
                            }

                            // display 'show more' link in category description espot if needed
                            if ($('.ellipsis-overflow').length > 0) {

                                $( document ).ready(function() {
                                    if(costcoloadstate.submodules){
                                        showOverflowLink();
                                    }else{
                                        document.addEventListener("CostcoSubModulesLoaded", showOverflowLink, false);
                                    }


                                });
                                $(window).resize(function() {
                                    if(costcoloadstate.submodules){
                                        showOverflowLink();
                                    }
                                });
                            }
                        </script>
                    </div><div id="syndi_powerpage" role="main" style="margin-bottom: 19px;"></div>

                    <div class="criteo-override">


                        <div id="sponsored-products">

                            <!-- This comment is necessary to avoid caching bug --><!-- BEGIN ContentAreaRwdESpot.jsp Criteo_Category_Espot --><!-- Debugging Purpose ... USE_SEARCH_BASED_CATENTRY_ESPOTS Flag Value :  1 --><!-- DEBUG SearchProvider:  Order of Catentry IDs from CMC: [] --><!-- END ContentAreaRwdESpot.jsp Criteo_Category_Espot -->
                        </div>




                        <script type="text/javascript" class="optanon-category-C0004">
                            function getCriteoUniversalParams() {
                                var uParams ={
                                    scriptSrc : '//dynamic.criteo.com/js/ld/ld.js?a=48266',
                                    uat : 'false',
                                    account : '48266',
                                    retailerVisitorId : COSTCO.COT.getRetailerVisitorId(),
                                    email : COSTCO.COT.getEmail()
                                }
                                return uParams;
                            }

                            function loadCriteo() {
                                var eventSpecificParams = {
                                    event : 'viewCategory',
                                    item : ["filler"],
                                    pageId : COSTCO.util.isMobile() ? 'viewMerch_M' : 'viewMerch',
                                    pageNumber : '',
                                    keywords : '',
                                    category : '32502',
                                    filters : []
                                }
                                COSTCO.COT.fireEvent($.extend(eventSpecificParams, getCriteoUniversalParams()));
                            };

                            function loadCriteoAddToCartParams(partNumber, price, quantity){
                                var eventSpecificParams = {
                                    event : 'addToCart',
                                    itemId: partNumber,
                                    price: price,
                                    quantity: quantity,
                                    zipCode : '',
                                    storeId : '10301',
                                }
                                COSTCO.COT.fireEvent($.extend(eventSpecificParams, getCriteoUniversalParams()));
                            };

                            $('body').bind('costco.cart.item.added', function (event, data) {
                                loadCriteoAddToCartParams(data.partNumber, data.price, data.quantity);
                            });

                            try{
                                if(typeof COSTCO != "undefined" && COSTCO && typeof COSTCO.COT != "undefined" && COSTCO.COT && typeof COSTCO.util != "undefined" && COSTCO.util){
                                    loadCriteo();
                                }else {
                                    document.addEventListener("CostcoSubModulesLoaded", loadCriteo, false);
                                }

                            }catch(e){

                                if(costcoloadstate.submodules){
                                    loadCriteo();
                                }else {
                                    document.addEventListener("CostcoSubModulesLoaded", loadCriteo, false);
                                }
                            }

                        </script></div>
                    <!-- END LayoutBuilderComposeOnly.jspf -->
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/footer/join-now.blade.php ENDPATH**/ ?>