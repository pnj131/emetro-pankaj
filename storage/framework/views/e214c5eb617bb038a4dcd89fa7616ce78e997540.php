<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!--Header-->

<!-- Category Page Start  -->
<div class="breadcrumb">
	<div class="container">
		<ol class="breadcrumb-list">
			<li>
				<a href="/">
					<span>Home</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span>Jewelry, Watches &amp; Sunglasses</span>
				</a>
			</li>
			<li>
				<strong> Sunglasses </strong>
			</li>
		</ol>
	</div>
</div>
<div class="columns">
	<div class="column main container">
		<div class="page-title-wrapper">
			<h1 class="page-title">
				<span class="base">Sunglasses</span>
			</h1>
		</div>
		<div class="s-b-cat">
			<h2>Shop By Category</h2>
			<div class="s-b-cat-row">
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="/product">
								<img src="assest/images/MTR1.jpeg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="#" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="/product">
								<img src="assest/images/p5.jpg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="product.blade.php" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="product.blade.php">
								<img src="assest/images/p6.jpg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="product.blade.php" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="product.blade.php">
								<img src="assest/images/MTR5.jpeg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="#" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="product.blade.php">
								<img src="assest/images/p2.jpg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="product.blade.php" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="product.blade.php">
								<img src="assest/images/MTR24.jpeg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="#" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		 <div class="products products-grid">
			<ol class="product-items">
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/MTR1.jpeg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/p5.jpg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/p6.jpg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/MTR5.jpeg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/p2.jpg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/MTR24.jpeg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/MTR1.jpeg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/p5.jpg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/p6.jpg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/MTR5.jpeg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/p2.jpg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/MTR24.jpeg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
			</ol>
			<div class="pagination-cat">
				<div class="row justify-content-center">
					<div class="paging">
						<ul>
							<li class="back disabled prev">
								<i class="fas fa-chevron-left"></i>
							</li>
							<li class="page-item selected">
								<a class="page-item-l" href="#">
									<span>1</span>
								</a>
							</li>
							<li class="page-item">
								<a class="page-item-l" href="#">
									<span>2</span>
								</a>
							</li>
							<li class="page-item">
								<a class="page-item-l" href="#">
									<span>3</span>
								</a>
							</li>
							<li class="back disabled next">
								<i class="fas fa-chevron-right"></i>
							</li>
						</ul>
					</div>
					<div class="backtotop text-right">
						<a href="#">Back To Top</a>
					</div>
				</div>
			</nav>
		</div>
		<div class="s-b-cat">
			<h2>Shop Related Categories</h2>
			<div class="s-b-cat-row">
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="#">
								<img src="assest/images/MTR1.jpeg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="#" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="#">
								<img src="assest/images/p5.jpg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="#" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="#">
								<img src="assest/images/p6.jpg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="#" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="#">
								<img src="assest/images/MTR5.jpeg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="#" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="#">
								<img src="assest/images/p2.jpg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="#" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
				<div class="s-b-cat-col">
					<div class="s-b-col-bg">
						<div class="s-b-img">
							<a href="#">
								<img src="assest/images/MTR24.jpeg">
							</a>
						</div>
						<div class="s-b-content">
							<a href="#" class="s-b-content-name">
								Sunglasses for Women
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Category Page End  -->


<!-- Footer -->
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/category.blade.php ENDPATH**/ ?>