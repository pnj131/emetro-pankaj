<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="breadcrumb">
    <div class="container">
        <ol class="breadcrumb-list">
            <li>
                <a href="/">
                    <span>Home</span>
                </a>
            </li>
            <li>
                <strong>Vendors and Suppliers </strong>
            </li>
        </ol>
    </div>
</div>
<section>
    <div class="catalog-view">
        <div class="container">
            <div class="catalog-view-row">
            </div>
            <!-- Product Details Start -->
            <div class="product-info">
                <ul class="product-tablist row m-0">
                    <li class="col p-0">
                        <a href="#product-details" class="scrollLink active">
                            <h2>Vendor Inquiries</h2>
                        </a>
                    </li>
                </ul>
                <div class="product-i-tabs row m-0">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <p>Prospective vendors of non-food or sundry items can contact the corporate office at the addresses below.</p>
                                <br>
                        </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h4 class="section-title">eMetro's corporate mailing address is:</h4>
                                <br>
                                <div class="product-info-description product-i-content">
                                    <p> #24, 4th Floor, TATA Nagar, Main Road,</p>
                                    <p> Kodigehalli, Sahakar Nagar Post, Bengaluru - 560092
                                    </p><br>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="product-i-box  pl-0 pr-0" id="product-details">
                                    <h4 class="section-title">eMetro's corporate mailing address is:</h4>
                                    <br>
                                    <div class="product-info-description product-i-content">
                                        <p> #24, 4th Floor, TATA Nagar, Main Road,</p>
                                        <p> Kodigehalli, Sahakar Nagar Post, Bengaluru - 560092
                                        </p><br>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/footer/vendors-suppliers.blade.php ENDPATH**/ ?>