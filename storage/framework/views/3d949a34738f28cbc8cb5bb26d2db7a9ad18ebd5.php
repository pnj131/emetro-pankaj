<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<!--Header-->
<!-- Slider -->
<div class="SliderSec">
  <div class="">

    <div class="owl-carousel owl-theme owl-loaded" >
      <div class="owl-stage-outer">
        <div class="owl-stage" >
            <div class="owl-item ">
                <video autoplay loop muted class="banner-video">
                    <source src="<?php echo e(asset('assest/images/banner/Main_Banner_24122021.mp4')); ?>" type="video/mp4">
                </video>
            </div>






        </div>
      </div>
    </div>
  </div>
</div>
<!-- End -->
<!-- Popular barnd -->
<div class="PopularBrandWrap">
  <div class="container">
    <div class="Heading">
      <h3>POPULAR BRANDS</h3>
    </div>
    <div class="WrapSLide">
      <div class="owl-carousel owl-theme owl-loaded brand">
        <div class="owl-stage-outer">
          <div class="owl-stage">
            <div class="owl-item"><img src="assest/images/1.png"></div>
            <div class="owl-item"><img src="assest/images/2.png"></div>
            <div class="owl-item"><img src="assest/images/3.png"></div>
            <div class="owl-item"><img src="assest/images/4.png"></div>
            <div class="owl-item"><img src="assest/images/5.png"></div>
            <div class="owl-item"><img src="assest/images/6.png"></div>
            <div class="owl-item"><img src="assest/images/7.png"></div>
            <div class="owl-item"><img src="assest/images/8.png"></div>
            <div class="owl-item"><img src="assest/images/9.png"></div>
            <div class="owl-item"><img src="assest/images/10.png"></div>
            <div class="owl-item"><img src="assest/images/11.png"></div>
            <div class="owl-item"><img src="assest/images/12.png"></div>
            <div class="owl-item"><img src="assest/images/13.png"></div>
            <div class="owl-item"><img src="assest/images/14.png"></div>
            <div class="owl-item"><img src="assest/images/15.png"></div>
            <div class="owl-item"><img src="assest/images/16.png"></div>
            <div class="owl-item"><img src="assest/images/17.png"></div>
            <div class="owl-item"><img src="assest/images/18.png"></div>
            <div class="owl-item"><img src="assest/images/19.png"></div>
            <div class="owl-item"><img src="assest/images/20.png"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End -->
<!-- Membership -->
<div class="MembershipSection">
  <div class="container">
    <div class="Heading">
      <h3>Membership Cards and Benefits </h3>
    </div>
    <div class="owl-carousel owl-theme owl-loaded Membership">
      <div class="owl-stage-outer">
        <div class="owl-stage">
          <!-- 5 product pair -->

               <div class="owl-item"><a href="#"><img src="assest/images/Lunar.jpg"></a></div>


               <div class="owl-item"><a href="#"><img src="assest/images/Sunstar.jpg"></a></div>


               <div class="owl-item"><a href="#"><img src="assest/images/Blue-Planet.jpg"></a></div>


               <div class="owl-item"><a href="#"><img src="assest/images/Galaxy.jpg"></a></div>


          <!-- End -->
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End -->

<!-- Featured Products  -->
<div class="FeaturedProductWrap">
    <div class="container">
        <div class="Heading">
            <h3>Featured Products</h3>
        </div>
        <div class="owl-carousel owl-theme owl-loaded FeaturedProduct">
            <div class="owl-stage-outer">
                <div class="owl-stage">
                    <!-- 5 product pair -->
                    <div class="owl-item">
                        <div class="CustomRow">
                            <div class="col-mdc-3">
                                <div class="FeaProCard">
                                    <a href="#">
                                        <img src="assest/images/MTR1.jpeg">
                                    </a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-mdc-3">
                                <div class="FeaProCard">
                                    <a href="#"><img src="assest/images/MTR3.jpeg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-mdc-3">
                                <div class="FeaProCard"> <a href="#"><img src="assest/images/MTR5.jpeg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-mdc-3">
                                <div class="FeaProCard"> <a href="#"><img src="assest/images/MTR24.jpeg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="CustomRow">
                            <div class="col-mdc-3">
                                <div class="FeaProCard"> <a href="#"><img src="assest/images/p5.jpg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-mdc-3">
                                <div class="FeaProCard"> <a href="#"><img src="assest/images/p6.jpg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-mdc-3">
                                <div class="FeaProCard"> <a href="#"><img src="assest/images/p1.jpg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-mdc-3">
                                <div class="FeaProCard"> <a href="#"><img src="assest/images/p6.jpg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="owl-item">
                        <div class="CustomRow">
                            <div class="col-mdc-3">
                                <div class="FeaProCard"> <a href="#"><img src="assest/images/p6.jpg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-mdc-3">
                                <div class="FeaProCard"> <a href="#"><img src="assest/images/p5.jpg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-mdc-3">
                                <div class="FeaProCard"> <a href="#"><img src="assest/images/p1.jpg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-mdc-3">
                                <div class="FeaProCard"> <a href="#"><img src="assest/images/p2.jpg"></a>
                                    <div class="Txt">
                                        <div class="product-name FeaProd">
                                            <span class="property">Product Name: </span>
                                            <a href="#"> MTR Breakfast Mix</a>
                                        </div>
                                        <div class="item-code FeaProd"><span class="property">Item Code : </span> 001</div>
                                        <div class="pack-size FeaProd d-flex justify-content-between align-items-center">
                                            <div class="size"><span class="property"> Pack Size :</span> 1</div>
                                            <select name="weight" class="weight form-control">
                                                <option value="200g">200g</option>
                                                <option value="500g">500g</option>
                                            </select>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-between">
                                            <div class="Price FeaProd"><span class="property">Price/PC :</span> 100</div>
                                            <div class="saving FeaProd"><span class="property">Instant Saving :</span> 5x1=5</div>
                                        </div>
                                        <div class="yourcost FeaProd"><span class="property">Your Cost : </span>95 Rs</div>
                                        <div class="product-i-row FeaProd d-flex align-items-center justify-content-between">
                                            <div class="quantity-selector-v2">
                                                <button id="minusQty" type="button" name="minusQty" class="btn">
                                                    <i class="fas fa-minus"></i>
                                                </button>
                                                <input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
                                                <button id="plusQty" type="button" name="plusQty" class="btn">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <div class="box-tocart">
                                                <button type="submit" title="Add to Cart" class="btn text-center">
                                                    <i class="fas fa-shopping-cart"></i> <span>Add to Cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- End -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End -->
<!-- Brand of the week -->
<div class="BrandOfTheWeek">
  <div class="container">
    <div class="Heading">
      <h3>BRAND OF THE WEEK </h3>
    </div>
    <div class="ImgBrandWeek"> <img src="assest/images/Brand_week.png" alt="img"> </div>
  </div>
</div>
<!-- end -->
<!-- Footer -->
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/index.blade.php ENDPATH**/ ?>