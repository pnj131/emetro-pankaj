<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="breadcrumb">
    <div class="container">
        <ol class="breadcrumb-list">
            <li>
                <a href="/">
                    <span>Home</span>
                </a>
            </li>
            <li>
                <strong> Charitable Contributions </strong>
            </li>
        </ol>
    </div>
</div>
<section>
    <div class="catalog-view">
        <div class="container">
            <div class="catalog-view-row">
            </div>
            <!-- Product Details Start -->
            <div class="product-info">
                <ul class="product-tablist row m-0">
                    <li class="col p-0">
                        <a href="#product-details" class="scrollLink active">
                            <h2>Charitable Contributions</h2>
                        </a>
                    </li>
                </ul>
                <div class="product-i-tabs row m-0">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">

                                <div class="product-info-description product-i-content">
                                   <span >eMetro’s primary charitable efforts specifically focus on programs supporting children, education,
                                       and health and human services in the communities where we do business.
                                       Throughout the year we receive a large number of requests from nonprofit organizations striving to make a positive impact,
                                       and we are thankful to be able to provide support to a variety of organizations and causes. While we would like to respond favorably to all requests,
                                       understandably, the needs are far greater than our allocated resources and we are unable to accommodate them all.
                                   </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h4 class="section-title">Donation and Grant Eligibility Guidelines</h4>
                                <br>
                                <p>To apply for eMetro support, organizations must meet the following general requirements:</p>
                                <div class="product-info-description product-i-content">

                                    <p>    •	Must be a nonprofit organization</p><br>
                                    <p>    •	Focus on supporting children, education, and/or health and human services <p>
                                        <br>
                                    <p>    •	Monetary requests only</p><br>


                                    <p>eMetro accepts and reviews funding requests throughout the year on a rolling basis,
                                        and organizations are eligible to submit one application per fiscal year (April–March).
                                        See our giving options below to determine the appropriate level for your organization’s request – </p>
                                    <br>
                                    <p>-	Drop Cash in the DONATION BOX kept near the Check-out counter at any eMetro Store Location
                                        -	Donate through online – using Debit Card / Credit Card / Net Banking / UPI and mention that Transaction as *CHARITABLE*.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/footer/charitable_contribution.blade.php ENDPATH**/ ?>