<?php echo $__env->make('layout.login-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!--Header-->

<!-- Login Start -->
<div class="g-login">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col">
				<div class="page-title-wrapper">
					<h1 class="page-title">
						<span class="base">Create Account</span>
					</h1>
				</div>
				<div class="g-login-bg">
					<form>
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email Address">
						</div>
						<div class="form-group password-control">
							<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
							<i class="fas fa-eye"></i>
						</div>
						<div class="form-group password-control">
							<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">
							<i class="fas fa-eye"></i>
						</div>
						<div class="form-group password-condition password-c-normal">
							<h5 class="mb-3">Password must include the following:</h5>
							<ul class="password-c-c">
								<li class="true-info"><span>Use between 8 and 16 characters</span></li>
								<li class="true-info"><span>Include at least one lowercase (a-z) and one uppercase letter (A-Z)</span></li>
								<li><span>Include at least one special character(e.g. !@#$&)</span></li>
								<li class="false-info"><span>Does not contain blank spaces or the following special characters: < > ,</span></li>
								<li><span>Include at least one digit (0-9)</span></li>
								<li class="false-info"><span>Passwords match</span></li>
							</ul>
							<div class="password-guide mb-2">
								<h6 class="d-flex">
									<span>Password Strength :</span>
									<span>
										<ul class="state">
											<li class="short">Too Short</li>
											<li class="weak">Weak</li>
											<li class="fair">Fair</li>
											<li class="good">Good</li>
											<li class="strong">Strong</li>
										</ul>
									</span>
								</h6>
							</div>
							<p class="passwordhelp">To improve strength, increase password length and use capital letters, numbers, and special characters (except &lt; &gt; , )</p>
						</div>
						<div class="form-group">
							<input id="membershipnumber" class="form-control" type="text" placeholder="Membership Number (optional)">
						</div>
						<div class="form-group">
							<a href="#">
								Where can I find my membership number?
							</a>
						</div>
						<div class="form-group">
							<div class="form-check">
							  <input class="form-check-input" type="checkbox" value="" id="invalidcheck2" required="">
							  <label class="form-check-label" for="invalidcheck2">
								Yes, I would like to receive emails about special promotions and new product information from E Metro. E Metro will not rent or sell your email address.
							  </label>
							</div>
						 </div>
						 <div class="form-group">
							<div class="terms-condition">
 							  <label class="form-check-label">
								By creating an account you agree to E Metro <a href="terms-and-conditions-of-use.html" target="_blank">terms and conditions</a> of use.
							  </label>
							</div>
						 </div>
						<button type="submit" class="btn btn-primary btn-block">Create Account</button>
					</form>
					<hr>
 					<div class="sign-in-btn sign-in-already">
						Already have an account? <a href="/sign-in">Sign In</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Login End -->
<!-- Footer -->
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/sign-up.blade.php ENDPATH**/ ?>