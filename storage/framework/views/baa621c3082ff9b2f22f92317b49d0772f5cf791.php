<?php echo $__env->make('layout.login-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!--Header-->

<!-- Login Start -->
<div class="g-login">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col">
				<div class="page-title-wrapper">
					<h1 class="page-title">
						<span class="base">Sign In</span>
					</h1>
				</div>
				<div class="g-login-bg">
					<form>
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email Address">
						</div>
						<div class="form-group password-control">
							<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
							<i class="fas fa-eye"></i>
						</div>
						<div class="forget-pass-link form-group">
							<a href="/forgot-password">
								Forgot Password?
							</a>
						</div>
						<div class="form-check form-group">
							<input type="checkbox" class="form-check-input" id="rememberMe">
							<label class="form-check-label" for="rememberMe">rememberMe</label>
						</div>
						<button type="submit" class="btn btn-primary btn-block">Sign In</button>
					</form>
					<hr>
					<h6>New to Metro?</h6>
					<div class="sign-in-btn">
						<a href="/sign-up" class="btn btn-block">
							Create Account
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Login End -->

<!-- Footer -->
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/sign-in.blade.php ENDPATH**/ ?>