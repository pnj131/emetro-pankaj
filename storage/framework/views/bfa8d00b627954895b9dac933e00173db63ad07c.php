<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="breadcrumb">
    <div class="container">
        <ol class="breadcrumb-list">
            <li>
                <a href="/">
                    <span>Home</span>
                </a>
            </li>
            <li>
                <strong> Job </strong>
            </li>
        </ol>
    </div>
</div>
<section>
    <div class="catalog-view">
        <div class="container">
            <div class="catalog-view-row">
            </div>
            <!-- Product Details Start -->
            <div class="product-info">
                <ul class="product-tablist row m-0">
                    <li class="col p-0">
                        <a href="#product-details" class="scrollLink active">
                            <h2>Job</h2>
                        </a>
                    </li>
                </ul>
                <div class="product-i-tabs row m-0">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">What Are You Looking For in a Career?</h3>
                                <div class="product-info-description product-i-content">
                                   <p>    •	Exciting opportunities</p><br>
                                   <p>    •	Career growth</p><br>
                                   <p>    •	Friendly and supportive work environment</p><br>
                                   <p>    •	Stability</p><br>
                                   <p>    •	A workplace focused on ethics and obeying the law</p><br>
                                   <p>    •	Great benefits</p><br>
                                </div>
                                <br>
                                <div class="product-info-description product-i-content">
                                    <p>   If you are an ambitious, energetic person who wants to LEARN & EARN and become an Entrepreneur then join eMetro !!!</p><br>
                                    <p>   You can enjoys a fast-paced team environment filled with challenges and opportunities,
                                        you've come to the right place. Our successful employees are service-oriented people with integrity and commitment toward a common goal of excellence.
                                        Read on to discover how to pursue employment opportunities with us. eMetro offers great jobs, great pay, great benefits and a great place to work.</p><br>
                                    <p>  Merchandising is the lifeblood of eMetro, and our business is centered on our Buying, Roaming and Resident Associates.
                                        Most employees begin their careers in the warehouse setting, becoming experts in eMetro merchandising and operations.
                                        The company also offers diverse career opportunities at our Home and Regional Offices in many other areas,
                                        such as Accounting, Buying, Marketing, Information Systems, and Human Resources, to name a few.
                                        Additionally, eMetro is dedicated to recognizing and rewarding employees for their hard work and loyalty.
                                        In fact, the majority of our management teams are promoted from within.</p><br>
                                    <p> Today we have Warehouse Managers and Vice Presidents who were once Stockers and Cashier Assistants or who started in clerical positions for eMetro.
                                        We believe eMetro's future executive officers are currently working in our locations, as well as in our Home and Regional Office.</p><br>

                                </div>
                                <h3 class="section-title">Regional and Home Office Jobs</h3>
                                <p>eMetro is committed to promoting from within the company. The majority of our current home and regional office team members are home grown.
                                    This means that they started in our warehouses,
                                    depots and business centers, learned the business and moved up within the company. This philosophy also ensures promotional opportunities for
                                    motivated individuals
                                </p>
                                <p>
                                    Today, we have accountants, buyers, and human resources personnel who began their careers as stockers or cashiers in our locations.
                                    We believe that eMetro's future officers are currently working in our warehouses, depots, and business centers, as well as in our Home and Regional Offices.
                                </p><br>
                                <h3 class="section-title">Information Technology Opportunities</h3>
                                <p>Our home office, located in Bangalore, Karnataka, India, is also the site of our Information Systems Division. For employment opportunities in I.T. ,
                                    please click on eMetro Jobs.</p><br>
                                <p>eMetro is currently seeking Licensed Pharmacists to fill positions for pharmacy managers and full time,
                                    part time and relief/floating pharmacist positions. eMetro positions as one of the Pharma OTC Drugs in India. </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h4 class="section-title">Key accessibility features of eMetro sites include, but are not limited to:</h4>
                                <br>
                                <div class="product-info-description product-i-content">

                                    <p>  1) Appropriate site structure for use with assistive technologies.</p><br>
                                    <p>  2) Appropriate text equivalents for images.</p><br>
                                    <p>  3) Appropriate labels for forms and input fields.</p><br>
                                    <p>  4) Full keyboard access.</p><br>
                                    <p>  5) Captioning for videos.</p><br>
                                    <p>  6) Proper color contrast ratios for text and images with text.</p><br>

                                    <p>If you are having trouble using this site (or mobile app) because of a disability, please call us at 080-6733 5567 for assistance. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/footer/job.blade.php ENDPATH**/ ?>