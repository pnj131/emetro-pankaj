<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="breadcrumb">
    <div class="container">
        <ol class="breadcrumb-list">
            <li>
                <a href="/">
                    <span>Home</span>
                </a>
            </li>
            <li>
                <strong> Member Privilege and Conditions </strong>
            </li>
        </ol>
    </div>
</div>
<section>
    <div class="catalog-view">
        <div class="container">
            <div class="catalog-view-row">
            </div>
            <!-- Product Details Start -->
            <div class="product-info">
                <ul class="product-tablist row m-0">
                    <li class="col p-0">
                        <a href="#product-details" class="scrollLink active">
                            <h2>Member Privileges & Conditions</h2>
                        </a>
                    </li>
                </ul>
                <div class="product-i-tabs row m-0">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <p>We look forward to serving you as an eMetro Member. If you have any questions, please ask our member services personnel at any eMetro Membership Counter or click Customer Service.
                                    eMetro Membership is subject to our Member Privileges & Conditions, which may change from time to time without prior notice.</p>
                                <br>
                                <h3 class="section-title">Membership</h3>
                                <div class="product-info-description product-i-content">
                                    <p>   •	Membership is available to all qualifying individuals 18 years of age and </p>
                                    <br>
                                    <p>   •	eMetro reserves the right to refuse membership to any applicant, and membership may be terminated at eMetro's discretion and without cause.</p>
                                    <br>
                                    <p>   •	Membership is subject to any and all rules adopted by eMetro, including our privacy policies and practices, and they may be amended from time
                                        to time without notice</p><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Membership Cards and Fees</h3>
                                <br>
                                <div class="product-info-description product-i-content">

                                    <p>•	Membership fee is for 12-month period from the date of enrollment.</p><br>
                                    <p>    •	Your membership card is valid at any eMetro Stock Points.</p><br>
                                    <p>    •	You will be required to use your membership card whenever you are buying items at eMetro Stock Points and when checking
                                            out at the time of completing your Order.</p><br>
                                    <p>   •	Report lost or stolen cards to any eMetro membership counter immediately or call Toll free Number</p><br>
                                    <p>   •	Cards remain the property of eMetro and must be returned upon request.</p><br>
                                    <p>   •	Limit one Executive Membership per household or business.</p><br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Renewing, Adding or Deleting Cards</h3>
                                <br>
                                <div class="product-info-description product-i-content">

                                    <p>•	Renewal fees are due no later than the last day of the month your membership expires. You may remit
                                        your renewal fee online at www.eMetro.com or at any eMetro Stock Points. eMetro members may charge their
                                        membership fees automatically on any Visa® Credit/Debit Card or Mastercard Debit Card. That card will be charged
                                        on the first day of your renewal month. If you have not signed up for auto renewal, your membership fee will be added to your
                                        purchase after expiration.</p><br>
                                    <p>    •	Membership renewal must be completed for all cardholders on the membership when the renewal is processed.</p><br>
                                    <p>    •	You will not receive new membership cards each year.</p><br>
                                    <p>    •	Memberships renewed within 2 months after expiration of the current membership year will be extended for 12 months
                                    from the expiration date. Memberships renewed more than 2 months after such expiration will be extended for 12 months from the renewal date.
                                    All renewals will be assessed at the membership fee in effect on the date the membership fee is paid.</p><br>
                                    </p><br>
                                    <p>•	The eMetro Membership Card holder is Authorized to make changes to the membership - to change the address, phone number,
                                        update communication preferences, and upgrade or renew the membership.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Risk-Free 100% Satisfaction Guarantee</h3>
                                <br>
                                <div class="product-info-description product-i-content">

                                    <p>.	We are committed to providing quality and value on the
                                        products we sell with a risk-free 100% satisfaction guarantee on both your membership and merchandise.
                                        Limited exceptions apply. See return policy for more information</p><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Prices</h3>
                                <br>
                                <div class="product-info-description product-i-content">
                                    <p>. Each item is marked with an item number or a UPC Code. The price of the item,
                                        along with the description and identifying number, is posted with the item.</p><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Payment</h3>
                                <br>
                                <div class="product-info-description product-i-content">
                                    <p> •	eMetro accepts all Visa cards, as well as Debit /Credit Cards, UPC to buy the Membership Cards,
                                        which will be put in the respective membership card’s virtual WALLET, which needs to be used for all your purchase at eMetro.
                                        You can buy the eMetro Cards at – Online (www.eMetro.com) or any nearest eMetro Stock Points or at KIOSK Desk.</p>
                                    <br>
                                    <p>  •	eMetro does not accept manufacturers' discount coupons (other than those distributed by eMetro) or other retail establishment discount coupons.
                                    </p><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Sales and Use Taxes, and Resale Certificates</h3>
                                <br>
                                <div class="product-info-description product-i-content">
                                   <p>•	The member agrees to pay eMetro any sales, excise, that is imposed on the sale price of the items purchased,
                                       which may be based on the price before any discounts or promotions. The member agrees that they will hold eMetro
                                       harmless and indemnify eMetro from any claim, loss, assessment or expense occasioned by non-payment of tax to eMetro.</p>
                                    <br>
                                    <p>   •	If any merchandise is being purchased for resale and an exemption from tax is sought, the member shall have
                                        valid resale license number on file with eMetro and shall notify the cashier prior to recording the sale.
                                        Such declaration, and the products purchased thereunder, shall be recorded on a "Certificate for Resale."
                                        Sales tax will not be charged at the time of purchase only on those products the member states are specifically for resale;
                                        all other products subject to tax will be deemed taxable.</p><br>
                                </p>   •	In the event any product that was purchased for resale (tax free) is subsequently consumed or used in any manner other
                                    than for resale that creates or imposes a sales or use tax, the member agrees to report and pay to the proper taxing authority any tax due,
                                    including penalties and interest.
                                   </p><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">General Policies</h3>
                                <br>
                                <div class="product-info-description product-i-content">
                                   <p>    •	Members are welcome to bring children and Sr. Citizens into the Stock Points; members are responsible for their children and guests.
                                            Children should not be left unattended. Only eMetro members may purchase </p><br>
                                   <p>    •	It is eMetro's intent to provide a shopping environment free from all forms of harassment and discrimination for employees, applicants,
                                            independent contractors, members and suppliers. We prohibit all forms of harassment and discrimination.</p><br>
                                   <p>    •	eetro reserves the right to inspect any container, backpack, briefcase, or other bag, upon entering or leaving the warehouse and to
                                            refuse entry to anyone at our discretion.</p><br>
                                   <p>    •	To ensure that all members are correctly charged for the merchandise purchased, all receipts and merchandise will be inspected as you
                                            leave the warehouse.</p><br>
                                   <p>    •	eMetro policy prohibits firearms to be brought into the warehouse, except in the case of authorized law-enforcement officers.</p><br>
                                   <p>    •	Animals are not permitted in any of the eMetro Stock Points.</p><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/footer/member-privilage.blade.php ENDPATH**/ ?>