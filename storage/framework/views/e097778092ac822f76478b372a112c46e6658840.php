<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!--Header-->

<!-- Cart Page Start  -->
<!-- Breadcrumb Start-->
<div class="breadcrumb">
	<div class="container">
		<ol class="breadcrumb-list">
			<li>
				<a href="/">
					<span>Home</span>
				</a>
			</li>
			<li>
				<strong> Cart </strong>
			</li>
		</ol>
	</div>
</div>
<!-- Breadcrumb End-->
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<!-- Page Heading Start -->
			<div class="page-title-wrapper">
				<h1 class="page-title">
					<span class="base">Cart</span>
					<span class="total-items"> (2 Items)</span>
				</h1>
			</div>
			<!-- Page Heading End -->
		</div>
	</div>
	<div class="cart-wrapper">
		<div class="row">
			<div class="col-12 col-lg-7 col-xl-8">
				<div class="cart-item-row">
					<!-- Cart Item Start -->
					<div class="cart-item">
						 <div class="row">
							<div class="col-md-7 col-lg-7">
								<div class="row">
									<div class="col-lg-3 col-sm-3 col-sm-3 col-3 item-thumbnail">
										<a href="#">
											<img src="assest/images/cart-item.jpg">
										</a>
									</div>
									<div class="col-lg-9 col-sm-9 col-sm-9 col-9">
										<div class="product-item-details">
											<strong class="product-item-name">
													<a href="#">Feit 73802 3-Light LED Vanity Fixture - 3000K</a>
											</strong>
											<div class="modal-details">
												<ul>
													<li>
														<span class="item-p-name">Item</span> <span class="item-p-details">2111001 </span>
													</li>
													<li>
														<span class="item-p-name">Model</span> <span class="item-p-details">241967C </span>
													</li>
												</ul>
											</div>
											<div class="product-price ">
												<span class="price" automation-id="priceOutput_1">
												  $59.99
													  <span style="display:none" class="itemPricePurchased">59.99</span>
												</span>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-8">
												<div class="product-i-row">
													<div class="quantity-selector-v2">
														<button id="minusQty" type="button" name="minusQty" class="btn">
															<i class="fas fa-minus"></i>
														</button>
														<input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
														<button id="plusQty" type="button" name="plusQty" class="btn">
															<i class="fas fa-plus"></i>
														</button>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="total-price">
													<span>
														Total
													</span>
													<div class="total-price-price">
														$119.98
													</div>
												</div>
											</div>
										</div>
										<div class="remove-link">
											<a href="javascript:void(0);">
												<span>Remove </span>
											</a>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-5 col-lg-5">
								<div class="shipping-select">
									<div class="shipping-col" for="Standard">
										<input type="radio" name="radio" id="Standard1" checked>
										Standard
										<span class="check"></span>
									</div>
									<div class="shipping-col" for="express-ship">

										<input type="radio" name="radio" id="express-ship1">
										Express 1 to 2 Business Days
										<span class="check"></span>
									</div>
								 </div>
								<div class="cart-delivery-details-link">
									<a href="javascript:void(0);">
										<span>Delivery Details </span>
									</a>
								</div>
							</div>
						</div>
						<div class="item-actions">
							<div class="row justify-content-end">
								<div class="col-md-5">
									<ul>
										<li>
											<a href="#">
												Add to List
											</a>
										</li>
										<li>
											<a href="#">
												Save for Later
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- Cart Item End -->
					<!-- Cart Item Start -->
					<div class="cart-item">
						 <div class="row">
							<div class="col-md-7 col-lg-7">
								<div class="row">
									<div class="col-lg-3 col-sm-3 col-sm-3 col-3 item-thumbnail">
										<a href="#">
											<img src="assest/images/cart-item.jpg">
										</a>
									</div>
									<div class="col-lg-9 col-sm-9 col-sm-9 col-9">
										<div class="product-item-details">
											<strong class="product-item-name">
													<a href="#">Feit 73802 3-Light LED Vanity Fixture - 3000K</a>
											</strong>
											<div class="modal-details">
												<ul>
													<li>
														<span class="item-p-name">Item</span> <span class="item-p-details">2111001 </span>
													</li>
													<li>
														<span class="item-p-name">Model</span> <span class="item-p-details">241967C </span>
													</li>
												</ul>
											</div>
											<div class="product-price ">
												<span class="price" automation-id="priceOutput_1">
												  $59.99
													  <span style="display:none" class="itemPricePurchased">59.99</span>
												</span>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-8">
												<div class="product-i-row">
													<div class="quantity-selector-v2">
														<button id="minusQty" type="button" name="minusQty" class="btn">
															<i class="fas fa-minus"></i>
														</button>
														<input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
														<button id="plusQty" type="button" name="plusQty" class="btn">
															<i class="fas fa-plus"></i>
														</button>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="total-price">
													<span>
														Total
													</span>
													<div class="total-price-price">
														$119.98
													</div>
												</div>
											</div>
										</div>
										<div class="remove-link">
											<a href="javascript:void(0);">
												<span>Remove </span>
											</a>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-5 col-lg-5">
								<div class="shipping-select">
									<div class="shipping-col" for="Standard">
										<input type="radio" name="radio" id="Standard2" checked>
										Standard
										<span class="check"></span>
									</div>
									<div class="shipping-col" for="express-ship">

										<input type="radio" name="radio" id="express-ship2">
										Express 1 to 2 Business Days
										<span class="check"></span>
									</div>
								 </div>
								<div class="cart-delivery-details-link">
									<a href="javascript:void(0);">
										<span>Delivery Details </span>
									</a>
								</div>
							</div>
						</div>
						<div class="item-actions">
							<div class="row justify-content-end">
								<div class="col-md-5">
									<ul>
										<li>
											<a href="#">
												Add to List
											</a>
										</li>
										<li>
											<a href="#">
												Save for Later
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- Cart Item End -->
					<!-- Cart Item Start -->
					<div class="cart-item">
						 <div class="row">
							<div class="col-md-7 col-lg-7">
								<div class="row">
									<div class="col-lg-3 col-sm-3 col-sm-3 col-3 item-thumbnail">
										<a href="#">
											<img src="assest/images/cart-item.jpg">
										</a>
									</div>
									<div class="col-lg-9 col-sm-9 col-sm-9 col-9">
										<div class="product-item-details">
											<strong class="product-item-name">
													<a href="#">Feit 73802 3-Light LED Vanity Fixture - 3000K</a>
											</strong>
											<div class="modal-details">
												<ul>
													<li>
														<span class="item-p-name">Item</span> <span class="item-p-details">2111001 </span>
													</li>
													<li>
														<span class="item-p-name">Model</span> <span class="item-p-details">241967C </span>
													</li>
												</ul>
											</div>
											<div class="product-price ">
												<span class="price" automation-id="priceOutput_1">
												  $59.99
													  <span style="display:none" class="itemPricePurchased">59.99</span>
												</span>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-8">
												<div class="product-i-row">
													<div class="quantity-selector-v2">
														<button id="minusQty" type="button" name="minusQty" class="btn">
															<i class="fas fa-minus"></i>
														</button>
														<input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
														<button id="plusQty" type="button" name="plusQty" class="btn">
															<i class="fas fa-plus"></i>
														</button>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="total-price">
													<span>
														Total
													</span>
													<div class="total-price-price">
														$119.98
													</div>
												</div>
											</div>
										</div>
										<div class="remove-link">
											<a href="javascript:void(0);">
												<span>Remove </span>
											</a>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-5 col-lg-5">
								<div class="shipping-select">
									<div class="shipping-col" for="Standard">
										<input type="radio" name="radio" id="Standard3" checked>
										Standard
										<span class="check"></span>
									</div>
									<div class="shipping-col" for="express-ship">

										<input type="radio" name="radio" id="express-ship3">
										Express 1 to 2 Business Days
										<span class="check"></span>
									</div>
								 </div>
								<div class="cart-delivery-details-link">
									<a href="javascript:void(0);">
										<span>Delivery Details </span>
									</a>
								</div>
							</div>
						</div>
						<div class="item-actions">
							<div class="row justify-content-end">
								<div class="col-md-5">
									<ul>
										<li>
											<a href="#">
												Add to List
											</a>
										</li>
										<li>
											<a href="#">
												Save for Later
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- Cart Item End -->
					<!-- Cart Item Start -->
					<div class="cart-item">
						 <div class="row">
							<div class="col-md-7 col-lg-7">
								<div class="row">
									<div class="col-lg-3 col-sm-3 col-sm-3 col-3 item-thumbnail">
										<a href="#">
											<img src="assest/images/cart-item.jpg">
										</a>
									</div>
									<div class="col-lg-9 col-sm-9 col-sm-9 col-9">
										<div class="product-item-details">
											<strong class="product-item-name">
													<a href="#">Feit 73802 3-Light LED Vanity Fixture - 3000K</a>
											</strong>
											<div class="modal-details">
												<ul>
													<li>
														<span class="item-p-name">Item</span> <span class="item-p-details">2111001 </span>
													</li>
													<li>
														<span class="item-p-name">Model</span> <span class="item-p-details">241967C </span>
													</li>
												</ul>
											</div>
											<div class="product-price ">
												<span class="price" automation-id="priceOutput_1">
												  $59.99
													  <span style="display:none" class="itemPricePurchased">59.99</span>
												</span>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-8">
												<div class="product-i-row">
													<div class="quantity-selector-v2">
														<button id="minusQty" type="button" name="minusQty" class="btn">
															<i class="fas fa-minus"></i>
														</button>
														<input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
														<button id="plusQty" type="button" name="plusQty" class="btn">
															<i class="fas fa-plus"></i>
														</button>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="total-price">
													<span>
														Total
													</span>
													<div class="total-price-price">
														$119.98
													</div>
												</div>
											</div>
										</div>
										<div class="remove-link">
											<a href="javascript:void(0);">
												<span>Remove </span>
											</a>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-5 col-lg-5">
								<div class="shipping-select">
									<div class="shipping-col" for="Standard">
										<input type="radio" name="radio" id="Standard4" checked>
										Standard
										<span class="check"></span>
									</div>
									<div class="shipping-col" for="express-ship">

										<input type="radio" name="radio" id="express-ship4">
										Express 1 to 2 Business Days
										<span class="check"></span>
									</div>
								 </div>
								<div class="cart-delivery-details-link">
									<a href="javascript:void(0);">
										<span>Delivery Details </span>
									</a>
								</div>
							</div>
						</div>
						<div class="item-actions">
							<div class="row justify-content-end">
								<div class="col-md-5">
									<ul>
										<li>
											<a href="#">
												Add to List
											</a>
										</li>
										<li>
											<a href="#">
												Save for Later
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- Cart Item End -->
				</div>
			</div>
			<div class="col-12 col-lg-5 col-xl-4 pull-right">
				<div class="cart-summary-row">
					<div class="visacard-row cart-summary">
						<div class="row">
							<div class="col-md-3">
								<img src="assest/images/visa-cart.jpg">
							</div>
							<div class="col-md-9">
								<a href="#">Apply for the Costco Anywhere Visa® Card by Citi</a>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<p>Earn 2% Cash Back on your Costco purchases. Exclusively for Costco members.</p>
							</div>
							<div class="col-12">
								<button type="submit" class="btn btn-primary btn-block">Apply Today</button>
							</div>
						</div>
					</div>
					<div class="cart-summary order-summary">
						<div class="order-s-total">
							<div class="d-flex justify-content-between">
								<span class="properties">Subtotal</span>
								<span class="value">$137.97</span>
							</div>
							<div class="d-flex justify-content-between cart-shipping">
								<span class="properties">Shipping & Handling for <a href="javascript:void(0);">67301</a></span>
								<span class="value">$0.00</span>
							</div>
						</div>
						<hr>
						<div class="d-flex justify-content-between order-estimated">
							<span class="properties">Estimated Total</span>
							<span class="value">$137.97</span>
						</div>
						<div class="order-summary-text-info">Applicable taxes will be calculated at checkout.</div>
						<button type="submit" class="btn btn-primary btn-block">Checkout</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<!-- Recently Viewed Start -->
<div class="related-products recently-viewed">
	<div class="container">
		<h3 class="section-title">Recently Viewed Products</h3>
		<div class="products products-grid">
			<ol class="product-items owl-carousel owl-theme">
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/MTR1.jpeg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/p5.jpg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/p6.jpg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/MTR5.jpeg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/p2.jpg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="product-item">
					<div class="product-item-info">
						<div class="product-item-photo">
							<a href="#" class="product-item-photo">
								<img src="assest/images/p2.jpg">
							</a>
						</div>
						<div class="product-item-details">
							<div class="col-xs-12 badge-class">
								<i class="fas fa-bars"></i>
							</div>
							<div class="member-only">
								<span class="co-c"><i class="far fa-user"></i></span>
								Member Only Item
							</div>
							<div class="product name product-item-name">
								<a class="product-item-link" href="#">
									Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
								</a>
							</div>
							<div class="ratings">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>
							</div>
							<div class="compare-product">
								<a href="#" rel="follow">
									<input id="compare_product" type="checkbox">
									<label for="compare_product">
										Compare Product
									</label>
								</a>
							</div>
						</div>
					</div>
				</li>
			</ol>
		</div>
	</div>
</div>
<!-- Recently Viewed End -->
<!-- Cart Page End  -->

<!-- Footer -->
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/cart.blade.php ENDPATH**/ ?>