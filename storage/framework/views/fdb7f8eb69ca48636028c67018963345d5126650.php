<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Emetro</title>
    <link rel = "icon" href ="<?php echo e(asset('assest/images/fav.png')); ?>">
    <link href="<?php echo e(asset('assest/css/bootstrap.min.css')); ?>"  type="text/css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"/>
    <link href="<?php echo e(asset('assest/css/owl.carousel.min.css')); ?>" type="text/css" rel="stylesheet">
    <link href="<?php echo e(asset('assest/css/animate.css')); ?>" type="text/css" rel="stylesheet">
    <link href="<?php echo e(asset('assest/css/aos.css')); ?>" type="text/css" rel="stylesheet">
    <link href="<?php echo e(asset('assest/css/style.css')); ?>" type="text/css" rel="stylesheet">
</head>
<body>
<!--Header-->
<div class="HeaderTop">
    <div class="container">
        <div class="d-flex justify-content-between align-items-center">
            <div class="header-top-col">
                <nav class="navbar header-t-links navbar-expand-lg">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon d-flex align-items-center justify-content-center"><i class="fas fa-bars"></i></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Rellow Food Offers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Find a Warehouse</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Get Email Offers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Customer Service</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="header-top-col">
                <ul class="CallUs">
                    <li> <i class="fas fa-phone-alt"></i>
                        <h5><span>Call Us Now</span><a href="tel:9964740564"> 9964740564</a></h5>
                    </li>
                    <li> <i class="far fa-envelope-open"></i>
                        <h5><span>Support</span><a href="mailto:Info@emetro.in"> Info@emetro.in</a></h5>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="HeaderMiddleSection">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-3 col-lg-3 col-md-3">
                <div class="Logo">
                    <a href="/">
                        <img src="assest/images/logo.png">
                    </a>
                </div>
            </div>
            <div class="col-xl-6 col-lg-5 col-md-5">
                <div class="search-form">
                    <form>
                        <input type="text" placeholder="Enter Product/City/Services..." required="">
                        <button type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-4">
                <div class="account-cart">
                    <ul>
                        <li class="d-xs-none"> <a href="/sign-in">Sign In / Register</a></li>

                        <li class="d-xs-none"> <a href="#">Order & Reorder </a></li>
                        <li class="cart-count"> <a href="/cart"><i class="fas fa-shopping-basket"></i>Cart <span class="count">6</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="navigation">
    <div class="NavBg">
        <div class="container">
            <nav>
                <p id="nav-toggle" class="nav-mobile"><span></span></p>
                <ul class="nav-list">

                    <li> <a href="javascript:void(0);" class="menu">Shop By: Categories <i class="fa fa-caret-down"></i></a>
                        <div class="nav-dropdown">
                            <div class="row">
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Clothing & Fashion Accessories</h6>
                                        <ul>
                                            <li><a href="/category">Sarees</a></li>
                                            <li><a href="/category">Salwar, Kurtis & Tops</a></li>
                                            <li><a href="#">Readymade Blouses</a></li>
                                            <li><a href="#">Shawls & Stoles</a></li>
                                            <li><a href="#">Ghagra & Lehengas</a></li>
                                            <li><a href="#">Jackets  & Waistcoats</a></li>
                                            <li><a href="#">Bags ,Shoes & Belts </a></li>
                                            <li><a href="#">Shoes & Slippers</a></li>
                                            <li><a href="#">Earings, Necklace & Bracelets </a></li>
                                            <li><a href="#">Uniforms</a></li>
                                            <li><a href="#">Kurta & Pyjama </a></li>
                                            <li><a href="#">Jacket & Waistcoats </a></li>
                                            <li><a href="#">Sherwani & Dhotis</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Handicrafts & Decoratives</h6>
                                        <ul>
                                            <li><a href="#">Jute, Cotton, Bamboo Items </a></li>
                                            <li><a href="#">Glass Arts</a></li>
                                            <li><a href="#">Pooja Items</a></li>
                                            <li><a href="#">Marriage Decorative Items</a></li>
                                            <li><a href="#">Paintings,  Murals ,Sculptures</a></li>
                                            <li><a href="#">Miniature  , Terrace Gardens </a></li>
                                            <li><a href="#">Macrame & Crochet arts</a></li>
                                            <li><a href="#">Idols & Statues</a></li>
                                            <li><a href="#">Wall Hangings, Vase </a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Furniture & Furnishings</h6>
                                        <ul>
                                            <li><a href="#">Curtains & Carpets</a></li>
                                            <li><a href="#">Bedcover, & Cushions</a></li>
                                            <li><a href="#">Wooden Furnitures</a></li>
                                            <li><a href="#">Wrought Iron Furnitures</a></li>
                                            <li><a href="#">Bamboo Furnitures</a></li>
                                            <li><a href="#">Plastic Furnitures</a></li>
                                            <li><a href="#">Glass Furnitures</a></li>
                                            <li><a href="#">Concrete Furnitures</a></li>
                                            <li><a href="#">Bombay Furnitures</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Electricals & Electronics</h6>
                                        <ul>
                                            <li><a href="#">UPS and Invertors</a></li>
                                            <li><a href="#">LED and LCDS</a></li>
                                            <li><a href="#">Solar Water Heater</a></li>
                                            <li><a href="#">Air Purifier </a></li>
                                            <li><a href="#">Air Conditioner & Accessories</a></li>
                                            <li><a href="#">Home Automation</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Hardware & Softwares</h6>
                                        <ul>
                                            <li><a href="#">Retails Software</a></li>
                                            <li><a href="#">ERP</a></li>
                                            <li><a href="#">SAP</a></li>
                                            <li><a href="#">Video Conferencing</a></li>
                                            <li><a href="#">Customised Applications</a></li>
                                            <li><a href="#">Repair & Maintenance</a></li>
                                            <li><a href="#">Desktop, Laptop & Notebooks</a></li>
                                            <li><a href="#">Printers & Scanners</a></li>
                                            <li><a href="#">Computer Accessories</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Groceries</h6>
                                        <ul>
                                            <li><a href="#">Rice, wheat & other Grains </a></li>
                                            <li><a href="#">Dals & Pulses</a></li>
                                            <li><a href="#">Dry Fruits </a></li>
                                            <li><a href="#">Edible oil & Ghee</a></li>
                                            <li><a href="#">Organic pulses &,Cereals</a></li>
                                            <li><a href="#">Organic Flours</a></li>
                                            <li><a href="#">Paper plates & Cups </a></li>
                                            <li><a href="#">Arecanut Plates,Cups & spoons </a></li>
                                            <li><a href="#">Sugar,Salt & Jaggery </a></li>
                                            <li><a href="#">Cleaning Products</a></li>
                                            <li><a href="#">Ready Mixes </a></li>
                                            <li><a href="#">Condiments & Snacks </a></li>
                                            <li><a href="#">Idli & Dosa Batter </a></li>
                                            <li><a href="#">Grocery Kit</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Health & Hygiene</h6>
                                        <ul>
                                            <li><a href="#">Yoga Mats</a></li>
                                            <li><a href="#">Sanitary Napkins, Tampons </a></li>
                                            <li><a href="#">Sanitizers & Stands</a></li>
                                            <li><a href="#">Cotton Pads, Buds</a></li>
                                            <li><a href="#">Dental Floss, Mouthwash</a></li>
                                            <li><a href="#">Orthopedic Belts</a></li>
                                            <li><a href="#">Diapers & Nappies</a></li>
                                            <li><a href="#">Toilet Paper & Wipes</a></li>
                                            <li><a href="#">Cotton Pads & Buds</a></li>
                                            <li><a href="#">Thermometer & Oxymeter</a></li>
                                            <li><a href="#">Beds, Mattress & Blankets</a></li>
                                            <li><a href="#">Stretcher & Furniture</a></li>
                                            <li><a href="#">Napkins & Towels</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Organic & Natural Products</h6>
                                        <ul>
                                            <li><a href="#">Skin Care,Body Care & Haircare</a></li>
                                            <li><a href="#">Health Drinks & Soups</a></li>
                                            <li><a href="#">Health Supplements</a></li>
                                            <li><a href="#">Immunity Boosters</a></li>
                                            <li><a href="#">Readymix</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Packaging & Labeling</h6>
                                        <ul>
                                            <li><a href="#">Corrugated Boxes</a></li>
                                            <li><a href="#">Flat Boxes</a></li>
                                            <li><a href="#">Plastic Kraft Paper Box</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Manufacturers -OEMs</h6>
                                        <ul>
                                            <li><a href="#">Aerospace</a></li>
                                            <li><a href="#">Automobiles</a></li>
                                            <li><a href="#">Medical Equipments</a></li>
                                            <li><a href="#">Defense </a></li>
                                            <li><a href="#">Textiles & Garments</a></li>
                                            <li><a href="#">Consumer Electronics</a></li>
                                            <li><a href="#">Kitchen Appliances</a></li>
                                            <li><a href="#">Safety  & Security</a></li>
                                            <li><a href="#">Agriculture</a></li>
                                            <li><a href="#">Construction</a></li>
                                            <li><a href="#">Office Automation</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Foods & Condiments</h6>
                                        <ul>
                                            <li><a href="#">Masala Powders & Paste</a></li>
                                            <li><a href="#">Dry/Baked Items</a></li>
                                            <li><a href="#">Frozen Foods</a></li>
                                            <li><a href="#">Beverages & Health Drinks</a></li>
                                            <li><a href="#">Pickles & Thoku</a></li>
                                            <li><a href="#">Appalams & Sandige</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu">Shop By: Brands <i class="fa fa-caret-down"></i></a>
                        <div class="nav-dropdown">
                            <div class="row">
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Clothing & Fashion Accessories</h6>
                                        <ul>
                                            <li><a href="category.blade.php">Sarees</a></li>
                                            <li><a href="category.blade.php">Salwar, Kurtis & Tops</a></li>
                                            <li><a href="#">Readymade Blouses</a></li>
                                            <li><a href="#">Shawls & Stoles</a></li>
                                            <li><a href="#">Ghagra & Lehengas</a></li>
                                            <li><a href="#">Jackets  & Waistcoats</a></li>
                                            <li><a href="#">Bags ,Shoes & Belts </a></li>
                                            <li><a href="#">Shoes & Slippers</a></li>
                                            <li><a href="#">Earings, Necklace & Bracelets </a></li>
                                            <li><a href="#">Uniforms</a></li>
                                            <li><a href="#">Kurta & Pyjama </a></li>
                                            <li><a href="#">Jacket & Waistcoats </a></li>
                                            <li><a href="#">Sherwani & Dhotis</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Handicrafts & Decoratives</h6>
                                        <ul>
                                            <li><a href="#">Jute, Cotton, Bamboo Items </a></li>
                                            <li><a href="#">Glass Arts</a></li>
                                            <li><a href="#">Pooja Items</a></li>
                                            <li><a href="#">Marriage Decorative Items</a></li>
                                            <li><a href="#">Paintings,  Murals ,Sculptures</a></li>
                                            <li><a href="#">Miniature  , Terrace Gardens </a></li>
                                            <li><a href="#">Macrame & Crochet arts</a></li>
                                            <li><a href="#">Idols & Statues</a></li>
                                            <li><a href="#">Wall Hangings, Vase </a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Furniture & Furnishings</h6>
                                        <ul>
                                            <li><a href="#">Curtains & Carpets</a></li>
                                            <li><a href="#">Bedcover, & Cushions</a></li>
                                            <li><a href="#">Wooden Furnitures</a></li>
                                            <li><a href="#">Wrought Iron Furnitures</a></li>
                                            <li><a href="#">Bamboo Furnitures</a></li>
                                            <li><a href="#">Plastic Furnitures</a></li>
                                            <li><a href="#">Glass Furnitures</a></li>
                                            <li><a href="#">Concrete Furnitures</a></li>
                                            <li><a href="#">Bombay Furnitures</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Electricals & Electronics</h6>
                                        <ul>
                                            <li><a href="#">UPS and Invertors</a></li>
                                            <li><a href="#">LED and LCDS</a></li>
                                            <li><a href="#">Solar Water Heater</a></li>
                                            <li><a href="#">Air Purifier </a></li>
                                            <li><a href="#">Air Conditioner & Accessories</a></li>
                                            <li><a href="#">Home Automation</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Hardware & Softwares</h6>
                                        <ul>
                                            <li><a href="#">Retails Software</a></li>
                                            <li><a href="#">ERP</a></li>
                                            <li><a href="#">SAP</a></li>
                                            <li><a href="#">Video Conferencing</a></li>
                                            <li><a href="#">Customised Applications</a></li>
                                            <li><a href="#">Repair & Maintenance</a></li>
                                            <li><a href="#">Desktop, Laptop & Notebooks</a></li>
                                            <li><a href="#">Printers & Scanners</a></li>
                                            <li><a href="#">Computer Accessories</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Groceries</h6>
                                        <ul>
                                            <li><a href="#">Rice, wheat & other Grains </a></li>
                                            <li><a href="#">Dals & Pulses</a></li>
                                            <li><a href="#">Dry Fruits </a></li>
                                            <li><a href="#">Edible oil & Ghee</a></li>
                                            <li><a href="#">Organic pulses &,Cereals</a></li>
                                            <li><a href="#">Organic Flours</a></li>
                                            <li><a href="#">Paper plates & Cups </a></li>
                                            <li><a href="#">Arecanut Plates,Cups & spoons </a></li>
                                            <li><a href="#">Sugar,Salt & Jaggery </a></li>
                                            <li><a href="#">Cleaning Products</a></li>
                                            <li><a href="#">Ready Mixes </a></li>
                                            <li><a href="#">Condiments & Snacks </a></li>
                                            <li><a href="#">Idli & Dosa Batter </a></li>
                                            <li><a href="#">Grocery Kit</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Health & Hygiene</h6>
                                        <ul>
                                            <li><a href="#">Yoga Mats</a></li>
                                            <li><a href="#">Sanitary Napkins, Tampons </a></li>
                                            <li><a href="#">Sanitizers & Stands</a></li>
                                            <li><a href="#">Cotton Pads, Buds</a></li>
                                            <li><a href="#">Dental Floss, Mouthwash</a></li>
                                            <li><a href="#">Orthopedic Belts</a></li>
                                            <li><a href="#">Diapers & Nappies</a></li>
                                            <li><a href="#">Toilet Paper & Wipes</a></li>
                                            <li><a href="#">Cotton Pads & Buds</a></li>
                                            <li><a href="#">Thermometer & Oxymeter</a></li>
                                            <li><a href="#">Beds, Mattress & Blankets</a></li>
                                            <li><a href="#">Stretcher & Furniture</a></li>
                                            <li><a href="#">Napkins & Towels</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Organic & Natural Products</h6>
                                        <ul>
                                            <li><a href="#">Skin Care,Body Care & Haircare</a></li>
                                            <li><a href="#">Health Drinks & Soups</a></li>
                                            <li><a href="#">Health Supplements</a></li>
                                            <li><a href="#">Immunity Boosters</a></li>
                                            <li><a href="#">Readymix</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Packaging & Labeling</h6>
                                        <ul>
                                            <li><a href="#">Corrugated Boxes</a></li>
                                            <li><a href="#">Flat Boxes</a></li>
                                            <li><a href="#">Plastic Kraft Paper Box</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Manufacturers -OEMs</h6>
                                        <ul>
                                            <li><a href="#">Aerospace</a></li>
                                            <li><a href="#">Automobiles</a></li>
                                            <li><a href="#">Medical Equipments</a></li>
                                            <li><a href="#">Defense </a></li>
                                            <li><a href="#">Textiles & Garments</a></li>
                                            <li><a href="#">Consumer Electronics</a></li>
                                            <li><a href="#">Kitchen Appliances</a></li>
                                            <li><a href="#">Safety  & Security</a></li>
                                            <li><a href="#">Agriculture</a></li>
                                            <li><a href="#">Construction</a></li>
                                            <li><a href="#">Office Automation</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="MenuDrop">
                                        <h6>Foods & Condiments</h6>
                                        <ul>
                                            <li><a href="#">Masala Powders & Paste</a></li>
                                            <li><a href="#">Dry/Baked Items</a></li>
                                            <li><a href="#">Frozen Foods</a></li>
                                            <li><a href="#">Beverages & Health Drinks</a></li>
                                            <li><a href="#">Pickles & Thoku</a></li>
                                            <li><a href="#">Appalams & Sandige</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu">Membership<i class="fa fa-caret-down"></i></a>
                        <div class="nav-dropdown" style="width: 350px !important; height: auto; display: none;">
                            <div class="row" style="height : auto !important;">

                                <div class="MenuDrop">
                                    <div><a href="#" class="text-info">Buy Membership</a></div>
                                    <div><a href="#" class="text-info">Renew Membership</a></div>
                                    <div><a href="#" class="text-info">Why Become A Member</a></div>

                                    <div style="margin-top: 15px;margin-bottom: 15px;">
                                        <img src="https://emetro.demolink.co.in/new_assest/images/membership_card.jpeg" width="320" height="200">
                                    </div>
                                </div>





                            </div>
                        </div>
                    </li>
                </ul>

            </nav>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>

<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/layout/header.blade.php ENDPATH**/ ?>