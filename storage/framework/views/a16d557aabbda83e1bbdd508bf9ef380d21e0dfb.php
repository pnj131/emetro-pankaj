<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="breadcrumb">
    <div class="container">
        <ol class="breadcrumb-list">
            <li>
                <a href="/">
                    <span>Home</span>
                </a>
            </li>
            <li>
                <strong> Return And Exchange</strong>
            </li>
        </ol>
    </div>
</div>
<section>
    <div class="catalog-view">
        <div class="container">
            <div class="catalog-view-row">
            </div>
            <!-- Product Details Start -->
            <div class="product-info">
                <ul class="product-tablist row m-0">
                    <li class="col p-0">
                        <a href="#product-details" class="scrollLink active">
                            <h2>eMetro Disclosure Regarding Human Trafficking and Anti-Slavery</h2>
                        </a>
                    </li>
                </ul>
                <div class="product-i-tabs row m-0">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">

                                <br>
                                <p>eMetro has a global supplier Code of Conduct, which prohibits human rights abuses in our supply chain. Practices such as human trafficking,
                                    physical abuse, restricting freedom of movement, confiscation of passports and other documentation, unsafe work environments, failure to pay
                                    adequate wages, excessive or forced overtime, illegal child labor, and many other aspects of worker welfare are addressed by the Code.
                                    By signing eMetro’s supplier agreement,
                                    the supplier warrants compliance with the Code, including by its sub-suppliers. We may acknowledge and accept a supplier's code as equivalent to our Code.
                                </p>
                                <br>
                               <p>
                                   To evaluate compliance, we arrange for the audit of certain facilities of selected suppliers, with an emphasis on suppliers of private-label
                                   merchandise and suppliers whose product or country of origin poses an increased risk. Audits are performed by independent third-party auditors who
                                   specialize in social responsibility audits. While we retain the right to conduct unannounced audits, as a practical matter, some minimum notice is given to comply
                                   with security concerns and to allow the supplier to collect records that are reviewed during the audit. Sub-suppliers generally are outside the scope of the audit.
                               </p><br>
                                <p>
                                    Unfortunately, violations of our Code still occur. When we discover a violation, we respond in a manner commensurate with the nature and extent of the violation.
                                    Certain violations may result in immediate remedial action, including termination of the business relationship. Depending on the circumstances,
                                    we may allow a supplier reasonable time to develop and implement a plan for remediation. In those instances we conduct follow-up audits to monitor progress.
                                </p><br>
                                <p>
                                    In general, we prefer working with the supplier to correct Code violations rather than immediately terminating the relationship.
                                    Termination is unlikely to correct the underlying issue and may cause further hardship to workers and their families who depend upon the employment.
                                    However, if the supplier fails to make satisfactory progress toward improvement, we will cease our business relationship with that supplier.
                                </p><br>
                                <p>
                                    Members of eMetro's buying team who manage a supplier relationship will continue to be provided with in-person and online training.
                                    The training covers the Code of Conduct and its importance to our business and to the workers who produce the merchandise we sell.
                                    We encourage anyone who is aware of violations of the law or our Code to notify their management, our Code of Conduct Compliance team.
                                </p><br>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/footer/supply-chain-disclosure.blade.php ENDPATH**/ ?>