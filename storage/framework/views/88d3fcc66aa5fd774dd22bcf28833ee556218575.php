<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="LinkListing">
                    <h5>About us</h5>
                    <ul class="LinkList">
                       <li><a href="/charitable-contribution">Charitable Contributions</a></li>
                        <li><a href="company-information">Company Information</a></li>
                        <li><a href="#">Sustainability Commitment</a></li>
                        <li><a href="#">Investor Relations</a></li>
                        <li><a href="jobs">Jobs</a></li>
                        <li><a href="#">Logo and Media Requests</a></li>
                        <li><a href="#">Product Videos</a></li>
                        <li><a href="#">The emetro Connection</a></li>
                        <li><a href="#">emetro Blog</a></li>
                        <li><a href="#">Employee Site</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="LinkListing">
                    <h5>Membership</h5>
                    <ul class="LinkList">
                        <li><a href="/join-now">Join Now</a></li>
                        <li><a href="/member-privilege">Member Privileges</a></li>
                        <li><a href="#">Executive Membership Terms</a></li>
                        <li><a href="#">Sign In or Register</a></li>
                        <li><a href="#">Credit Card</a></li>
                        <li><a href="/vendors-and-suppliers">Vendors & Suppliers</a></li>
                        <li><a href="/supply-chain-disclosure">Supply Chain Disclosure</a></li>
                        <li><a href="#">Supplier Diversity</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="LinkListing">
                    <h5>Customer Service</h5>
                    <ul class="LinkList">
                        <li><a href="#">Emetro Shop Card Balance</a></li>
                        <li><a href="#">Order By Item Number</a></li>
                        <li><a href="#">Support & Warranty</a></li>
                        <li><a href="#">Export & Domestic Volume Sales</a></li>
                        <li><a href="#">Order Status</a></li>
                        <li><a href="/preventing-fraud">Preventing Fraud</a></li>
                        <li><a href="#">Shipping</a></li>
                        <li><a href="#">Rebates</a></li>
                        <li><a href="#">Recalls & Product Notices</a></li>
                        <li><a href="/return-and-exchange">Returns and Exchanges</a></li>
                        <li><a href="#">Returns Policy</a></li>
                        <li><a href="accesebility">Accessibility</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="LinkListing">
                    <h5>Locations & Services </h5>
                    <ul class="LinkList Last">
                        <li><a href="#">Find a Warehouse</a></li>
                        <li><a href="#">Find Locations-coming soon</a></li>
                        <li><a href="#">Hours and Holiday Closures</a></li>
                        <li><a href="#">Special Events</a></li>
                    </ul>
                    <ul class="SocialLink">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="FooterBootm">
    <p>Designed and Developed by whetstone CG | All right reserved.</p>
</div>
<!-- End -->

<!-- Delivery Modal Start -->
<div class="modal fade delivery-row" tabindex="-1" role="dialog" id="delivery-modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h2>2 Day Delivery</h2>
                <hr>
                <p>2-Day and Same-Day product availability vary by area. In order to view the products available in your area, please provide your zip cod</p>
                <div class="delivery-postal-popup">
                    <input id="postal-code-input" class="form-control" name="deliveryPostalCode" type="text" placeholder="ZIP Code">
                    <button type="button" class="btn btn-primary">Set Delivery ZIP Code</button>
                    <button type="button" class="btn btn-primary">Continue Shopping</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Delivery Modal End -->


<script src="<?php echo e(asset('assest/js/jquery-3.4.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('assest/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assest/js/owl.carousel.js')); ?>"></script>
<script src="<?php echo e(asset('assest/js/aos.js')); ?>"></script>
<script src="<?php echo e(asset('assest/js/main.js')); ?>"></script>
</body>
</html>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/layout/footer.blade.php ENDPATH**/ ?>