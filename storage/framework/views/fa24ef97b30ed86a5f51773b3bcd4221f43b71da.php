<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!--Header-->

<!-- Category Page Start  -->
<div class="breadcrumb">
	<div class="container">
		<ol class="breadcrumb-list">
			<li>
				<a href="/">
					<span>Home</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span>Jewelry, Watches &amp; Sunglasses</span>
				</a>
			</li>
			<li>
				<strong> Sunglasses </strong>
			</li>
		</ol>
	</div>
</div>
<div class="catalog-view">
	<div class="container">
		<div class="catalog-view-row">
			<!-- Product Slider Start -->
			<div class="catalog-view-col product-slider">
				<div id="sync1" class="slider owl-carousel">
				  <div class="item"><img src="assest/images/MTR1.jpeg"></div>
				  <div class="item"><img src="assest/images/p5.jpg"></div>
				  <div class="item"><img src="assest/images/p6.jpg"></div>
				  <div class="item"><img src="assest/images/MTR5.jpeg"></div>
				  <div class="item"><img src="assest/images/MTR1.jpeg"></div>
				  <div class="item"><img src="assest/images/p5.jpg"></div>

				</div>
				<div id="sync2" class="navigation-thumbs owl-carousel">
				  <div class="item"><img src="assest/images/MTR1.jpeg"></div>
				  <div class="item"><img src="assest/images/p5.jpg"></div>
				  <div class="item"><img src="assest/images/p6.jpg"></div>
				  <div class="item"><img src="assest/images/MTR5.jpeg"></div>
				  <div class="item"><img src="assest/images/MTR1.jpeg"></div>
				  <div class="item"><img src="assest/images/p5.jpg"></div>

				</div>
			</div>
			<!-- Product Slider End -->
			<!-- product Details Start -->
			<div class="catalog-view-col product-details">
				<div class="product-info-main">
					<div class="page-title-wrapper">
						<h1 class="page-title">
							<span class="base">Ray-Ban RB4216 Light Havana Polarized Sunglasses</span>
						</h1>
					</div>
					<div class="modal-details">
						<ul>
							<li>
								<span class="item-p-name">Item</span> <span class="item-p-details">2111001 </span>
							</li>
							<li>
								<span class="item-p-name">Model</span> <span class="item-p-details">241967C </span>
							</li>
						</ul>
					</div>
					<div class="product-reviews-summary empty">
						<div class="reviews-actions">
							<a class="action add" href="#">
								<div class="ratings">
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
									<i class="fas fa-star"></i>
								</div>  Write a review </a>
						</div>
					</div>
					<div class="price-box">
						<div class="normal-price d-flex justify-content-between">
							<label>Your Price</label>
							<div class="pull-right price">
								<span class="currency">$</span>
								<span class="value">229.99</span>
							</div>
						</div>
					</div>
					<div class="shipping-statement">S
						hipping &amp; Handling Included*
					</div>
					<div class="p-features">
						<h4>Features:</h4>
						<ul class="p-features-row">
							<li>Available in Blue or Black Dial</li>
							<li> Stainless Steel Case &amp; Bracelet</li>
							<li> Swiss Quartz Movement</li>
							<li> 100m Water Resistance</li>
							<li> Swiss Made</li>
						</ul>
					</div>
					<div class="product-options-wrapper">
						<div class="swatch-attribute color">
							<div class="swatch-attribute-top">
								<span class="swatch-attribute-label">Color</span>
								<span class="swatch-attribute-selected-option">Black</span>
							</div>
							<div class="swatch-attribute-options">
								<div class="swatch-option color selected">
									blue
								</div>
								<div class="swatch-option color">
									Black
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Product Slider End -->
			<!-- Add to cart Start -->
			<div class="catalog-view-col product-interaction">
				<div class="product-i-row">
					<div class="quantity-selector-v2">
						<button id="minusQty" type="button" name="minusQty" class="btn">
							<i class="fas fa-minus"></i>
						</button>
						<input type="tel" id="minQtyText" class="form-control valid text-center" value="1">
						<button id="plusQty" type="button" name="plusQty" class="btn">
							<i class="fas fa-plus"></i>
						</button>
					</div>
					<div class="box-tocart">
						<button type="submit" title="Add to Cart" class="btn text-center">
							<span>Add to Cart</span>
						</button>
					</div>
					<div class="add-to-list">
						<button class="add-to-list-link btn text-center">
							<span class="add-to-list-bar"><i class="fas fa-bars"></i></span>
							<span>Add to List </span>
						</button>
					</div>
					<div class="messages visible-lg visible-xl visible-xs visible-sm visible-md">
						<p class="primary-clause">Arrives approximately 3 - 5 business days from time of order.</p>

					</div>
				</div>
			</div>
			<!-- Add to cart End -->
		</div>
		<!-- Product Details Start -->
		<div class="product-info">
			<ul class="product-tablist row m-0">
				<li class="col p-0">
					<a href="#product-details" class="scrollLink active">
						<span>Product Details</span>
					</a>
				</li>
				<li class="col p-0">
					<a href="#specifications" class="scrollLink">
						<span>Specifications</span>
					</a>
				</li>
				<li class="col p-0">
					<a href="#shipping-returns" class="scrollLink">
						<span>Shipping &amp; Returns</span>
					</a>
				</li>
				<li class="col p-0">
					<a href="#nav-pdp-tab-header-12" class="scrollLink">
						<span>Reviews</span>
					</a>
				</li>
			</ul>
			<div class="product-i-tabs row m-0">
				<div class="product-i-box col-lg-8 col-md-12 pl-0 pr-0" id="product-details">
					<h3 class="section-title">Product Details</h3>
					<div class="product-info-description product-i-content">
						<span style="font-weight:bold;">Item may be available at your local warehouse for a lower, non-delivered price.</span><br><br><span style="font-weight:bold;">Features:</span><br>
							<ul>
								<li>Brand: Kirkland Signature </li>
								<li>Frame Material: Metal</li>
								<li>Lens Material: Tri Acetate Cellulose</li>
								<li>UV Protection: 100%</li>
								<li>Polarized</li>
							</ul>
							<span style="font-weight:bold;">Eye Size:</span> 59<br><span style="font-weight:bold;">Bridge/Temple Size: </span>16 / 140<br>
						<br>
 						<div class="prop-warning"><span style="font-weight:bold;">WARNING: </span> This product can expose you to chemicals including nickel, which is known to the State of California to cause cancer and birth defects or other reproductive harm. For more information go to: <a href="#" class="external">www.P65Warnings.ca.gov</a>.</div>
 					</div>
				</div>
				<div class="product-i-box col-lg-8 col-md-12 pl-0 pr-0" id="specifications">
					<h3 class="section-title">Product Details</h3>
					<div class="product-info-specification product-i-content">
					 	<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">Brand</div>
							<div class="col-xs-6 col-md-7 col-lg-8">Kirkland Signature</div>
						</div>
						<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">Case Type</div>
							<div class="col-xs-6 col-md-7 col-lg-8">Hard</div>
						</div>
						<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">Color</div>
							<div class="col-xs-6 col-md-7 col-lg-8">Silver</div>
						</div>
						<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">Frame Made In</div>
							<div class="col-xs-6 col-md-7 col-lg-8">China</div>
						</div>
						<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">Frame Material</div>
							<div class="col-xs-6 col-md-7 col-lg-8">Metal</div>
						</div>
						<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">Gender</div>
							<div class="col-xs-6 col-md-7 col-lg-8">Men's</div>
						</div>
						<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">Lens Color</div>
							<div class="col-xs-6 col-md-7 col-lg-8">Gray</div>
						</div>
						<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">Lens Made In</div>
							<div class="col-xs-6 col-md-7 col-lg-8">China</div>
						</div>
						<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">Lens Material</div>
								<div class="col-xs-6 col-md-7 col-lg-8">Tri Acetate</div>
						</div>
						<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">Lens Type</div>
							<div class="col-xs-6 col-md-7 col-lg-8">Polarized</div>
						</div>
						<div class="row">
							<div class="spec-name col-xs-6 col-md-5 col-lg-4">UV Protection %</div>
							<div class="col-xs-6 col-md-7 col-lg-8">100 Percent</div>
						</div>
					</div>
				</div>
				<div class="product-i-box col-lg-8 col-md-12 pl-0 pr-0" id="shipping-returns">
					<h3 class="section-title"> Shipping & Returns</h3>
					<div class="product-shipping-box product-i-content">
					If you’re not completely satisfied with this Kirkland Signature product, your money will be refunded.
					<br><br>
					Standard shipping via UPS Ground is included in the quoted price.
					<br><br>
					Express shipping is via UPS.
					<br><br>
					Please choose your shipping method at checkout.
					<br><br>
					An additional Shipping and Handling fee will apply to express shipments. This fee will be quoted at checkout.
					<br><br>
					*Delivery is available to Alaska, Hawaii and Puerto Rico; however, an additional Shipping and Handling fee will apply. This fee will be quoted at checkout. Additional transit time may also be required.
					<br><br>
					Sunglass returns can be made in the Optical department at your local Costco.
					</div>
				</div>
			</div>
		</div>
		<!-- Product Details End -->

		<!-- Related Product Start -->
		<div class="related-products">
			<h3 class="section-title">Product Details</h3>
			<div class="products products-grid">
				<ol class="product-items owl-carousel owl-theme">
					<li class="product-item">
						<div class="product-item-info">
							<div class="product-item-photo">
								<a href="#" class="product-item-photo">
									<img src="assest/images/MTR1.jpeg">
								</a>
							</div>
							<div class="product-item-details">
								<div class="col-xs-12 badge-class">
									<i class="fas fa-bars"></i>
								</div>
								<div class="member-only">
									<span class="co-c"><i class="far fa-user"></i></span>
									Member Only Item
								</div>
								<div class="product name product-item-name">
									<a class="product-item-link" href="#">
										Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
									</a>
								</div>
								<div class="ratings">
									<div class="ratings">
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</div>
								</div>
								<div class="compare-product">
									<a href="#" rel="follow">
										<input id="compare_product" type="checkbox">
										<label for="compare_product">
											Compare Product
										</label>
									</a>
								</div>
							</div>
						</div>
					</li>
					<li class="product-item">
						<div class="product-item-info">
							<div class="product-item-photo">
								<a href="#" class="product-item-photo">
									<img src="assest/images/p5.jpg">
								</a>
							</div>
							<div class="product-item-details">
								<div class="col-xs-12 badge-class">
									<i class="fas fa-bars"></i>
								</div>
								<div class="member-only">
									<span class="co-c"><i class="far fa-user"></i></span>
									Member Only Item
								</div>
								<div class="product name product-item-name">
									<a class="product-item-link" href="#">
										Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
									</a>
								</div>
								<div class="ratings">
									<div class="ratings">
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</div>
								</div>
								<div class="compare-product">
									<a href="#" rel="follow">
										<input id="compare_product" type="checkbox">
										<label for="compare_product">
											Compare Product
										</label>
									</a>
								</div>
							</div>
						</div>
					</li>
					<li class="product-item">
						<div class="product-item-info">
							<div class="product-item-photo">
								<a href="#" class="product-item-photo">
									<img src="assest/images/p6.jpg">
								</a>
							</div>
							<div class="product-item-details">
								<div class="col-xs-12 badge-class">
									<i class="fas fa-bars"></i>
								</div>
								<div class="member-only">
									<span class="co-c"><i class="far fa-user"></i></span>
									Member Only Item
								</div>
								<div class="product name product-item-name">
									<a class="product-item-link" href="#">
										Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
									</a>
								</div>
								<div class="ratings">
									<div class="ratings">
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</div>
								</div>
								<div class="compare-product">
									<a href="#" rel="follow">
										<input id="compare_product" type="checkbox">
										<label for="compare_product">
											Compare Product
										</label>
									</a>
								</div>
							</div>
						</div>
					</li>
					<li class="product-item">
						<div class="product-item-info">
							<div class="product-item-photo">
								<a href="#" class="product-item-photo">
									<img src="assest/images/MTR5.jpeg">
								</a>
							</div>
							<div class="product-item-details">
								<div class="col-xs-12 badge-class">
									<i class="fas fa-bars"></i>
								</div>
								<div class="member-only">
									<span class="co-c"><i class="far fa-user"></i></span>
									Member Only Item
								</div>
								<div class="product name product-item-name">
									<a class="product-item-link" href="#">
										Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
									</a>
								</div>
								<div class="ratings">
									<div class="ratings">
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</div>
								</div>
								<div class="compare-product">
									<a href="#" rel="follow">
										<input id="compare_product" type="checkbox">
										<label for="compare_product">
											Compare Product
										</label>
									</a>
								</div>
							</div>
						</div>
					</li>
					<li class="product-item">
						<div class="product-item-info">
							<div class="product-item-photo">
								<a href="#" class="product-item-photo">
									<img src="assest/images/p2.jpg">
								</a>
							</div>
							<div class="product-item-details">
								<div class="col-xs-12 badge-class">
									<i class="fas fa-bars"></i>
								</div>
								<div class="member-only">
									<span class="co-c"><i class="far fa-user"></i></span>
									Member Only Item
								</div>
								<div class="product name product-item-name">
									<a class="product-item-link" href="#">
										Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
									</a>
								</div>
								<div class="ratings">
									<div class="ratings">
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</div>
								</div>
								<div class="compare-product">
									<a href="#" rel="follow">
										<input id="compare_product" type="checkbox">
										<label for="compare_product">
											Compare Product
										</label>
									</a>
								</div>
							</div>
						</div>
					</li>
					<li class="product-item">
						<div class="product-item-info">
							<div class="product-item-photo">
								<a href="#" class="product-item-photo">
									<img src="assest/images/p2.jpg">
								</a>
							</div>
							<div class="product-item-details">
								<div class="col-xs-12 badge-class">
									<i class="fas fa-bars"></i>
								</div>
								<div class="member-only">
									<span class="co-c"><i class="far fa-user"></i></span>
									Member Only Item
								</div>
								<div class="product name product-item-name">
									<a class="product-item-link" href="#">
										Perfect Aire 12000 BTU Single Zone Outdoor Unit - Forest Series 115V
									</a>
								</div>
								<div class="ratings">
									<div class="ratings">
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
										<i class="fas fa-star"></i>
									</div>
								</div>
								<div class="compare-product">
									<a href="#" rel="follow">
										<input id="compare_product" type="checkbox">
										<label for="compare_product">
											Compare Product
										</label>
									</a>
								</div>
							</div>
						</div>
					</li>
				</ol>
			</div>
		</div>
		<!-- Related Product End -->
	</div>
</div>

<!-- Footer -->
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/product.blade.php ENDPATH**/ ?>