<?php echo $__env->make('layout.login-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!--Header-->

<!-- Login Start -->
<div class="g-login">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col">
				<div class="page-title-wrapper">
					<h1 class="page-title">
						<span class="base">Password Reset</span>
					</h1>
				</div>
				<div class="g-login-bg">
					<form>
						<div class="intro form-group">Enter your account email address to receive a verification code to reset your password.</div>
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email Address">
						</div>
						<button type="submit" class="btn btn-primary btn-block">Send Verification Code</button>
					</form>
  					<div class="sign-in-btn">
						<a href="#" class="btn btn-block">
							Cancel
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Login End -->
<!-- Footer -->
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/forgot-password.blade.php ENDPATH**/ ?>