<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Emetro</title>
    <link rel = "icon" href ="<?php echo e(asset('assest/images/fav.png')); ?>">
    <link href="<?php echo e(asset('assest/css/bootstrap.min.css')); ?>"  type="text/css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"/>
    <link href="<?php echo e(asset('assest/css/owl.carousel.min.css')); ?>" type="text/css" rel="stylesheet">
    <link href="<?php echo e(asset('assest/css/animate.css')); ?>" type="text/css" rel="stylesheet">
    <link href="<?php echo e(asset('assest/css/aos.css')); ?>" type="text/css" rel="stylesheet">
    <link href="<?php echo e(asset('assest/css/style.css')); ?>" type="text/css" rel="stylesheet">
</head>
<body class="account-page">
<!--Header-->

<div class="HeaderMiddleSection">
    <div class="container">
        <div class="Content">
            <div class="MiddleWrap LeftWrap">
                <ul class="CallUs">
                    <li> <i class="fas fa-phone-alt"></i>
                        <h5><span>Call Us Now</span><a href="tel:9964740564"> 9964740564</a></h5>
                    </li>
                </ul>
            </div>
            <div class="LeftWrap">
                <div class="Logo"><a href="/"><img src="assest/images/logo.png"></a></div>
            </div>
            <div class="MiddleWrap">
                <ul class="CallUs">
                    <li> <i class="far fa-envelope-open"></i>
                        <h5><span>Support</span><a href="mailto:Info@emetro.in"> Info@emetro.in</a></h5>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/layout/login-header.blade.php ENDPATH**/ ?>