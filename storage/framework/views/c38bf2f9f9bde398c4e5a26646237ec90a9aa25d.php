<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="breadcrumb">
    <div class="container">
        <ol class="breadcrumb-list">
            <li>
                <a href="/">
                    <span>Home</span>
                </a>
            </li>

            <li>
                <strong> About US </strong>
            </li>
        </ol>
    </div>
</div>
<section>
    <div class="catalog-view">
        <div class="container">
            <div class="catalog-view-row">

            </div>
            <!-- Product Details Start -->
            <div class="product-info">
                <ul class="product-tablist row m-0">
                    <li class="col p-0">
                        <a href="#product-details" class="scrollLink active">
                            <h2>About Us</h2>
                        </a>
                    </li>
                </ul>
                <div class="product-i-tabs row m-0">
                   <div class="row">
                       <div class="col-md-6 col-lg-6 col-sm-6">
                           <div class="product-i-box  pl-0 pr-0" id="product-details">
                               <h3 class="section-title">Did You Know?</h3>
                               <div class="product-info-description product-i-content">
                                   <span >eMetro is a retailer with warehouse operations in Bangalore, India.
                                       We are the recognized leader in our field, dedicated to quality in every area of our business and respected for our outstanding business ethics.
                                       Despite our large size and explosive expansion, we continue to provide a family atmosphere in which our employees thrive and succeed.
                                   </span>
                               </div>
                           </div>
                       </div>
                       <div class="col-md-6 col-lg-6 col-sm-6">
                           <div class="product-i-box  pl-0 pr-0" id="product-details">
                               <h3 class="section-title">What Is eMetro?</h3>
                               <div class="product-info-description product-i-content">
                                   <span >eMetro is a membership based retail and bulk Store / eComm, dedicated to bringing our members the best possible prices on quality brand-name merchandise.
                                       With multi-locations in Bangalore, eMetro provides a wide selection of merchandise, plus the convenience of specialty departments and exclusive member services,
                                       all designed to make your shopping experience a pleasurable one.
                                   </span>
                               </div>
                           </div>
                       </div>
                   </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-612 col-sm-12">
                            <div class="product-i-box col-lg-12 col-md-12 pl-0 pr-0" id="product-details">
                                <h3 class="section-title">The History of eMetro</h3>
                                <div class="product-info-description product-i-content">
                                   <p >The company's first location, opened in 2019 under the name of Fortex Trade Technologies Limited.
                                       Originally serving only small businesses, the company found it could achieve far greater buying people by also serving a selected audience of non-business members.
                                       With that change, the growth of the Fortex Trade industry was off and running.
                                       In 2021, the first eMetro Multi Store was opened in Vidyaranyapura, Bangalore, Karnataka. eMetro became the Brand for FORTEX TRADE TECHNOLOGIES LIMITED in Jun 2021.
                                   </p><br>
                                    <p>
                                        Our operating philosophy has been simple. Keep costs down and pass the savings on to our members.
                                        Our large membership base and tremendous buying power, combined with our never-ending quest for efficiency, result in the best possible prices for our members.
                                        Since resuming the eMetro name in 2019, the company has grown with total sales in recent fiscal years exceeding 8 Crores.
                                    </p><br>
                                    <p>
                                        eMetro has transformed the retail world. eMetro was the world's first membership based Store,
                                        a place where efficient buying and operating practices gave members access to unmatched savings.
                                        At first, eMetro was limited exclusively to business members, who could purchase a wide range of supplies and wholesale items.
                                    </p><br>
                                    <p>
                                        Over the next year, both FTTL and eMetro continued to innovate and grow, and in 2021,
                                        the two mega-retailers merged,
                                        creating a gifted leadership team that soon made eMetro as Brand for Fortex Trade Technologies limited the most successful Online Store in Bangalore.
                                    </p><br>
                                    <p>
                                        Today, as the company evolves, it stays true to the qualities that helped attract and retain millions of loyal members in and around Bangalore.
                                    </p><br>
                                    <p>
                                        Commitment to quality. eMetro warehouses carry about 4,000 SKUs (stock keeping units) compared to the 30,000 found at most supermarkets.
                                        By carefully choosing products based on quality, price, brand, and features, the company can offer the best value to members.
                                    </p><br>
                                    <p>
                                        Entrepreneurial spirit. Throughout the years, the entrepreneurial drive for excellence has continued to define eMetro staff at every level.
                                        From its management team to the people on the Store floor, everyone is united in a common goal to exceed member expectations.
                                    </p><br>
                                    <p>
                                        Employee focus. eMetro is often noted for being much more employee-focused than other Fortune 500 companies.
                                        By offering fair wages and top-notch benefits, the company has created a workplace culture that attracts positive,
                                        high-energy, talented employees.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-lg-8 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Our Mission</h3>
                                <div class="product-info-description product-i-content">
                                   <b>To continually provide our members/customers with quality goods and services at the lowest possible prices.
                                   </b><br>
                                    <span>In order to achieve our mission we will conduct our business with the following Code of Ethics in mind:</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Our Code of Ethics</h3>
                                <div class="product-info-description product-i-content">
                                    <p><b>1.	Obey the law.</b></p>
                                    <p> <b>2.	Take care of our members.</b></p>
                                    <p> <b>3.	Take care of our employees.</b></p>
                                    <p>  <b>4.	Respect our suppliers.</b></p><br>
                                    <p>If we do these four things throughout our organization, then we will achieve our ultimate goal, which is to:</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-612 col-sm-12">
                            <div class="product-i-box col-lg-12 col-md-12 pl-0 pr-0" id="product-details">
                                <h3 class="section-title">TheMetro Code of Ethics</h3>
                                <h4 class="section-title">Obey the law</h4><br>
                                <p>The law is irrefutable! Absent a moral imperative to challenge a law,
                                    we must conduct our business in total compliance with the laws of every community where we do business. We pledge to:</p>
                                <br>
                                <div class="product-info-description product-i-content">
                                    <p >•	Comply with all laws and other legal requirements.
                                    </p><br>
                                    <p>
                                        •	Respect all public officials and their positions.
                                    </p><br>

                                    <p>•	Comply with safety and security standards for all products sold.</p><br>
                                    <p>•	Alert management if we observe illegal workplace misconduct by other employees.</p><br>

                                    <p>•	Exceed ecological standards required in every community where we do business.</p><br>
                                    <p>•	Comply with all applicable wage and hour laws.</p><br>
                                    <p>•	Comply with all applicable antitrust laws.</p><br>
                                    <p>•	Conduct business in and with foreign countries in a manner that is legal and proper under United States, India and foreign laws.</p><br>
                                    <p>•	Not offer or give any form of bribe or kickback or other thing of value to any person or pay to obtain or expedite government action
                                            or otherwise act in violation of the Foreign Corrupt Practices Act or the laws of other countries.</p><br>
                                    <p>•	Not request or receive any bribe or kickback.</p><br>
                                    <p>•	Promote fair, accurate, timely, and understandable disclosure in reports filed with the Securities and
                                            Exchange Commission and in other public communications by the</p><br>

                                    </p><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Take care of our members</h3>
                                <div class="product-info-description product-i-content">
                                    <p>eMetro membership is open to business owners, as well as individuals. Our members are our reason for being – the key to our success.
                                        If we don’t keep our members happy, little else that we do will make a difference. There are plenty of shopping alternatives for our
                                        members and if they fail to show up, we cannot survive. Our members have extended a trust to eMetro by virtue of paying a fee to shop with us.
                                        We will succeed only if we do not violate the trust,
                                        they have extended to us, and that trust extends to every area of our business. To continue to earn their trust, we pledge to:.
                                    </p><br>
                                    <p>•	Provide top-quality products at the best prices in the market.</p><br>
                                    <p>   •	Provide high quality, safe and wholesome food products by requiring that both suppliers and employees be in compliance with the highest food
                                        safety standards in the industry.</p><br>
                                    <p>   •	Provide our members with a 100% satisfaction guarantee on every product and service we sell, including their membership fee.</p><br>
                                    <p>    •	Assure our members that every product we sell is authentic in make and in representation of performance.</p><br>
                                    <p>   •	Make our shopping environment a pleasant experience by making our members feel welcome as our guests.</p><br>
                                    <p>   •	Provide products to our members that will be ecologically sensitive.</p><br>
                                    <p>   •	Provide our members with the best customer service in the retail industry.</p><br>
                                    <p>   •	Give back to our communities through employee volunteerism and employee and corporate contributions to United Way and Children’s Hospitals.</p><br>

                                    <span>In order to achieve our mission we will conduct our business with the following Code of Ethics in mind:</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Take care of our employees</h3>
                                <p>Our employees are our most important asset. We believe we have the very best employees in the warehouse club industry,
                                    and we are committed to providing them with rewarding challenges and ample opportunities for personal and career growth.
                                    We pledge to provide our employees with:</p>
                                <div class="product-info-description product-i-content">
                                    <p>•	Competitive wages</p><br>
                                    <p>•	Great benefits</p><br>
                                    <p>•	A safe and healthy work environment</p><br>
                                    <p>•	Challenging and fun work</p><br>
                                    <p>•	Career opportunities</p><br>
                                    <p>•	An atmosphere free from harassment or discrimination</p><br>
                                    <p>•	An Open Door Policy that allows access to ascending levels of management to resolve issues</p><br>
                                    <p>•	Opportunities to give back to their communities through volunteerism and fund-raising Career</p><br>
                                </div>
                                <br>
                                <p>Opportunities at eMetro:</p><br>
                                <p>•	eMetro is committed to promoting from within the Company. The majority of our current management team members
                                    (including Warehouse, Merchandise, Administrative, Membership, Front End and Receiving Managers) are “home grown.”</p>
                                <p>•	Our growth plans remain very aggressive and our need for qualified, experienced employees to fill supervisory and management positions remains great.</p>
                                <p>•	Today we have Location Managers and Vice Presidents who were once Stockers and Callers or who started in clerical positions for eMetro.</p>
                                        We believe that eMetro’ s future
                                    Company executive officers are currently working in our warehouses, depots and buying offices, as well as in our Home Office.</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Respect our suppliers</h3>
                                <div class="product-info-description product-i-content">
                                    <p>Our suppliers are our partners in business and for us to prosper as a company, they must prosper with us. To that end, we strive to:
                                    </p><br>
                                    <p>    •	Treat all suppliers and their representatives as we would expect to be treated if visiting their places of business.</p>
                                    <p>    •	Honor all commitments.</p>
                                    <p>    •	Protect all suppliers’ property assigned to eMetro as though it were our own.</p>
                                    <p>    •	Not accept gratuities of any kind from a supplier.</p>
                                </div>
                                <br>
                                <div class="product-info-description product-i-content">
                                    <p>These guidelines are exactly that – guidelines – some common sense rules for the conduct of our business.
                                        At the core of our philosophy as a company is the implicit understanding that all of us, employees and management alike,
                                        must conduct ourselves in an honest and ethical manner every day. Dishonest conduct will not be tolerated.
                                        To do any less would be unfair to the overwhelming majority of our employees who support and respect eMetro’s commitment to ethical business conduct.
                                        Our employees must avoid actual or apparent conflicts of interest, including creating a business in competition with the Company or
                                        working for or on behalf of another employer in competition with the Company.
                                        If you are ever in doubt as to what course of action to take on a business matter that is open to varying ethical interpretations,
                                        TAKETHE HIGH ROAD AND DO WHAT IS RIGHT.
                                    </p><br>
                                    <p>If we follow the four principles of our Code of Ethics throughout our organization,
                                        then we will achieve our fifth principle and ultimate goal, which is to:</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Reward our shareholders</h3>
                                <p>    •	As a company with stock that is traded publicly on the  Stock Market, our shareholders are our business partners.</p>
                                <br>
                                <p>    •	We can only be successful so long as we are providing them with a good return on the money they invest in our Company.</p>
                                <br>
                                <p>    •	This, too, involves the element of trust. They trust us to use their investment wisely and to operate our business in such a way that it is profitable.</p>
                                <p>    •	Over the years eMetro has been in business, we have consistently followed an upward trend in the value of our stock. Yes, we have had our ups and our downs,
                                    but the overall trend has been consistently up.</p><br>
                                <p>    •	We believe eMetro stock is a good investment, and we pledge to operate our Company in such a way that our present and future stockholders,
                                    as well as our employees, will be rewarded for our efforts</p><br>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">Reporting of Violations and Enforcement</h3>
                                <div class="product-info-description product-i-content">
                                    <p>The Code of Ethics applies to all directors, officers, and employees of the Company.
                                        Conduct that violates the Code of Ethics will constitute grounds for disciplinary action,
                                        ranging from reprimand to termination and possible criminal prosecution.
                                    </p><br>
                                    <p> All employees are expected to promptly report actual or suspected violations of law or the Code of Ethics.
                                        Federal law, other laws and eMetro policy protect employees from retaliation if complaints are made in good faith.
                                        Violations involving employees should be reported to the responsible Executive Vice President, who shall be responsible for
                                        taking prompt and appropriate action to investigate and respond. Other violations (such as those involving suppliers) and those involving accounting,
                                        internal control and auditing should be reported to the general Counsel or the Chief Compliance Officer (2549 WAUKEGAN RD UNIT 330 BANNOCKBURN, IL 60015 USA.),
                                        who shall be responsible for taking prompt and appropriate action to investigate and respond.
                                        Reports or complaints can also be made, confidentially if you choose, through the Whistleblower Policy link on the Company’s eNet or Intranet site..</p>
                                </div>
                                <br>
                                <div class="product-info-description product-i-content">
                                    <p>What do eMetro’s Mission Statement and Code of Ethics have to do with you?
                                    </p><br>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">EVERYTHING!</h3>
                                <p>   The continued success of our Company depends on how well each of eMetro’s employees adheres to the high standards mandated by our Code of Ethics.
                                    And a successful company means increased opportunities for success and advancement for each of you.</p>
                                <br>
                                <p>   No matter what your current job, you can put eMetro’s Code of Ethics to work every day.
                                    It’s reflected in the energy and enthusiasm you bring to work, in the relationships you build with your management,
                                    your co-workers, our suppliers and our members</p>
                                <br>
                                <p>   By always choosing to do the right thing, you will build your own self-esteem, increase your chances for success and make eMetro more successful, too.
                                    It is the synergy of ideas and talents,
                                    each of us working together and contributing our best, which makes eMetro the great company it is today and lays the groundwork for what we will be tomorrow.</p>
                                <p>    •	Over the years eMetro has been in business, we have consistently followed an upward trend in the value of our stock. Yes, we have had our ups and our downs,
                                    but the overall trend has been consistently up.</p><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/footer/about-us.blade.php ENDPATH**/ ?>