<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="breadcrumb">
    <div class="container">
        <ol class="breadcrumb-list">
            <li>
                <a href="/">
                    <span>Home</span>
                </a>
            </li>
            <li>
                <strong> Accesibility </strong>
            </li>
        </ol>
    </div>
</div>
<section>
    <div class="catalog-view">
        <div class="container">
            <div class="catalog-view-row">
            </div>
            <!-- Product Details Start -->
            <div class="product-info">
                <ul class="product-tablist row m-0">
                    <li class="col p-0">
                        <a href="#product-details" class="scrollLink active">
                            <h2>Accessibility</h2>
                        </a>
                    </li>
                </ul>
                <div class="product-i-tabs row m-0">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3 class="section-title">eMetro's Commitment</h3>
                                <div class="product-info-description product-i-content">
                                   <span >eMetro is committed to providing access to our websites and mobile apps to people with disabilities.
                                       To meet this commitment, we strive to design and maintain our web pages and apps in conformance with standards.
                                   </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h4 class="section-title">Key accessibility features of eMetro sites include, but are not limited to:</h4>
                                <br>
                                <div class="product-info-description product-i-content">

                                    <p>  1) Appropriate site structure for use with assistive technologies.</p><br>
                                    <p>  2) Appropriate text equivalents for images.</p><br>
                                    <p>  3) Appropriate labels for forms and input fields.</p><br>
                                    <p>  4) Full keyboard access.</p><br>
                                    <p>  5) Captioning for videos.</p><br>
                                    <p>  6) Proper color contrast ratios for text and images with text.</p><br>

                                  <p>If you are having trouble using this site (or mobile app) because of a disability, please call us at 080-6733 5567 for assistance. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/footer/accesibility.blade.php ENDPATH**/ ?>