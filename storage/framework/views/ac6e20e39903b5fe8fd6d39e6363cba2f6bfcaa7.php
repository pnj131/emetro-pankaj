<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="breadcrumb">
    <div class="container">
        <ol class="breadcrumb-list">
            <li>
                <a href="/">
                    <span>Home</span>
                </a>
            </li>
            <li>
                <strong> Preventing Fraud </strong>
            </li>
        </ol>
    </div>
</div>
<section>
    <div class="catalog-view">
        <div class="container">
            <div class="catalog-view-row">
            </div>
            <!-- Product Details Start -->
            <div class="product-info">
                <ul class="product-tablist row m-0">
                    <li class="col p-0">
                        <a href="#product-details" class="scrollLink active">
                            <h2>Preventing Fraud</h2>
                            <h5>Currently Known Scams</h5>
                        </a>
                    </li>
                </ul>
                <div class="product-i-tabs row m-0">
                    <div class="row">
                        <div class="col-md-6 col-lg-16 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <p>Keeping our customers' personal information secure and confidential is one of eMetro's highest priorities.
                                    Below are some of the most common types of scams and suggestions on how you can be aware and help protect yourself against identity theft.</p>
                                <br>

                                <div class="product-info-description product-i-content">
                                    <p>  .	Fictitious Job Offer Emails</p><br>
                                    <p>  .	Phishing and Spoofing</p><br>
                                    <p>  .	Pop-up Advertisements</p><br>
                                    <p>  .	Check Scam</p><br>
                                    <p>  .	Fraudulent Phone Calls</p><br>
                                    <p>  .	PayPal Phishing Scam</p><br>
                                    <p>  .	What Can You Do?</p><br>
                                </div>
                                <br>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h4 class="section-title">FICTITIOUS JOB OFFER EMAILS</h4>
                                <br>
                                <div class="product-info-description product-i-content">

                                    <p> We are aware that someone has launched an email campaign advising individuals that eMetro is offering or may offer them a job.
                                        The emails can be quite authentic-looking. They contain a subject line such as "Provisional Job Offer," and purport to have been
                                        sent directly from one of our executives or from our human resources department. Enclosures, such as company information sheets
                                        and detailed questionnaires, may display the eMetro logo.
                                        The emails may include a request for payment of a processing fee of several hundred rupees and/or they may offer reimbursement for certain job-application or relocation expenses.</p><br>
                                    <p>  These emails are fraudulent. eMetro does not extend job offers via email to individuals with whom it has had no prior contact.
                                        We do not ask prospective employees to pay a fee to be considered for a position or to receive a job offer. We do not authorize recruiters
                                        or agents to do any of these things on our behalf.
                                         Although we sincerely regret that anyone may have been deceived by these fraudulent job offers, we will not honor them in any way.</p><br>
                                    <p>  You should not respond to any of these job offers by revealing personal information, nor should you send any funds in response to them.
                                         If you receive one of these fraudulent offers, please report the matter to the Indian Secret Services.</p><br>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-6 col-lg-16 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                               <h3>PHISHING AND SPOOFING</h3>

                                <div class="product-info-description product-i-content">
                                    <p>It is an unfortunate fact of the Internet that at any given time there are numerous illegitimate pop-up ads, surveys, websites, emails,
                                        social media posts and advertisements that purport to be from or authorized by eMetro.
                                        It is unlikely that eMetro is affiliated with these promotions. Rather, their purpose is to entice you to disclose personal information such as the
                                        following:
                                    </p>
                                    <p>    •	Name and address</p><br>
                                    <p>    •	Aadhar / PAN Numbers</p><br>
                                    <p>    •	Credit card numbers / Bank account numbers</p><br>
                                    <p>    •	PINs / passwords</p><br>
                                </div>
                                <p>They may also ask you to purchase something that may or may not be delivered to you.</p>
                                <br>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h4 class="section-title">We’ve provided some tips below to help you determine a promotion is authentic.</h4>
                                <br>
                                <div class="product-info-description product-i-content">
                                    <p>    •	Unsolicited electronic communications from eMetro do not ask for your personal information such as username,
                                                password, Credit Card Information, Date of Birth or Aadhaar Card Number. Never provide personal information in response
                                                to an electronic communication.
                                    </p><br>
                                    <p>    •	If you receive an order confirmation for something you did not order from www.eMetro.com,
                                                do not click on any links or open any attachments.
                                    </p><br>
                                    <p>    •	If you receive a communication that looks like it is from eMetro, check to see who sent it.
                                            Be aware of typos and misspellings and, in particular, of return addresses and contact links
                                            that do not end with a plain “@eMetro.com.” When in doubt, don’t respond.
                                    </p><br>
                                    <p>    •	Digital eMetro Shop Cards are the only exception to this rule. Digital eMetro Cards will arrive from the email: eMetro-cust@emetro.com
                                    </p><br>
                                    <p>    •	Be particularly cautious of pop-up solicitations, job-finding sites, and opportunities to win eMetro Membership Cards.
                                    </p><br>
                                    <P>    •	Review the Terms and Conditions of any eMetro-related app before downloading to ensure that the app is a genuine eMetro app,
                                                created and supported by eMetro.
                                    </p><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-16 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3>POP-UP ADVERTISEMENTS</h3>

                                <div class="product-info-description product-i-content">
                                    <p>Some advertisements "pop up" in a separate browser window advising that you have won a contest or request that you participate in a
                                        survey to collect a prize. They may then ask that you provide personal information in order to receive your gift.
                                        By clicking on the link it is possible that you are also downloading viruses designed to capture or destroy information on your computer.
                                    </p><br>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3>CHECK SCAM</h3>
                                <div class="product-info-description product-i-content">
                                   <p>
                                       A similar but low-tech scam circulates occasionally, in the form of a Cheque, purportedly from eMetro, and instructions to deposit it right away,
                                       usually in order to receive the balance of a larger sum you have "won" or that has otherwise come to you unexpectedly. You will be instructed to
                                       immediately send a payment, in the form of a second personal Cheque, wire transfer, or similar, to some third party to cover taxes, processing fees,
                                       administrative costs, or some similarly vague expense. The Cheque you received in the mail likely will look quite authentic, and probably has our eMetro
                                       Logo on it. Do not deposit the Cheque or follow the instructions you receive with it. The Cheque you deposit will bounce;
                                       the Cheque you write or funds you wire will clear, before you know about the bounce, and certainly before you can effect stop payment on your own
                                   </p><br>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-16 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3>FRAUDULENT PHONE CALLS</h3>

                                <div class="product-info-description product-i-content">
                                    <p>We have received reports of both members and non-members being contacted in an apparent phone scam / phishing scheme attempting to collect personal information.
                                        The caller offers either a eMetro Membership Card as a reward for completing a survey or a Gift in exchange for providing some personal information.
                                    </p><br>
                                    <p>
                                        The fraudsters may be using software to show a local phone number on caller ID but hide the real phone number the calls are originating from.
                                    </p><br>
                                    <p>
                                        Please know that these are fraudulent calls attempting to obtain personal information. These are in no way affiliated with eMetro or any Gift from eMetro.
                                        If you believe you have provided these callers your personal information,
                                        we recommend that you take appropriate steps by raising an FIR or possibly to the near-by Police Station if you believe your identity has been stolen.
                                    </p>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="product-i-box  pl-0 pr-0" id="product-details">
                                <h3>WHAT CAN YOU DO?</h3>
                                <div class="product-info-description product-i-content">

                                    <p>   •	Never respond to emails that cannot be verified.</p><br>
                                    <p>   •	Never provide personal information via e-mail.</p><br>
                                    <p>   •	Contact the business by using legitimate phone numbers to verify the request.</p><br>
                                    <p>   •	Enter websites using your browser and not by clicking on provided links.</p><br>
                                    <p>   •	Be cautious of any solicitation requesting that you deposit a Cheque or pay a fee to collect a prize, get a job, or cover vaguely described "costs."</p><br>
                                    <p>   •	Consider filing an FIR or Online complaints may be filed with the Police Station.</p><br>

                                    <p>   Please contact us to report any suspicious emails or contacts that are using the eMetro name.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/footer/preventing-fraud.blade.php ENDPATH**/ ?>