<style>

    img, object, video, embed {
        height: 75%;
        max-width: 100%;
    }
    #img1{
        margin-top : 10px;
    }
    #img2{
        margin-top : 10px;
    }

    #ac-wrapper {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(255, 255, 255, .6);
        z-index: 9998;
    }
    #popup {
        padding: 40px;
        width: 567px;
        height: 400px;
        background: #FFFFFF;
        border: 5px solid #000;

        box-shadow: #64686e 0px 0px 3px 3px;
        -moz-box-shadow: #64686e 0px 0px 3px 3px;
        -webkit-box-shadow: #64686e 0px 0px 3px 3px;
        position: relative;
        top: 70px;
        left: 300px;
    }
</style>
<!-- Pin code wrapper -->
<div id="ac-wrapper" style="padding: 100px;">
    <div id="popup">
        <div class="Logo"><a href="#"><img src="https://emetro.demolink.co.in/new_assest/images/logo.png" height="75" width="150"></a></div>
        <h5>In order to view the products available in your area. Please provide your Pin Code.</h5>
        <br>
        <div class="form-group col-md-12">
            <input type="text" name="zipcode" class="form-control" placeholder="Pin Code">

        </div>
        <br>
        <div class="form-group col-md-12">
            <input type="button" name="zip-btn" class="form-control btn btn-info" value="Set Delivery Pin Code" onclick="PopUp('hide')" style="background-color: #0a4881;">

        </div>

        <br>
        <div class="form-group col-md-12">
            <input type="button" name="continue-btn" value="Continue Shopping" class="form-control btn btn-info" onclick="PopUp('hide')" style="background-color: #0a4881;">

        </div>




    </div>
</div>
<!--  pin code end ---->


<script>
    function PopUp(hideOrshow) {
        if (hideOrshow == 'hide') document.getElementById('ac-wrapper').style.display = "none";
        else document.getElementById('ac-wrapper').removeAttribute('style');
    }
</script>
<?php /**PATH D:\xampp7\htdocs\emetro_ecommerce\resources\views/layout/pincode_wrapper.blade.php ENDPATH**/ ?>